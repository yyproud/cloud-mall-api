package com.yang.api.system.dubbo;

import com.yang.common.core.exception.ServiceException;
import com.yang.system.dto.LoginDto;
import com.yang.system.dto.RegisterDto;
import com.yang.system.entity.SysUserEntity;
import com.yang.system.vo.LoginVo;

/**
 * dubbo 系统用户
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 13:55
 */
public interface RangedSysUserService {
    /**
     * 根据用户账号查询
     *
     * @param loginDto 用户登陆信息
     * @return 用户实体
     * @throws ServiceException 自定义异常
     */
    LoginVo getByUserName(LoginDto loginDto) throws ServiceException;

    /**
     * 注册用户
     *
     * @param registerDto 注册信息
     * @return 用户实体
     * @throws ServiceException 自定义异常
     */
    LoginVo register(RegisterDto registerDto) throws ServiceException;

    /**
     * 根据id查询用户
     *
     * @param id 用户id
     * @return SysUserEntity
     */
    SysUserEntity getUserById(Long id);
}
