package com.yang.api.system.dubbo;

import com.yang.im.dto.ChatDto;
import com.yang.im.dto.ChatListDto;
import com.yang.im.dto.ChatMemberDto;
import com.yang.im.entity.ChatListEntity;
import com.yang.im.entity.ChatMemberEntity;

import java.util.List;

/**
 * @Author: yang
 * @CreateTime: 2024-10-25
 */
public interface RangedChatListService {

    /**
     * 查询chatList
     *
     * @param userId   用户id
     * @param friendId 好友id
     * @return Long
     */
    ChatListEntity queryChatListId(Long userId, Long friendId);

    /**
     * 新增chatList
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(ChatListDto dto);

    Boolean insertMember(ChatMemberDto chatMemberDto);

    // 新增会话
    Boolean addChat(ChatDto chatDto);

    List<ChatMemberEntity> queryMember(String listId);
}
