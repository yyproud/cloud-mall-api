package com.yang.api.system.dubbo;

import com.yang.im.dto.FriendApplyDto;
import com.yang.im.dto.FriendDto;
import com.yang.im.entity.FriendApplyEntity;
import com.yang.im.entity.FriendEntity;

/**
 * @Author: yang
 * @CreateTime: 2024-10-22
 */
public interface RangedFriendService {

    FriendEntity queryFriendById(Long friendId, Long userId);

    Boolean insert(FriendDto dto);

    FriendApplyEntity checkApply(Long friendId, Long userId);

    /**
     * 查看申请详情
     *
     * @param id id
     * @return FriendApplyEntity
     */
    FriendApplyEntity getApplyById(Long id);


    Boolean updateApply(FriendApplyDto friendApplyDto);

    Boolean insertApply(FriendApplyDto friendApplyDto);
}
