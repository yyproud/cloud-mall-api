
<h1 style="margin: 30px 0 30px; font-weight: bold; text-align: center">cloud-mall-api</h1>
<h4 style="text-align: center">基于Spring Cloud Alibaba 微服务架构的电商学习项目。</h4>



### 技术栈

- Spring Boot  2.7.11
- Spring Cloud 2021.0.7
- Spring Cloud Alibaba 2021.0.5.0
- Gateway
- Dubbo 3.1.8
- Nacos 2.2.0
- Sentinel 1.8.6
- Seata 1.6.1
- RocketMq 4.9.4
- Sa-Token 1.34.0
- MyBatis-Plus 3.5.3.1
- Mysql 8.0.32
- Redis 
- Netty
- WebSocket
- Minio
- Swagger+Knife4j
### 项目结构

```
cloud-mall-api
├── mall-api       // DUBBO接口模块
├── mall-auth      // 认证中心
├── mall-common    // 公共模块
│   └── mall-common-core        // 公共核心模块
│   └── mall-common-doc         // API文档模块
│   └── mall-common-dubbo       // DUBBO服务模块
│   └── mall-common-entity      // 公共实体类模块
│   └── mall-common-mq          // RocketMq模块
│   └── mall-common-mybatis     // 数据库服务模块
│   └── mall-common-redis       // 缓存模块
│   └── mall-common-satoken     // 令牌模块
│   └── mall-common-security    // 安全模块
│── mall-gateway    // 网关模块
│── mall-service    // 服务模块
│   └── mall-service-coss       // 云对象存储服务模块
│   └── mall-service-generator  // 代码生成器模块
│   └── mall-service-im         // 即时消息服务模块
│   └── mall-service-push       // 推送模块
│   └── mall-service-system     // 系统模块
