package com.yang.common.mybatis.core.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yang.common.mybatis.core.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 公用实体基类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 16:30
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class BaseEntity extends BaseDto {

    /**
     * 创建者
     */
    private Long createId;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createTime;
    /**
     * 描述
     */
    private String remark;
    /**
     * 删除标识
     */
    private Integer isDel;

    /**
     * 创建者姓名
     */
    private String createName;

    /**
     * 请求参数
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params = new HashMap<>();
}
