package com.yang.common.mybatis.core.page;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/18 16:37
 */
@Data
@NoArgsConstructor
public class PageView<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 总记录数
     */
    private int totalCount;
    /**
     * 每页记录数
     */
    private int pageSize;
    /**
     * 总页数
     */
    private int totalPage;
    /**
     * 当前页数
     */
    private int currPage;
    /**
     * 排序规则集合
     */
    private List<OrderItem> orderItems;
    /**
     * 列表数据
     */
    private List<T> list;

    /**
     * 封装返回分页数据
     *
     * @param page 原始数据
     * @return 封装后数据
     */
    public static <T> PageView<T> build(IPage<T> page) {
        PageView<T> rspData = new PageView<>();
        rspData.setList(page.getRecords());
        rspData.setTotalCount((int) page.getTotal());
        rspData.setPageSize((int) page.getSize());
        rspData.setCurrPage((int) page.getCurrent());
        rspData.setTotalPage((int) page.getPages());
        rspData.setOrderItems(page.orders());
        return rspData;
    }


}
