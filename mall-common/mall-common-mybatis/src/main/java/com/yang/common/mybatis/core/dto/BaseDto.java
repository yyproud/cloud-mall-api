package com.yang.common.mybatis.core.dto;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.yang.common.core.exception.ServiceException;
import com.yang.common.core.utils.SqlUtils;
import com.yang.common.core.utils.StringUtils;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 公用查询参数基类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/6 11:28
 */
@Data
public class BaseDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 分页页数
     */
    private Integer page;
    /**
     * 每页大小
     */
    private Integer limit;
    /**
     * 排序列
     */
    private String orders;
    /**
     * 排序的方向desc或者asc
     */
    private String isAsc;

    /**
     * 额外请求参数
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params = new HashMap<>();

    /**
     * 当前记录起始索引 默认值
     */
    public static final int DEFAULT_PAGE_NUM = 1;

    /**
     * 每页显示记录数 默认值 默认查全部
     */
    public static final int DEFAULT_PAGE_SIZE = Integer.MAX_VALUE;
}
