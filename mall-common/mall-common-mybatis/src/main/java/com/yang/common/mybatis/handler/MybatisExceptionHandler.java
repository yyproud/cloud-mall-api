package com.yang.common.mybatis.handler;

import com.yang.common.core.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.MyBatisSystemException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * Mybatis异常处理器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/28 15:40
 */
@Slf4j
@RestControllerAdvice
public class MybatisExceptionHandler {

    private static final String CANNOT_FIND_DATASOURCE_EXCEPTION = "CannotFindDataSourceException";

    /**
     * 主键或UNIQUE索引，数据重复异常
     */
    @ExceptionHandler(DuplicateKeyException.class)
    public Result handleDuplicateKeyException(DuplicateKeyException e, HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        log.error("请求地址'{}',数据库中已存在记录'{}'", requestUrl, e.getMessage());
        return Result.error("数据库中已存在该记录");
    }

    /**
     * Mybatis系统异常 通用处理
     */
    @ExceptionHandler(MyBatisSystemException.class)
    public Result handleCannotFindDataSourceException(MyBatisSystemException e, HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        String message = e.getMessage();
        if (message != null && message.contains(CANNOT_FIND_DATASOURCE_EXCEPTION)) {
            log.error("请求地址'{}', 未找到数据源", requestUrl);
            return Result.error("未找到数据源，请联系管理员确认");
        }
        log.error("请求地址'{}', Mybatis系统异常", requestUrl, e);
        return Result.error(message);
    }
}
