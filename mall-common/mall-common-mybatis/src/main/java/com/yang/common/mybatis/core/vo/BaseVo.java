package com.yang.common.mybatis.core.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 公用视图基类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/10/9 17:04
 */
@Data
public class BaseVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 创建者姓名
     */
    private String createName;
    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm", timezone = "GMT+8")
    private Date createTime;
}
