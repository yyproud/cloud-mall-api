package com.yang.common.mybatis.core.entity;

import lombok.Data;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Tree基类
 *
 * @author Lion Li
 */

@Data
public class TreeEntity<T> implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 父菜单名称
     */
    private String parentName;

    /**
     * 父菜单ID
     */
    private Long parentId;

    /**
     * 子部门
     */
    private List<T> children = new ArrayList<>();

}
