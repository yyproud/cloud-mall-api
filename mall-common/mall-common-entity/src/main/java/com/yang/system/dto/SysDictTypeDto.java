package com.yang.system.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 字典类型业务对象
 *
 * @author 杨旭
 * @email
 * @create 2023/10/16 17:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysDictTypeDto extends BaseDto {

    /**
     * 字典主键
     */
    @NotNull(message = "字典主键不能为空", groups = {EditGroup.class})
    private Long dictId;

    /**
     * 字典名称
     */
    @NotBlank(message = "字典名称不能为空", groups = {AddGroup.class, EditGroup.class})
    private String dictName;

    /**
     * 字典类型
     */
    @NotBlank(message = "字典类型不能为空", groups = {AddGroup.class, EditGroup.class})
    private String dictType;

    /**
     * 状态（0正常 1停用）
     */
    @NotBlank(message = "状态（0正常 1停用）不能为空", groups = {AddGroup.class, EditGroup.class})
    private String status;

    /**
     * 备注
     */
    private String remark;

    /**
     * 查询开始时间
     */
    private String beginCreateTime;
    /**
     * 查询结束时间
     */
    private String endCreateTime;

}
