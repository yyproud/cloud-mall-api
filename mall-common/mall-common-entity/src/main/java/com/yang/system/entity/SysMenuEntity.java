package com.yang.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.TreeEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 系统菜单
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/26 20:18
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_menu")
public class SysMenuEntity extends TreeEntity<SysMenuEntity> {

    /**
     * 菜单类型（目录）
     */
    public static final String TYPE_DIR = "M";
    /**
     * 菜单类型（菜单）
     */
    public static final String TYPE_MENU = "C";
    /**
     * 菜单类型（按钮）
     */
    public static final String TYPE_BUTTON = "F";
    /**
     * 是否菜单外链（是）
     */
    public static final String YES_FRAME = "0";
    /**
     * 是否菜单外链（否）
     */
    public static final String NO_FRAME = "1";
    /**
     * InnerLink组件标识
     */
    public static final String INNER_LINK = "InnerLink";
    /**
     * Layout组件标识
     */
    public static final String LAYOUT = "Layout";
    /**
     * ParentView组件标识
     */
    public static final String PARENT_VIEW = "ParentView";
    /**
     * 菜单ID
     */
    @TableId
    private Long menuId;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 父菜单ID
     */
    private Long parentId;
    /**
     * 显示顺序
     */
    private Integer menuSort;
    /**
     * 路由地址
     */
    private String path;
    /**
     * 路由参数
     */
    private String pathParam;
    /**
     * 组件路径
     */
    private String component;
    /**
     * 是否为外链（0:是 1:否）
     */
    private String isFrame;
    /**
     * 是否缓存（0:缓存 1:不缓存）
     */
    private String isCache;
    /**
     * 菜单类型（M目录 C菜单 F按钮）
     */
    private String menuType;
    /**
     * 显示状态（0:显示 1:隐藏）
     */
    private String visible;
    /**
     * 菜单状态（0:正常 1:停用）
     */
    private String menuStatus;
    /**
     * 权限标识
     */
    private String perms;
    /**
     * 菜单图标
     */
    private String icon;
    /**
     * 创建者
     */
    private Long createId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 备注
     */
    private String remark;
    /**
     * 删除标识
     */
    @TableLogic
    private Integer isDel;
}
