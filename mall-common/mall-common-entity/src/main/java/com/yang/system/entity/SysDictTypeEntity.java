package com.yang.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 字典类型对象
 *
 * @author 杨旭
 * @email
 * @create 2023/10/16 17:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_dict_type")
public class SysDictTypeEntity extends BaseEntity {


    /**
     * 状态：正常
     */
    public static final Integer STATUS_VALID = 0;
    /**
     * 状态：停用
     */
    public static final Integer STATUS_LOCK = 1;
    /**
     * 字典主键
     */
    @TableId(value = "dict_id")
    private Long dictId;
    /**
     * 字典名称
     */
    private String dictName;
    /**
     * 字典类型
     */
    private String dictType;
    /**
     * 状态（0正常 1停用）
     */
    private String status;
}
