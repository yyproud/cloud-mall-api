package com.yang.system.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 用户视图
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 9:28
 */
@Data
public class SysUserVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long userId;
    /**
     * 用户姓名
     */
    private String nickName;
    /**
     * 用户账号
     */
    private String userName;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 用户性别
     */
    private String gender;
    /**
     * 用户签名
     */
    private String doodling;
}