package com.yang.system.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * 登陆用户视图
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/23 14:46
 */
@Data
public class LoginVo implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * 用户类型
     */
    private String userType;
    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 用户账号
     */
    private String userName;
    /**
     * 用户姓名
     */
    private String nickName;
    /**
     * 部门ID
     */
    private Long deptId;
    /**
     * 部门名称
     */
    private String deptName;
    /**
     * 菜单权限
     */
    private Set<String> menuPermission;
    /**
     * 角色权限
     */
    private Set<String> rolePermission;
    /**
     * TOKEN
     */
    private String token;

}
