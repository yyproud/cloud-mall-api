package com.yang.system.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户实体类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 9:08
 */
@Data
@TableName("sys_user")
public class SysUserEntity implements Serializable {

    /**
     * 账号状态：有效
     */
    public static final Integer STATUS_VALID = 0;
    /**
     * 账号状态：锁定
     */
    public static final Integer STATUS_LOCK = 1;
    /**
     * 默认密码
     */
    public static final String DEFAULT_PASSWORD = "123456";
    /**
     * 性别：男
     */
    public static final Integer SEX_MALE = 1;
    /**
     * 性别：女
     */
    public static final Integer SEX_FEMALE = 2;
    private static final long serialVersionUID = 1L;
    /**
     * 用户id
     */
    @TableId
    private Long userId;
    /**
     * 用户类型
     */
    private String userType;
    /**
     * 用户姓名
     */
    private String nickName;
    /**
     * 用户账号
     */
    private String userName;
    /**
     * 用户密码
     */
    private String password;
    /**
     * 用户性别
     */
    private String gender;
    /**
     * 联系方式
     */
    private String mobile;
    /**
     * 用户头像
     */
    private String avatar;
    /**
     * 用户状态
     */
    private Integer status;
    /**
     * 创建者
     */
    private Long createId;
    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 最后登陆时间
     */
    private Date lastLoginTime;
    /**
     * 最后登陆ip
     */
    private String lastLoginIp;
    /**
     * 用户描述
     */
    private String remark;
    private String doodling;
    /**
     * 上级id
     */
    private Long parentId;
    /**
     * 删除标识
     */
    @TableLogic
    private Integer isDel;
}
