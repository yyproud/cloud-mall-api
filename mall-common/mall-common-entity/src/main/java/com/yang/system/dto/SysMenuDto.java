package com.yang.system.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 系统菜单
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/26 15:53
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysMenuDto extends BaseDto {

    /**
     * 用户ID
     */
    private Long userId;
    /**
     * 菜单名称
     */
    private String menuName;
    /**
     * 显示状态（0:显示 1:隐藏）
     */
    private Integer visible;
    /**
     * 菜单状态（0:正常 1:停用）
     */
    private Integer menuStatus;

}
