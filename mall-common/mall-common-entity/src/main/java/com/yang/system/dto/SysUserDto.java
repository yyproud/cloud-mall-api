package com.yang.system.dto;

import lombok.Data;

/**
 * 用户查询传参
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 9:28
 */
@Data
public class SysUserDto {
    private Long userId;
    private String userName;
}
