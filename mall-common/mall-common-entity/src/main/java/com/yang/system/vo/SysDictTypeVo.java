package com.yang.system.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 字典类型视图对象
 *
 * @author 杨旭
 * @email
 * @create 2023/10/16 17:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class SysDictTypeVo extends BaseVo {


    /**
     * 字典主键
     */
    private Long dictId;

    /**
     * 字典名称
     */
    private String dictName;

    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 状态（0正常 1停用）
     */
    private String status;

    /**
     * 备注
     */
    private String remark;


}
