package com.yang.system.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 登陆信息
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/23 14:46
 */
@Data
public class LoginDto implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 用户类型
     */
    private String userType;
    /**
     * 用户账号
     */
    private String userName;
    /**
     * 用户密码
     */
    private String password;
    /**
     * 验证码KEY
     */
    private String key;
    /**
     * 验证码CODE
     */
    private String code;
}
