package com.yang.system.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 字典数据业务对象
 *
 * @author 杨旭
 * @email
 * @create 2023/10/17 16:33
 */
@Data
@ApiModel(value = "字典数据业务对象")
@EqualsAndHashCode(callSuper = true)
public class SysDictDataDto extends BaseDto {

    /**
     * 字典编码
     */
    @ApiModelProperty(value = "字典编码")
    @NotNull(message = "字典编码不能为空", groups = {EditGroup.class})
    private Long dictCode;

    /**
     * 字典排序
     */
    @ApiModelProperty(value = "字典排序")
    @NotNull(message = "字典排序不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long dictSort;

    /**
     * 字典标签
     */
    @ApiModelProperty(value = "字典标签")
    @NotBlank(message = "字典标签不能为空", groups = {AddGroup.class, EditGroup.class})
    private String dictLabel;

    /**
     * 字典键值
     */
    @ApiModelProperty(value = "字典键值")
    @NotBlank(message = "字典键值不能为空", groups = {AddGroup.class, EditGroup.class})
    private String dictValue;

    /**
     * 字典类型
     */
    @ApiModelProperty(value = "字典类型")
    @NotBlank(message = "字典类型不能为空", groups = {AddGroup.class, EditGroup.class})
    private String dictType;

    /**
     * 样式属性（其他样式扩展）
     */
    @ApiModelProperty(value = "样式属性（其他样式扩展）")
    private String cssClass;

    /**
     * 表格回显样式
     */
    @ApiModelProperty(value = "表格回显样式")
    @NotBlank(message = "表格回显样式不能为空", groups = {AddGroup.class, EditGroup.class})
    private String listClass;

    /**
     * 是否默认（Y是 N否）
     */
    @ApiModelProperty(value = "是否默认", example = "Y是 N否")
    @NotBlank(message = "是否默认（Y是 N否）不能为空", groups = {AddGroup.class, EditGroup.class})
    private String isDefault;

    /**
     * 状态（0正常 1停用）
     */
    @ApiModelProperty(value = "状态", example = "0正常 1停用")
    @NotBlank(message = "状态（0正常 1停用）不能为空", groups = {AddGroup.class, EditGroup.class})
    private String status;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注")
    private String remark;
}
