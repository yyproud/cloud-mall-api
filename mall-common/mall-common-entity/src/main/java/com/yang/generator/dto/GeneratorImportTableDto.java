package com.yang.generator.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * 导入表数据
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/19 9:45
 */
@Data
public class GeneratorImportTableDto implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 表名集合
     */
    private String tables;
    /**
     * 作者
     */
    private String author;
    /**
     * 生成包路径
     */
    private String packageName;
    /**
     * 自动去除表前缀
     */
    private Boolean autoRemovePre;
    /**
     * 表前缀
     */
    private String tablePrefix;

}
