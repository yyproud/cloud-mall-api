package com.yang.generator.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.yang.common.mybatis.core.dto.BaseDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * 代码生成查询器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/6 11:22
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class GeneratorTableDto extends BaseDto {

    /**
     * 表名称
     */
    private String tableName;
    /**
     * 表描述
     */
    private String tableComment;


    /**
     * 自定义请求参数
     */
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, Object> params = new HashMap<>();
}
