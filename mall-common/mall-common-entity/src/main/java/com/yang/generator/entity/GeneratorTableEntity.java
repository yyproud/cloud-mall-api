package com.yang.generator.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.core.constant.GeneratorConstants;
import com.yang.common.core.utils.StringUtils;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.apache.commons.lang3.ArrayUtils;

import java.util.List;

/**
 * 代码生成业务表
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 15:58
 */
@Data
@TableName("generator_table")
@EqualsAndHashCode(callSuper = true)
public class GeneratorTableEntity extends BaseEntity {


    @TableId
    private Long tableId;
    /**
     * 表名称
     */
    private String tableName;
    /**
     * 表描述
     */
    private String tableComment;
    /**
     * 关联子表的表名
     */
    private String subTableName;
    /**
     * 子表关联的外键名
     */
    private String subTableFkName;
    /**
     * 实体类名称
     */
    private String className;
    /**
     * 使用的模板（crud:单表操作 tree:树表操作 sub:主子表）
     */
    private String tplCategory;
    /**
     * 生成包路径
     */
    private String packageName;
    /**
     * 生成模块名
     */
    private String moduleName;
    /**
     * 生成业务名
     */
    private String businessName;
    /**
     * 生成功能名
     */
    private String functionName;
    /**
     * 生成功能作者
     */
    private String functionAuthor;
    /**
     * 生成功能作者邮箱
     */
    private String functionEmail;
    /**
     * 生成代码方式（0:zip压缩包 1:自定义路径）
     */
    private String generatorType;
    /**
     * 生成路径（不填默认项目路径）
     */
    private String generatorPath;
    /**
     * 其它生成选项
     */
    private String options;
    /**
     * 自动去除表前缀 (1:是 2:否)
     */
    private Integer autoRemovePre;
    /**
     * 表前缀
     */
    private String tablePrefix;

    /**
     * 一对多表
     */
    @TableField(exist = false)
    private List<GeneratorTableColumnEntity> generatorTableColumns;
    /**
     * 菜单ID列表
     */
    @TableField(exist = false)
    private List<Long> menuIds;
    /**
     * 主键信息
     */
    @TableField(exist = false)
    private GeneratorTableColumnEntity pkColumn;
    /**
     * 子表信息
     */
    @TableField(exist = false)
    private GeneratorTableEntity subTable;
    /**
     * 树编码字段
     */
    @TableField(exist = false)
    private String treeCode;
    /**
     * 树父编码字段
     */
    @TableField(exist = false)
    private String treeParentCode;
    /**
     * 树名称字段
     */
    @TableField(exist = false)
    private String treeName;
    /**
     * 上级菜单ID字段
     */
    @TableField(exist = false)
    private String parentMenuId;
    /**
     * 上级菜单名称字段
     */
    @TableField(exist = false)
    private String parentMenuName;

    public static boolean isSub(String tplCategory) {
        return tplCategory != null && StringUtils.equals(GeneratorConstants.TPL_SUB, tplCategory);
    }

    public static boolean isTree(String tplCategory) {
        return tplCategory != null && StringUtils.equals(GeneratorConstants.TPL_TREE, tplCategory);
    }

    public static boolean isCrud(String tplCategory) {
        return tplCategory != null && StringUtils.equals(GeneratorConstants.TPL_CRUD, tplCategory);
    }

    public static boolean isSuperColumn(String tplCategory, String javaField) {
        if (isTree(tplCategory)) {
            return StringUtils.equalsAnyIgnoreCase(javaField,
                    ArrayUtils.addAll(GeneratorConstants.TREE_ENTITY, GeneratorConstants.BASE_ENTITY));
        }
        return StringUtils.equalsAnyIgnoreCase(javaField, GeneratorConstants.BASE_ENTITY);
    }

    public boolean isSub() {
        return isSub(this.tplCategory);
    }

    public boolean isTree() {
        return isTree(this.tplCategory);
    }

    public boolean isCrud() {
        return isCrud(this.tplCategory);
    }

    public boolean isSuperColumn(String javaField) {
        return isSuperColumn(this.tplCategory, javaField);
    }
}


