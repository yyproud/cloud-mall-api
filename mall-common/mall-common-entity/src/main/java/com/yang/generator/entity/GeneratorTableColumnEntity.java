package com.yang.generator.entity;

import com.mybatisflex.annotation.Id;
import com.mybatisflex.annotation.Table;
import com.yang.common.core.constant.GeneratorConstants;
import com.yang.common.core.utils.StringUtils;
import lombok.Data;

import java.io.Serializable;

/**
 * 代码生成业务字段表
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 16:47
 */
@Data
@Table("generator_table_column")
public class GeneratorTableColumnEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    private Long columnId;
    /**
     * 业务表id
     */
    private Long tableId;
    /**
     * 列名称
     */
    private String columnName;
    /**
     * 列描述
     */
    private String columnComment;
    /**
     * 列类型
     */
    private String columnType;
    /**
     * JAVA类型
     */
    private String javaType;
    /**
     * JAVA字段名
     */
    private String javaField;
    /**
     * 是否主键
     */
    private String isPk;
    /**
     * 是否自增
     */
    private Integer isIncrement;
    /**
     * 是否必填
     */
    private String isRequired;
    /**
     * 是否为插入字段
     */
    private String isInsert;
    /**
     * 是否编辑字段
     */
    private String isEdit;
    /**
     * 是否列表字段
     */
    private String isList;
    /**
     * 是否查询字段
     */
    private String isQuery;
    /**
     * 查询方式
     */
    private String queryType;
    /**
     * 显示类型
     */
    private String htmlType;
    /**
     * 字典类型
     */
    private String dictType;

    /**
     * 排序
     */
    private Integer sort;

    public static boolean isSuperColumn(String javaField) {
        return StringUtils.equalsAnyIgnoreCase(javaField,
                // BaseEntity
                "createBy", "createTime", "updateBy", "updateTime",
                // TreeEntity
                "parentName", "parentId");
    }

    public static boolean isUsableColumn(String javaField) {
        // isSuperColumn()中的名单用于避免生成多余Domain属性，若某些属性在生成页面时需要用到不能忽略，则放在此处白名单
        return StringUtils.equalsAnyIgnoreCase(javaField, "parentId", "orderNum");
    }

    public boolean isPk() {
        return isPk(this.isPk);
    }

    public boolean isPk(String isPk) {
        return StringUtils.equals(GeneratorConstants.REQUIRE, isPk);
    }

    public boolean isSuperColumn() {
        return !isSuperColumn(this.javaField);
    }

    public boolean isList() {
        return isList(this.isList);
    }

    public boolean isList(String isList) {
        return "1".equals(isList);
    }

    public boolean isRequired() {
        return isRequired(this.isRequired);
    }

    public boolean isRequired(String isRequired) {
        return isRequired != null && StringUtils.equals("1", isRequired);
    }

    public boolean isInsert() {
        return isInsert(this.isInsert);
    }

    public boolean isInsert(String isInsert) {
        return isInsert != null && StringUtils.equals("1", isInsert);
    }

    public boolean isEdit() {
        return isInsert(this.isEdit);
    }

    public boolean isEdit(String isEdit) {
        return isEdit != null && StringUtils.equals("1", isEdit);
    }

    public boolean isUsableColumn() {
        return isUsableColumn(javaField);
    }

    public boolean isQuery() {
        return isQuery(this.isQuery);
    }

    public boolean isQuery(String isQuery) {
        return isQuery != null && StringUtils.equals("1", isQuery);
    }
}
