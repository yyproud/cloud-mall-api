package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;


/**
 * chatGroupApply业务对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatGroupApplyDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     * 会话id
     */
    @NotNull(message = "会话id不能为空", groups = {AddGroup.class, EditGroup.class})
    private String listId;

    /**
     * 邀请人id
     */
    @NotNull(message = "邀请人id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long inviteUserId;

    /**
     * 被邀请人id
     */
    @NotNull(message = "被邀请人id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long inviteToUserId;

    /**
     * 处理人
     */
    @NotNull(message = "处理人不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long handleUserId;

    /**
     * 状态
     */
    @NotNull(message = "状态不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer status;

    /**
     * 处理时间
     */
    @NotNull(message = "处理时间不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long handleTime;

    /**
     * 是否已读
     */
    @NotNull(message = "是否已读不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer isReader;

    /**
     * 邀请时间
     */
    @NotNull(message = "邀请时间不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long time;


}
