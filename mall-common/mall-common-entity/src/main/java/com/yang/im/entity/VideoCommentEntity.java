package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 视频评论对象
 *
 * @author yang
 * @email
 * @create 2024/10/20 20:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("im_video_comment")
public class VideoCommentEntity extends BaseEntity {


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 视频id
     */
    private Long shareId;
    /**
     * 评论id
     */
    private Long commentId;
    /**
     * 被评论人
     */
    private Long toId;
    /**
     * 评论内容
     */
    private String content;
    /**
     * 备注
     */
    private String remark;

}
