package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;


/**
 * chatList业务对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatListDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     * 用户id
     */
    @NotNull(message = "用户id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long userId;
    private Long friendId;

    /**
     * 消息列表
     */
    @NotNull(message = "消息列表不能为空", groups = {AddGroup.class, EditGroup.class})
    private String listId;

    /**
     * 类型：0单聊 1群聊
     */
    @NotNull(message = "类型：0单聊 1群聊不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer type;

    /**
     * 置顶
     */
    @NotNull(message = "置顶不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer top;

    /**
     * 置顶时间
     */
    @NotNull(message = "置顶时间不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long topTime;

    /**
     * 未读消息数量
     */
    @NotNull(message = "未读消息数量不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer noReaderNum;

    /**
     * 上次聊天时间
     */
    @NotNull(message = "上次聊天时间不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long lastChatTime;

    /**
     * 是否免打扰 0否 1是
     */
    @NotNull(message = "是否免打扰 0否 1是不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer isDisturb;


}
