package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 用户发布视频列对象
 *
 * @author yang
 * @email
 * @create 2024/10/20 20:46
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("im_video_share")
public class VideoShareEntity extends BaseEntity {


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 分类id
     */
    private Long categoryId;
    /**
     * 标题
     */
    private String title;
    /**
     * 视频地址
     */
    private String video;
    /**
     * 封面地址
     */
    private String gif;
    /**
     * 点赞
     */
    private Integer fabulous;
    /**
     * 评论
     */
    private Integer comment;
    /**
     * 备注
     */
    private String remark;

}
