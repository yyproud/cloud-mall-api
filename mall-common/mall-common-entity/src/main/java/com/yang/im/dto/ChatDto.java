package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * chat业务对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     * 房间id
     */
    @NotNull(message = "房间id不能为空", groups = {AddGroup.class, EditGroup.class})
    private String chatListId;

    /**
     * 消息发送者
     */
    @NotNull(message = "消息发送者不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long userId;

    /**
     * 消息内容类型
     */
    @NotNull(message = "消息内容类型不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer contentType;

    /**
     * 消息类型
     */
    @NotNull(message = "消息类型不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer msgType;

    /**
     * 消息内容拓展
     */
    @NotBlank(message = "消息内容拓展不能为空", groups = {AddGroup.class, EditGroup.class})
    private String content;

    /**
     * 发送时间
     */
    @NotNull(message = "发送时间不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long time;


}
