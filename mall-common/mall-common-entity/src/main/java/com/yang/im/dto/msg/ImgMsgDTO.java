package com.yang.im.dto.msg;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * 文本消息实体类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2024/3/14 10:36
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ImgMsgDTO {


    @ApiModelProperty("宽度（像素）")
    @NotNull
    private Integer width;

    @ApiModelProperty("高度（像素）")
    @NotNull
    private Integer height;
}
