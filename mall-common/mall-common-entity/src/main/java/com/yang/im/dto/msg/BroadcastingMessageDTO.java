package com.yang.im.dto.msg;

import lombok.Data;

/**
 * 广播发送的消息体
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/27 16:34
 */
@Data
public class BroadcastingMessageDTO {
    private static final long serialVersionUID = 1L;

    /**
     * 房间id
     */
    private Long roomId;

    /**
     * 房间类型
     */
    private Integer roomType;

    /**
     * 接收人id 如果房间类型是群组则为空
     */
    private Long receivedFriendId;

    private TextMsgDTO textMsgDTO;

}
