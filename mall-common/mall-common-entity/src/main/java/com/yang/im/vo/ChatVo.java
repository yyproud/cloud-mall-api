package com.yang.im.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * chat视图对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatVo extends BaseVo {


    /**
     *
     */
    private Long id;

    /**
     * 房间id
     */
    private String chatListId;

    /**
     * 消息发送者
     */
    private Long userId;

    /**
     * 消息内容类型
     */
    private Integer contentType;

    /**
     * 消息类型
     */
    private Integer msgType;

    /**
     * 消息内容拓展
     */
    private String content;

    /**
     * 发送时间
     */
    private Long time;


}
