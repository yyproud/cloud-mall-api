package com.yang.im.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 客户端存入Redis的聊天信息
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/29 15:05
 */
@Data
public class ChatLinkInfoVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private String userId;
    /**
     * ip地址
     */
    private String ip;
    /**
     * ip地址归属地
     */
    private String ipHomeLocation;
    /**
     * 入场时间
     */
    private String landingTime;

}
