package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * friend业务对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FriendDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     * 用户id
     */
    @NotNull(message = "用户id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long userId;

    /**
     * 好友id
     */
    @NotNull(message = "好友id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long friendId;

    /**
     * 用户名称/备注首字母
     */
    @NotBlank(message = "用户名称/备注首字母不能为空", groups = {AddGroup.class, EditGroup.class})
    private String letter;

    /**
     * 来源
     */
    @NotNull(message = "来源不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer friendFrom;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = {AddGroup.class, EditGroup.class})
    private String remark;

    /**
     * 创建时间
     */
    @NotNull(message = "创建时间不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long time;


}
