package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 用户发布视频列业务对象
 *
 * @author yang
 * @email
 * @create 2024/10/20 20:46
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoShareDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     * 分类id
     */
    @NotNull(message = "分类id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long categoryId;

    /**
     * 标题
     */
    @NotBlank(message = "标题不能为空", groups = {AddGroup.class, EditGroup.class})
    private String title;

    /**
     * 视频地址
     */
    @NotBlank(message = "视频地址不能为空", groups = {AddGroup.class, EditGroup.class})
    private String video;

    /**
     * 封面地址
     */
    @NotBlank(message = "封面地址不能为空", groups = {AddGroup.class, EditGroup.class})
    private String gif;

    /**
     * 点赞
     */
    @NotNull(message = "点赞不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer fabulous;

    /**
     * 评论
     */
    @NotNull(message = "评论不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer comment;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = {AddGroup.class, EditGroup.class})
    private String remark;


}
