package com.yang.im.dto.msg;

import lombok.Data;

import java.io.Serializable;

/**
 * 检查token入参
 *
 * @Author: yang
 * @CreateTime: 2024-10-21
 */
@Data
public class CheckTokenDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String data;

}
