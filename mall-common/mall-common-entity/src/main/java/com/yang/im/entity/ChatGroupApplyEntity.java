package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * chatGroupApply对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("im_chat_group_apply")
public class ChatGroupApplyEntity extends BaseEntity {


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 会话id
     */
    private String listId;
    /**
     * 邀请人id
     */
    private Long inviteUserId;
    /**
     * 被邀请人id
     */
    private Long inviteToUserId;
    /**
     * 处理人
     */
    private Long handleUserId;
    /**
     * 状态
     */
    private Integer status;
    /**
     * 处理时间
     */
    private Long handleTime;
    /**
     * 是否已读
     */
    private Integer isReader;
    /**
     * 邀请时间
     */
    private Long time;

}
