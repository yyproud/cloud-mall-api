package com.yang.im.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 视频评论视图对象
 *
 * @author yang
 * @email
 * @create 2024/10/20 20:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoCommentVo extends BaseVo {


    /**
     *
     */
    private Long id;

    /**
     * 视频id
     */
    private Long shareId;

    /**
     * 评论id
     */
    private Long commentId;

    /**
     * 被评论人
     */
    private Long toId;

    /**
     * 评论内容
     */
    private String content;

    /**
     * 备注
     */
    private String remark;


}
