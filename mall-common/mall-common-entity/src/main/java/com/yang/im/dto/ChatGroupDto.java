package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * chatGroup业务对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatGroupDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {AddGroup.class, EditGroup.class})
    private String listId;

    /**
     * 管理员id
     */
    @NotNull(message = "管理员id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long mainId;

    /**
     * 群名称
     */
    @NotBlank(message = "群名称不能为空", groups = {AddGroup.class, EditGroup.class})
    private String name;

    /**
     * 群公告
     */
    @NotBlank(message = "群公告不能为空", groups = {AddGroup.class, EditGroup.class})
    private String notice;

    /**
     * 是否禁言：0 否 1是
     */
    @NotNull(message = "是否禁言：0 否 1是不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer isMsg;

    /**
     * 群内加好友 0否 1是
     */
    @NotNull(message = "群内加好友 0否 1是不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer canGetBigred;


}
