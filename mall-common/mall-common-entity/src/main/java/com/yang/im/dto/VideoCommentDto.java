package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 视频评论业务对象
 *
 * @author yang
 * @email
 * @create 2024/10/20 20:44
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoCommentDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     * 视频id
     */
    @NotNull(message = "视频id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long shareId;

    /**
     * 评论id
     */
    @NotNull(message = "评论id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long commentId;

    /**
     * 被评论人
     */
    @NotNull(message = "被评论人不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long toId;

    /**
     * 评论内容
     */
    @NotBlank(message = "评论内容不能为空", groups = {AddGroup.class, EditGroup.class})
    private String content;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = {AddGroup.class, EditGroup.class})
    private String remark;


}
