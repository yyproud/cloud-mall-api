package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * friendApply业务对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FriendApplyDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     * 申请人id
     */
    @NotNull(message = "申请人id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long applyUserId;

    /**
     * 被申请人id
     */
    @NotNull(message = "被申请人id不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long friendUserId;

    /**
     * 申请内容
     */
    @NotBlank(message = "申请内容不能为空", groups = {AddGroup.class, EditGroup.class})
    private String content;

    /**
     * 申请来源
     */
    @NotNull(message = "申请来源不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer friendFrom;

    /**
     * 是否已读
     */
    @NotNull(message = "是否已读不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer isReader;

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {AddGroup.class, EditGroup.class})
    private Integer action;

    /**
     * 申请时间
     */
    @NotNull(message = "申请时间不能为空", groups = {AddGroup.class, EditGroup.class})
    private Long time;


}
