package com.yang.im.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 用户发布视频列视图对象
 *
 * @author yang
 * @email
 * @create 2024/10/20 20:46
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoShareVo extends BaseVo {


    /**
     *
     */
    private Long id;

    /**
     * 分类id
     */
    private Long categoryId;

    /**
     * 标题
     */
    private String title;

    /**
     * 视频地址
     */
    private String video;

    /**
     * 封面地址
     */
    private String gif;

    /**
     * 点赞
     */
    private Integer fabulous;

    /**
     * 评论
     */
    private Integer comment;

    /**
     * 备注
     */
    private String remark;


}
