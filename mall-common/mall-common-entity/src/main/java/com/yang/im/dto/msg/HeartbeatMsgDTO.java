package com.yang.im.dto.msg;

import com.yang.im.enums.ChatMessageTypeEnum;
import io.swagger.annotations.ApiModel;
import lombok.Data;

/**
 * 心跳消息实体类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2024/3/16 18:12
 */
@Data
@ApiModel(value = "字典数据业务对象")
public class HeartbeatMsgDTO {
    /**
     * 消息类型
     */
    private String action = ChatMessageTypeEnum.HEARTBEAT.getAction();

    private String data;
}
