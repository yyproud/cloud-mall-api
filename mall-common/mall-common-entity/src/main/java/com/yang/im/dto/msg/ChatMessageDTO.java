package com.yang.im.dto.msg;

import lombok.Data;

import java.io.Serializable;

/**
 * 客户端发送的消息体
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/27 16:34
 */
@Data
public class ChatMessageDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 房间id
     */
    private Long roomId;

    /**
     * 消息类型
     */
    private String action;

    /**
     * 消息体
     */
    private Object data;
}
