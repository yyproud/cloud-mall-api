package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 短视频分类业务对象
 *
 * @author yang
 * @email
 * @create 2024/10/20 20:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoCategoryDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     *
     */
    @NotBlank(message = "不能为空", groups = {AddGroup.class, EditGroup.class})
    private String name;


}
