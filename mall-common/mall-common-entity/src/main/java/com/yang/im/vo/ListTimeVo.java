package com.yang.im.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.yang.common.mybatis.core.vo.BaseVo;


/**
 * 消息最后阅读时间视图对象
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 23:03
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ListTimeVo extends BaseVo {


    /**
     * 
     */
    private Long id;

    /**
     * 
     */
    private String listId;

    /**
     * 
     */
    private Long userId;

    /**
     * 
     */
    private Long readTime;


}
