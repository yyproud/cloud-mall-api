package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * friendApply对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@TableName("im_friend_apply")
public class FriendApplyEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 申请人id
     */
    private Long applyUserId;
    /**
     * 被申请人id
     */
    private Long friendUserId;
    /**
     * 申请内容
     */
    private String content;
    /**
     * 申请来源
     */
    private Integer friendFrom;
    /**
     * 是否已读
     */
    private Integer isReader;
    /**
     *
     */
    private Integer action;
    /**
     * 申请时间
     */
    private Long time;

}
