package com.yang.im.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.yang.common.mybatis.core.vo.BaseVo;


/**
 * 成员视图对象
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 01:24
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatMemberVo extends BaseVo {


    /**
     * 
     */
    private Long id;

    /**
     * 
     */
    private String listId;

    /**
     * 
     */
    private Long userId;

    /**
     * 
     */
    private String nickName;

    /**
     * 
     */
    private Integer isAdmin;

    /**
     * 
     */
    private Integer isMsg;

    /**
     * 
     */
    private Integer isDisturb;

    /**
     * 
     */
    private Long inviteId;

    /**
     * 
     */
    private Integer voiceRoomState;


}
