package com.yang.im.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * friend视图对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class FriendVo extends BaseVo {


    /**
     *
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 好友id
     */
    private Long friendId;

    /**
     * 用户名称/备注首字母
     */
    private String letter;

    /**
     * 来源
     */
    private Integer from;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Long time;

    private String avatar;
    private String name;
    private String listId;


}
