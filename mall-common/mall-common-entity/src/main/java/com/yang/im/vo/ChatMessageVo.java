package com.yang.im.vo;

import lombok.Data;

import java.io.Serializable;

/**
 * 服务端返回的消息体
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/27 16:34
 */
@Data
public class ChatMessageVo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 消息类型
     */
    private Integer type;

    /**
     * 消息体
     */
    private Object body;
}
