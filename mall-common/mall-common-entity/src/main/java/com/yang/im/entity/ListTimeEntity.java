package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * 消息最后阅读时间对象
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 23:03
 */
@Data
@TableName("im_list_time")
public class ListTimeEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 
     */
    private String listId;
    /**
     * 
     */
    private Long userId;
    /**
     * 
     */
    private Long readTime;

}
