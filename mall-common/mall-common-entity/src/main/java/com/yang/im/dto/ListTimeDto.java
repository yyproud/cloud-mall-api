package com.yang.im.dto;

import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import com.yang.common.mybatis.core.dto.BaseDto;


/**
 * 消息最后阅读时间业务对象
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 23:03
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ListTimeDto extends BaseDto {

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * 
     */
    @NotBlank(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private String listId;

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long userId;

    /**
     * 
     */
    @NotNull(message = "不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long readTime;


}
