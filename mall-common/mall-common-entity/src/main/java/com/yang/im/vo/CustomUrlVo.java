package com.yang.im.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 自定义网址视图对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/19 23:28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomUrlVo extends BaseVo {


    /**
     *
     */
    private Long id;

    /**
     * 名称
     */
    private String name;

    /**
     * 图标
     */
    private String logo;

    /**
     * 网址
     */
    private String url;

    /**
     * 状态（0正常 1停用）
     */
    private String status;

    /**
     * 备注
     */
    private String remark;


}
