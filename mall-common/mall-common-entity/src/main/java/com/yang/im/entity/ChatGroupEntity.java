package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * chatGroup对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("im_chat_group")
public class ChatGroupEntity extends BaseEntity {


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     *
     */
    private String listId;
    /**
     * 管理员id
     */
    private Long mainId;
    /**
     * 群名称
     */
    private String name;
    /**
     * 群公告
     */
    private String notice;
    /**
     * 是否禁言：0 否 1是
     */
    private Integer isMsg;
    /**
     * 群内加好友 0否 1是
     */
    private Integer canGetBigred;
    private Integer isPhoto;

}
