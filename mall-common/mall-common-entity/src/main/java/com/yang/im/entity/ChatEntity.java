package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;


/**
 * chat对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@TableName("im_chat")
public class ChatEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 房间id
     */
    private String chatListId;
    /**
     * 消息发送者
     */
    private Long userId;
    /**
     * 消息内容类型
     */
    private Integer contentType;
    /**
     * 消息类型
     */
    private Integer msgType;
    /**
     * 消息内容拓展
     */
    private String content;
    /**
     * 发送时间
     */
    private Long time;

}
