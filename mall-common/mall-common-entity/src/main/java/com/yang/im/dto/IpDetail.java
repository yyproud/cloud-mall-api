package com.yang.im.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * IP详情
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/30 17:10
 */
@Data
public class IpDetail implements Serializable {


    public final static String ACCURACY_CITY = "城市";
    /**
     * 精度
     */
    private String accuracy;
    /**
     * 洲
     */
    private String continent;
    /**
     * 国
     */
    private String country;
    /**
     * 省
     */
    private String prov;
    /**
     * 市
     */
    private String city;
    /**
     * 区
     */
    private String district;

    public String getDistrict() {
        String district = this.district;
        if (ACCURACY_CITY.equals(this.accuracy)) {
            district = this.city;
        }
        return district;
    }


}
