package com.yang.im.vo.msg;

import lombok.Data;

import java.io.Serializable;

/**
 * 返回客户端的消息体
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/27 16:34
 */
@Data
public class ChatMessageVO implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 消息类型
     */
    private String action;

    /**
     * 消息体
     */
    private Object data;
}
