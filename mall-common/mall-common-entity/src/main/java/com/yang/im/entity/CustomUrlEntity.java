package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 自定义网址对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/19 23:28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("im_custom_url")
public class CustomUrlEntity extends BaseEntity {


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 名称
     */
    private String name;
    /**
     * 图标
     */
    private String logo;
    /**
     * 网址
     */
    private String url;
    /**
     * 状态（0正常 1停用）
     */
    private Integer status;
    /**
     * 备注
     */
    private String remark;

}
