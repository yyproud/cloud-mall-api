package com.yang.im.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 短视频分类视图对象
 *
 * @author yang
 * @email
 * @create 2024/10/20 20:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class VideoCategoryVo extends BaseVo {


    /**
     *
     */
    private Long id;

    /**
     *
     */
    private String name;


}
