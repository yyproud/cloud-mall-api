package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * friend对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("im_friend")
public class FriendEntity extends BaseEntity {


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 好友id
     */
    private Long friendId;
    /**
     * 用户名称/备注首字母
     */
    private String letter;
    /**
     * 来源
     */
    private Integer friendFrom;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Long time;

}
