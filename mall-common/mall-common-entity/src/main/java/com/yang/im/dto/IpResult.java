package com.yang.im.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Objects;

/**
 * 自定义IP结果
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/30 17:07
 */
@Data
public class IpResult<T> implements Serializable {
    /**
     * 返回状态码
     */
    private String code;
    /**
     * 返回信息
     */
    private String msg;
    /**
     * 返回数据
     */
    private T data;

    /**
     * 是否成功
     *
     * @return 结果
     */
    public boolean isSuccess() {
        return Objects.nonNull(this.code) && "Success".equals(this.code);
    }
}
