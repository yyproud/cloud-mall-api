package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * chatList对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@TableName("im_chat_list")
public class ChatListEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 好友id
     */
    private Long friendId;
    /**
     * 消息列表
     */
    private String listId;
    /**
     * 类型：0单聊 1群聊
     */
    private Integer type;
    /**
     * 置顶
     */
    private Integer top;
    /**
     * 置顶时间
     */
    private Long topTime;
    /**
     * 未读消息数量
     */
    private Integer noReaderNum;
    /**
     * 上次聊天时间
     */
    private Long lastChatTime;
    /**
     * 是否免打扰 0否 1是
     */
    private Integer isDisturb;

}
