package com.yang.im.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * 消息类型枚举
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/27 16:51
 */
@Getter
@AllArgsConstructor
public enum ChatMessageTypeEnum {
    ACTION_ERROR("actionError", "行动失败"),
    CHECK_TOKEN("checkToken", "检查token"),
    HEARTBEAT("ping", "心跳包"),
    TEXT("1", "文本消息"),
    RECALL("2", "撤回消息"),
    IMG("3", "图片"),
    FILE("4", "文件"),
    SOUND("5", "语音"),
    VIDEO("6", "视频"),
    EMOJI("7", "表情"),
    SYSTEM("8", "系统消息");

    /**
     * 类型
     */
    private final String action;
    /**
     * 描述
     */
    private final String desc;

    public static ChatMessageTypeEnum getAction(String action) {
        return Arrays.stream(ChatMessageTypeEnum.values())
                .filter(item -> item.getAction().equals(action))
                .findFirst()
                .orElse(HEARTBEAT);
    }
}
