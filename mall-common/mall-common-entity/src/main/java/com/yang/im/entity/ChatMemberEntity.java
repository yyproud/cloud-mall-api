package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;


/**
 * 成员对象
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 01:24
 */
@Data
@TableName("im_chat_member")
public class ChatMemberEntity implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 
     */
    @TableId(value = "id")
    private Long id;
    /**
     * 
     */
    private String listId;
    /**
     * 
     */
    private Long userId;
    /**
     * 
     */
    private String nickName;
    /**
     * 
     */
    private Integer isAdmin;
    /**
     * 
     */
    private Integer isMsg;
    /**
     * 
     */
    private Integer isDisturb;
    /**
     * 
     */
    private Long inviteId;
    /**
     * 
     */
    private Integer voiceRoomState;

}
