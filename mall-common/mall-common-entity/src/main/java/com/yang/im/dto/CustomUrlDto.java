package com.yang.im.dto;

import com.yang.common.mybatis.core.dto.BaseDto;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


/**
 * 自定义网址业务对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/19 23:28
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomUrlDto extends BaseDto {

    /**
     *
     */
    @NotNull(message = "不能为空", groups = {EditGroup.class})
    private Long id;

    /**
     * 名称
     */
    @NotBlank(message = "名称不能为空", groups = {AddGroup.class, EditGroup.class})
    private String name;

    /**
     * 图标
     */
    @NotBlank(message = "图标不能为空", groups = {AddGroup.class, EditGroup.class})
    private String logo;

    /**
     * 网址
     */
    @NotBlank(message = "网址不能为空", groups = {AddGroup.class, EditGroup.class})
    private String url;

    /**
     * 状态（0正常 1停用）
     */
    @NotBlank(message = "状态（0正常 1停用）不能为空", groups = {AddGroup.class, EditGroup.class})
    private String status;

    /**
     * 备注
     */
    @NotBlank(message = "备注不能为空", groups = {AddGroup.class, EditGroup.class})
    private String remark;


}
