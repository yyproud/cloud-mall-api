package com.yang.im.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * chatGroupApply视图对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatGroupApplyVo extends BaseVo {


    /**
     *
     */
    private Long id;

    /**
     * 会话id
     */
    private String listId;

    /**
     * 邀请人id
     */
    private Long inviteUserId;

    /**
     * 被邀请人id
     */
    private Long inviteToUserId;

    /**
     * 处理人
     */
    private Long handleUserId;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 处理时间
     */
    private Long handleTime;

    /**
     * 是否已读
     */
    private Integer isReader;

    /**
     * 邀请时间
     */
    private Long time;


}
