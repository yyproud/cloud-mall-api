package com.yang.im.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * chatList视图对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatListVo extends BaseVo {


    /**
     *
     */
    private Long id;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 消息列表
     */
    private String listId;

    /**
     * 类型：0单聊 1群聊
     */
    private Integer type;

    /**
     * 置顶
     */
    private Integer top;

    /**
     * 置顶时间
     */
    private Long topTime;

    /**
     * 未读消息数量
     */
    private Integer noReaderNum;

    /**
     * 上次聊天时间
     */
    private Long lastChatTime;

    /**
     * 是否免打扰 0否 1是
     */
    private Integer isDisturb;


}
