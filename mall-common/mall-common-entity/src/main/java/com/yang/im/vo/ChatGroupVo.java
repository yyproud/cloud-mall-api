package com.yang.im.vo;

import com.yang.common.mybatis.core.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * chatGroup视图对象
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class ChatGroupVo extends BaseVo {


    /**
     *
     */
    private Long id;

    /**
     *
     */
    private String listId;

    /**
     * 管理员id
     */
    private Long mainId;

    /**
     * 群名称
     */
    private String name;

    /**
     * 群公告
     */
    private String notice;

    /**
     * 是否禁言：0 否 1是
     */
    private Integer isMsg;

    /**
     * 群内加好友 0否 1是
     */
    private Integer canGetBigred;


}
