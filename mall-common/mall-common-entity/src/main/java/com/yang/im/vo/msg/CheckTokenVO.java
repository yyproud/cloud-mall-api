package com.yang.im.vo.msg;

import lombok.Data;

import java.io.Serializable;

/**
 * 检查token出参
 *
 * @Author: yang
 * @CreateTime: 2024-10-21
 */
@Data
public class CheckTokenVO implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 错误消息
     */
    private String err;
}
