package com.yang.im.entity.msg;

import lombok.Data;

import java.io.Serializable;

/**
 * 文本消息体
 *
 * @Author: yang
 * @CreateTime: 2024-10-27
 */
@Data
public class TextMsgEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    private String text;
}
