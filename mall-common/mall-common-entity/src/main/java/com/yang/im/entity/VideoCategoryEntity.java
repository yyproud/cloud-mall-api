package com.yang.im.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.yang.common.mybatis.core.entity.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 短视频分类对象
 *
 * @author yang
 * @email
 * @create 2024/10/20 20:11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("im_video_category")
public class VideoCategoryEntity extends BaseEntity {


    /**
     *
     */
    @TableId(value = "id")
    private Long id;
    /**
     *
     */
    private String name;

}
