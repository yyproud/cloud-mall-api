package com.yang.im.event;

import com.yang.im.vo.ChatLinkInfoVo;
import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * 用户下线监听事件
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/29 16:50
 */
@Getter
public class UserOfflineEvent extends ApplicationEvent {

    private final ChatLinkInfoVo chatLinkInfoVo;

    public UserOfflineEvent(Object source, ChatLinkInfoVo chatLinkInfoVo) {
        super(source);
        this.chatLinkInfoVo = chatLinkInfoVo;
    }
}
