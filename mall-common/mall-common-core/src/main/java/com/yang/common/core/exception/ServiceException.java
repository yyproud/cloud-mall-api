package com.yang.common.core.exception;

import com.yang.common.core.constant.HttpStatus;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/24 9:49
 */
public class ServiceException extends RuntimeException {
    private int code;
    private String message;

    public ServiceException() {
    }

    public ServiceException(String message) {
        super(message);
        this.code = HttpStatus.ERROR;
        this.message = message;
    }

    public ServiceException(int code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public ServiceException(BaseCodeEnum baseCodeEnum) {
        this(baseCodeEnum.getCode(), baseCodeEnum.getMessage());
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
