package com.yang.common.core.constant;

/**
 * 代码生成常量类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/20 9:56
 */
public interface GeneratorConstants {

    /**
     * 字符串类型
     */
    String TYPE_STRING = "String";
    /**
     * 时间类型
     */
    String TYPE_DATE = "Date";
    /**
     * 高精度计算类型
     */
    String TYPE_BIGDECIMAL = "BigDecimal";
    /**
     * 整型
     */
    String TYPE_INTEGER = "Integer";
    /**
     * 长整型
     */
    String TYPE_LONG = "Long";

    /**
     * 数据库字符串类型
     */
    String[] COLUMN_TYPE_STR = {"char", "varchar", "nvarchar", "varchar2"};
    /**
     * 数据库文本类型
     */
    String[] COLUMN_TYPE_TEXT = {"tinytext", "text", "mediumtext", "longtext"};
    /**
     * 数据库时间类型
     */
    String[] COLUMN_TYPE_TIME = {"datetime", "time", "date", "timestamp"};
    /**
     * 数据库数字类型
     */
    String[] COLUMN_TYPE_NUMBER = {"tinyint", "smallint", "mediumint", "int", "number", "integer", "bigint", "float", "double", "decimal"};

    /**
     * BO对象 不需要添加字段
     */
    String[] COLUMN_NAME_NOT_ADD = {"create_id", "create_time", "is_del"};
    /**
     * BO对象 不需要编辑字段
     */
    String[] COLUMN_NAME_NOT_EDIT = {"create_id", "create_time", "is_del"};
    /**
     * VO对象 不需要返回字段
     */
    String[] COLUMN_NAME_NOT_LIST = {"create_id", "create_time", "is_del"};
    /**
     * BO对象 不需要查询字段
     */
    String[] COLUMN_NAME_NOT_QUERY = {"create_id", "create_time", "is_del", "remark"};
    /**
     * Tree基类字段
     */
    String[] TREE_ENTITY = {"parentName", "parentId", "children"};
    /**
     * Entity基类字段
     */
    String[] BASE_ENTITY = {"createId", "createTime", "isDel"};
    /**
     * 单选框
     */
    String HTML_RADIO = "radio";
    /**
     * 文本域
     */
    String HTML_TEXTAREA = "textarea";
    /**
     * 下拉框
     */
    String HTML_SELECT = "select";
    /**
     * 图片上传控件
     */
    String HTML_IMAGE_UPLOAD = "imageUpload";
    /**
     * 文件上传控件
     */
    String HTML_FILE_UPLOAD = "fileUpload";
    /**
     * 文本框
     */
    String HTML_INPUT = "input";
    /**
     * 日期控件
     */
    String HTML_DATETIME = "datetime";
    /**
     * 富文本控件
     */
    String HTML_EDITOR = "editor";
    /**
     * 复选框
     */
    String HTML_CHECKBOX = "checkbox";

    /**
     * 相等查询
     */
    String QUERY_EQ = "EQ";
    /**
     * 模糊查询
     */
    String QUERY_LIKE = "LIKE";

    /**
     * 需要
     */
    String REQUIRE = "1";

    /**
     * 主子表（增删改查）
     */
    String TPL_SUB = "sub";
    /**
     * 树表（增删改查）
     */
    String TPL_TREE = "tree";
    /**
     * 树编码字段
     */
    String TREE_CODE = "treeCode";
    /**
     * 树父编码字段
     */
    String TREE_PARENT_CODE = "treeParentCode";
    /**
     * 树名称字段
     */
    String TREE_NAME = "treeName";
    /**
     * 上级菜单ID字段
     */
    String PARENT_MENU_ID = "parentMenuId";

    /**
     * 上级菜单名称字段
     */
    String PARENT_MENU_NAME = "parentMenuName";

    /**
     * 单表（增删改查）
     */
    String TPL_CRUD = "crud";
}
