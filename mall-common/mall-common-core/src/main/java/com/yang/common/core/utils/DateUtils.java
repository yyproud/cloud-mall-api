package com.yang.common.core.utils;

import cn.hutool.core.date.DateUtil;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间工具类
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/22 15:32
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class DateUtils extends org.apache.commons.lang3.time.DateUtils{

    public static final String YYYY_MM_DD = "yyyy-MM-dd";
    public static final String FILE_YYYY_MM_DD = "yyyy/MM/dd";

    /**
     * 获取当前日期, 默认格式为yyyy-MM-dd
     *
     * @return String
     */
    public static String getDate() {
        return dateTimeNow(YYYY_MM_DD);
    }
    public static String getDate(String format) {
        return dateTimeNow(format);
    }
    public static String dateTimeNow(final String format) {
        return parseDateToStr(format, new Date());
    }
    public static String parseDateToStr(final String format, final Date date) {
        return new SimpleDateFormat(format).format(date);
    }

    /**
     * 日期格式化 日期格式为：yyyy-MM-dd
     * @param date  日期
     * @param pattern  格式，如：DateUtils.DATE_TIME_PATTERN
     * @return  返回yyyy-MM-dd格式日期
     */
    public static String format(Date date, String pattern) {
        return DateUtil.format(date, pattern);
    }
    /**
     * 获取今日年份
     * @return 年份
     */
    public static String getYear(){
        return format(new Date(), "yyyy");
    }
    /**
     * 获取今日月份
     * @return 月份
     */
    public static String getMonth(){
        return format(new Date(), "MM");
    }
    /**
     * 获取今日日期
     * @return 日期
     */
    public static String getDay(){
        return format(new Date(), "dd");
    }

    public static void main(String[] args) {
        System.err.println(getDate("yyyy/MM/dd HH:mm"));;
    }
}
