package com.yang.common.core.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import com.yang.common.core.constant.HttpStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回结果
 *
 * @author 杨旭
 * @email 2795210596@qq.com
 * @create 2023/5/6 9:39
 */
public class Result extends HashMap<String, Object> {
    private static final long serialVersionUID = 1L;

    /**
     * 状态码
     */
    public static final String CODE_TAG = "code";

    /**
     * 返回内容
     */
    public static final String MSG_TAG = "message";
    /**
     * 返回内容-成功
     */
    public static final String MSG_TAG_OK = "操作成功";
    /**
     * 返回内容-失败
     */
    public static final String MSG_TAG_ERROR = "操作失败";

    /**
     * 数据对象
     */
    public static final String DATA_TAG = "data";

    /**
     * 创建返回结果
     */
    public Result() {
    }

    /**
     * 创建常规的返回结果
     *
     * @param code 状态码
     * @param msg  返回内容
     */
    public Result(int code, String msg) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
    }

    /**
     * 创建带数据的返回结果
     *
     * @param code 状态码
     * @param msg  返回内容
     * @param data 数据对象
     */
    public Result(int code, String msg, Object data) {
        super.put(CODE_TAG, code);
        super.put(MSG_TAG, msg);
        if (StringUtils.isNotNull(data)) {
            super.put(DATA_TAG, data);
        }
    }

    /**
     * 返回成功方法
     *
     * @return 返回结果
     */
    public static Result ok() {
        return Result.ok(MSG_TAG_OK);
    }

    /**
     * 返回成功方法,携带数据
     *
     * @param data 数据对象
     * @return 返回结果
     */
    public static Result ok(Object data) {
        return Result.ok(MSG_TAG_OK, data);
    }

    /**
     * 返回成功方法,自定义内容
     *
     * @param msg 返回内容
     * @return 返回结果
     */
    public static Result ok(String msg) {
        return Result.ok(msg, null);
    }

    /**
     * 返回成功消息
     *
     * @param msg  返回内容
     * @param data 数据对象
     * @return 返回结果
     */
    public static Result ok(String msg, Object data) {
        return new Result(HttpStatus.SUCCESS, msg, data);
    }

    /**
     * 返回失败消息，自定义内容
     *
     * @param msg 返回内容
     * @return 返回结果
     */
    public static Result error(String msg) {
        return new Result(HttpStatus.ERROR, msg);
    }

    /**
     * 返回失败消息，自定义状态码与内容
     *
     * @param code 状态码
     * @param msg  返回内容
     * @return 返回结果
     */
    public static Result error(int code, String msg) {
        return new Result(code, msg);
    }

    /**
     * 返回错误消息
     *
     * @return 返回结果
     */
    public static Result error() {
        return Result.error(MSG_TAG_ERROR);
    }

    /**
     * 返回String类型Json消息
     *
     * @param msg 返回内容
     * @return 返回结果
     */
    public static String okJson(String msg) {
        Map<String, Object> objectMap = new HashMap<>(16);
        objectMap.put(CODE_TAG, HttpStatus.SUCCESS);
        objectMap.put(MSG_TAG, msg);
        return JSONObject.toJSONString(objectMap);
    }

    /**
     * 返回String类型Json消息
     *
     * @param data 返回数据
     * @return 返回结果
     */
    public static String okJson(Object data) {
        Map<String, Object> objectMap = new HashMap<>(16);
        objectMap.put(CODE_TAG, HttpStatus.SUCCESS);
        objectMap.put(MSG_TAG, MSG_TAG_OK);
        objectMap.put(DATA_TAG, data);
        return JSON.toJSONString(
                objectMap,
                SerializerFeature.WriteNullListAsEmpty,
                SerializerFeature.WriteMapNullValue
        );
    }

    /**
     * 返回String类型Json消息
     *
     * @param msg  返回内容
     * @param data 返回数据
     * @return 返回结果
     */
    public static String okJson(String msg, Object data) {
        Map<String, Object> objectMap = new HashMap<>(16);
        objectMap.put(CODE_TAG, HttpStatus.SUCCESS);
        objectMap.put(MSG_TAG, msg);
        objectMap.put(DATA_TAG, data);
        return JSONObject.toJSONString(objectMap);
    }

    /**
     * 返回String类型Json消息
     *
     * @param code 状态码
     * @param msg  返回内容
     * @param data 返回数据
     * @return 返回结果
     */
    public static String errorJson(int code, String msg, Object data) {
        Map<String, Object> objectMap = new HashMap<>(16);
        objectMap.put(CODE_TAG, code);
        objectMap.put(MSG_TAG, msg);
        objectMap.put(DATA_TAG, data);
        return JSONObject.toJSONString(objectMap);
    }

    /**
     * 返回String类型Json消息
     *
     * @param code 状态码
     * @param msg  返回内容
     * @return 返回结果
     */
    public static String errorJson(int code, String msg) {
        Map<String, Object> objectMap = new HashMap<>(16);
        objectMap.put(CODE_TAG, code);
        objectMap.put(MSG_TAG, msg);
        return JSONObject.toJSONString(objectMap);
    }

    /**
     * 返回String类型Json
     *
     * @param code 状态码
     * @param msg  返回内容
     * @return 返回结果
     */
    public String stringJson(int code, String msg) {
        Map<String, Object> objectMap = new HashMap<>(16);
        objectMap.put(CODE_TAG, code);
        objectMap.put(MSG_TAG, msg);
        return JSONObject.toJSONString(objectMap);
    }

    @Override
    public Result put(String key, Object value) {
        super.put(key, value);
        return this;
    }
}
