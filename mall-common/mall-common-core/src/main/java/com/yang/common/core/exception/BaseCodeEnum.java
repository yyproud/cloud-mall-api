package com.yang.common.core.exception;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/24 9:51
 */
public interface  BaseCodeEnum {
    /**
     * 获取错误码
     * @return 错误码
     */
    int getCode();

    /**
     * 获取错误提示
     * @return 错误提示
     */
    String getMessage();
}
