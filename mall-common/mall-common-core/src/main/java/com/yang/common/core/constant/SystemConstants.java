package com.yang.common.core.constant;

/**
 * 系统服务常量
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/2 21:45
 */
public interface SystemConstants {
    /**
     * 管理员ID
     */
    Long ADMIN_ID = 1L;
}
