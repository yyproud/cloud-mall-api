package com.yang.common.security.configuration;

import cn.dev33.satoken.filter.SaServletFilter;
import cn.dev33.satoken.interceptor.SaInterceptor;
import cn.dev33.satoken.same.SaSameUtil;
import com.yang.common.core.constant.HttpStatus;
import com.yang.common.core.utils.Result;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/23 15:48
 */
@AutoConfiguration
public class SecurityConfiguration implements WebMvcConfigurer {
    /**
     * 注册sa-token的拦截器
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        // 注册路由拦截器，自定义验证规则
        registry.addInterceptor(new SaInterceptor()).addPathPatterns("/**");
    }

    /**
     * 校验是否从网关转发
     */
    @Bean
    public SaServletFilter getSaServletFilter() {
        return new SaServletFilter()
                .addInclude("/**")
                .addExclude("/actuator/**")
                .setAuth(obj -> SaSameUtil.checkCurrentRequestToken())
                .setError(e -> new Result().stringJson(HttpStatus.UNAUTHORIZED, "请通过正规途径访问系统资源!"));
    }
}
