package com.yang.common.security.handler;

import cn.dev33.satoken.exception.NotLoginException;
import cn.dev33.satoken.exception.NotPermissionException;
import cn.dev33.satoken.exception.NotRoleException;
import cn.dev33.satoken.exception.SameTokenInvalidException;
import com.yang.common.core.constant.HttpStatus;
import com.yang.common.core.exception.ServiceException;
import com.yang.common.core.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 异常翻译器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/23 17:03
 */
@Slf4j
@RestControllerAdvice
public class SecurityExceptionHandler {
    /**
     * 权限码异常
     */
    @ExceptionHandler(NotPermissionException.class)
    public Result handleNotPermissionException(NotPermissionException e, HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        log.error("请求地址'{}',权限码校验失败'{}'", requestUrl, e.getMessage());
        return Result.error(HttpStatus.FORBIDDEN, "没有访问权限，请联系管理员授权");
    }

    /**
     * 角色权限异常
     */
    @ExceptionHandler(NotRoleException.class)
    public Result handleNotRoleException(NotRoleException e, HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        log.error("请求地址'{}',角色权限校验失败'{}'", requestUrl, e.getMessage());
        return Result.error(HttpStatus.FORBIDDEN, "没有访问权限，请联系管理员授权");
    }

    /**
     * 认证失败
     */
    @ExceptionHandler(NotLoginException.class)
    public Result handleNotLoginException(NotLoginException e, HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        log.error("请求地址'{}',认证失败'{}',无法访问系统资源", requestUrl, e.getMessage());
        return Result.error(HttpStatus.UNAUTHORIZED, "认证失败，无法访问系统资源");
    }

    /**
     * 无效认证
     */
    @ExceptionHandler(SameTokenInvalidException.class)
    public Result handleSameTokenInvalidException(SameTokenInvalidException e, HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        log.error("请求地址'{}',内网认证失败'{}',无法访问系统资源", requestUrl, e.getMessage());
        return Result.error(HttpStatus.UNAUTHORIZED, "认证失败，无法访问系统资源");
    }

    /**
     * 请求方式不支持
     *
     * @param e       异常体
     * @param request 请求体
     * @return 定义异常内容
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public Result handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException e,
                                                      HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        log.info("请求地址'{}',不支持'{}'请求", requestUrl, e.getMessage());
        return Result.error(HttpStatus.BAD_METHOD, "请求地址不支持该请求类型!");
    }

    /**
     * 自定义业务异常
     *
     * @param e       异常体
     * @param request 请求体
     * @return 定义异常内容
     */
    @ExceptionHandler(ServiceException.class)
    public Result handleServiceException(ServiceException e, HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        log.info("请求地址:'{}',自定义业务异常:'{}'", requestUrl, e.getMessage());
        return Result.error(e.getCode(), e.getMessage());
    }

    /**
     * 自定义验证异常
     *
     * @param e       异常体
     * @param request 请求体
     * @return 定义异常内容
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Result handleMethodArgumentNotValidException(MethodArgumentNotValidException e, HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        String message = allErrors.stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(","));
        log.info("请求地址:'{}',异常内容:'参数校验失败!',失败详情:'{}'", requestUrl, message);
        return Result.error(HttpStatus.BAD_REQUEST, message);
    }

    /**
     * 自定义入参异常
     *
     * @param request 请求体
     * @return 定义异常内容
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Result httpMessageNotReadableException(HttpServletRequest request) {
        String requestUrl = request.getRequestURI();
        log.info("请求地址:'{}',异常内容:'请求参数缺失或请求参数为空'", requestUrl);
        return Result.error(HttpStatus.BAD_REQUEST, "请求参数缺失或请求参数为空!");
    }
}
