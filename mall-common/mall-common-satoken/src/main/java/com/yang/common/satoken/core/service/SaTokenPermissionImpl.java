package com.yang.common.satoken.core.service;

import cn.dev33.satoken.stp.StpInterface;
import com.yang.common.satoken.utils.LoginHelper;
import com.yang.system.vo.LoginVo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * sa-token 权限管理实现类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/27 20:33
 */
public class SaTokenPermissionImpl implements StpInterface {
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        LoginVo loginUser = LoginHelper.getLoginUser();
        Set<String> menuPermission = loginUser.getMenuPermission();
        return new ArrayList<> (menuPermission);
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        return null;
    }
}
