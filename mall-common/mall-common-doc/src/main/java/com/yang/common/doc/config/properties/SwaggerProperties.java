package com.yang.common.doc.config.properties;


import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/12/13 11:25
 */
@ConfigurationProperties(prefix = "swagger")
public class SwaggerProperties {
    /**
     * 标题
     */
    private String title;
    /**
     * 联系人信息
     */
    private String name;
    private String url;
    private String email;
    /**
     * 版本
     */
    private String version;
    /**
     * 描述
     */
    private String description;
    /**
     * 扫描的包路径
     */
    private String basePackage;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBasePackage() {
        return basePackage;
    }

    public void setBasePackage(String basePackage) {
        this.basePackage = basePackage;
    }
}
