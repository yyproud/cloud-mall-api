package com.yang.auth.service;

import com.yang.system.dto.LoginDto;
import com.yang.system.dto.RegisterDto;
import com.yang.system.vo.LoginVo;

/**
 * 登录服务
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/4 16:25
 */
public interface LoginService {

    /**
     * 用户登陆
     *
     * @param loginDto 登陆信息
     * @return 详细信息
     */
    LoginVo login(LoginDto loginDto);

    /**
     * 用户注册
     *
     * @param registerDto 注册信息
     * @return 详细信息
     */
    LoginVo register(RegisterDto registerDto);

    /**
     * 登出
     */
    void logout();
}
