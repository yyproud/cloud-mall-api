package com.yang.auth.service.impl;

import com.wf.captcha.*;
import com.wf.captcha.base.Captcha;
import com.yang.auth.config.properties.ValidateCodeProperties;
import com.yang.auth.service.ValidateCodeService;
import com.yang.common.redis.utils.RedisUtils;
import com.yang.common.core.exception.ServiceException;
import com.yang.common.core.utils.Result;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.time.Duration;


/**
 * 验证码服务实现类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/3 17:38
 */
@Service
public class ValidateCodeServiceImpl implements ValidateCodeService {

    @Resource
    ValidateCodeProperties validateCodeProperties;

    @Override
    public Result create(String key) {
        // 校验是否输入KEY
        if (StringUtils.isBlank(key)) {
            throw new ServiceException("验证码KEY不能为空!");
        }
        // 创建验证码
        Captcha captcha = createCaptcha();
        // 存入数据库
        RedisUtils.setCacheObject(ValidateCodeProperties.CODE_PREFIX + key, captcha.text(), Duration.ofSeconds(validateCodeProperties.getTime()));
        return Result.ok().put("data", captcha.toBase64());
    }

    @Override
    public void check(String key, String code) {
        if (StringUtils.isBlank(code)) {
            throw new ServiceException("请输入验证码!");
        }
        if (StringUtils.isBlank(key)) {
            throw new ServiceException("请输入验证码KEY!");
        }
        String redisKey = ValidateCodeProperties.CODE_PREFIX + key;
        String redisCode = RedisUtils.getCacheObject(redisKey);
        if (null == redisCode) {
            throw new ServiceException("验证码已过期!");
        }
        if (!StringUtils.equalsIgnoreCase(code, redisCode)) {
            throw new ServiceException("验证码不正确!");
        }
        // 校验成功清除code
        RedisUtils.deleteObject(redisKey);
    }

    /**
     * 根据类型生成验证码
     *
     * @return 验证码
     */
    private Captcha createCaptcha() {
        Captcha captcha;
        Integer width = validateCodeProperties.getWidth();
        Integer height = validateCodeProperties.getHeight();
        Integer length = validateCodeProperties.getLength();
        Integer charType = validateCodeProperties.getCharType();
        switch (validateCodeProperties.getType()) {
            case ValidateCodeProperties.ARITHMETIC_CAPTCHA:
                captcha = new ArithmeticCaptcha(width, height);
                captcha.setLen(2);
                break;
            case ValidateCodeProperties.CHINESE_CAPTCHA:
                captcha = new ChineseCaptcha(width, height, length);
                break;
            case ValidateCodeProperties.CHINESE_GIF_CAPTCHA:
                captcha = new ChineseGifCaptcha(width, height, length);
                break;
            case ValidateCodeProperties.GIF_CAPTCHA:
                captcha = new GifCaptcha(width, height, length);
                captcha.setCharType(charType);
                break;
            default:
                captcha = new SpecCaptcha(width, height, length);
                captcha.setCharType(charType);
                break;
        }
        return captcha;
    }
}
