package com.yang.auth.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.yang.api.system.dubbo.RangedSysUserService;
import com.yang.auth.config.properties.ValidateCodeProperties;
import com.yang.auth.service.LoginService;
import com.yang.auth.service.ValidateCodeService;
import com.yang.common.satoken.utils.LoginHelper;
import com.yang.system.dto.LoginDto;
import com.yang.system.dto.RegisterDto;
import com.yang.system.vo.LoginVo;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 登录服务实现类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/4 16:25
 */
@Service
public class LoginServiceImpl implements LoginService {

    @DubboReference
    RangedSysUserService rangedSysUserService;
    @Resource
    ValidateCodeService validateCodeService;
    @Resource
    ValidateCodeProperties validateCodeProperties;

    @Override
    public LoginVo login(LoginDto loginDto) {
        if (!"H5".equals(loginDto.getUserType())) {
            // 判断是否开启验证码
            if (validateCodeProperties.getIsEnabled()) {
                // 校验验证码
                validateCodeService.check(loginDto.getKey(), loginDto.getCode());
            }
        }
        // 获取登录信息
        LoginVo loginVo = rangedSysUserService.getByUserName(loginDto);
        // 登录信息保存
        LoginHelper.login(loginVo);
        // 获取保存的TOKEN
        loginVo.setToken(StpUtil.getTokenValue());
        // 返回登录视图
        return loginVo;
    }

    @Override
    public LoginVo register(RegisterDto registerDto) {
        // 注册账号
        LoginVo loginVo = rangedSysUserService.register(registerDto);
        // 登录信息保存
        LoginHelper.login(loginVo);
        // 获取保存的TOKEN
        loginVo.setToken(StpUtil.getTokenValue());
        // 返回登录视图
        return loginVo;
    }

    @Override
    public void logout() {
        StpUtil.logout();
    }
}