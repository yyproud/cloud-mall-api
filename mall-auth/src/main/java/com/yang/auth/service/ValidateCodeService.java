package com.yang.auth.service;

import com.yang.common.core.exception.ServiceException;
import com.yang.common.core.utils.Result;

import javax.servlet.http.HttpServletRequest;

/**
 * 验证码服务
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/3 17:38
 */
public interface ValidateCodeService {

    /**
     * 生成验证码
     *
     * @param key 获取参数
     * @return 验证码
     * @throws ServiceException 自定义异常
     */
    Result create(String key) throws ServiceException;

    /**
     * 校验验证码
     *
     * @param key  前端上送 key
     * @param code 前端上送待校验值
     * @throws ServiceException 自定义异常
     */
    void check(String key, String code) throws ServiceException;
}
