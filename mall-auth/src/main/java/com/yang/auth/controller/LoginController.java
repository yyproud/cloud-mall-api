package com.yang.auth.controller;

import cn.dev33.satoken.secure.BCrypt;
import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.yang.auth.config.CaptChaBlockHandler;
import com.yang.auth.service.LoginService;
import com.yang.auth.service.ValidateCodeService;
import com.yang.common.core.utils.Result;
import com.yang.system.dto.LoginDto;
import com.yang.system.dto.RegisterDto;
import com.yang.system.vo.LoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;

/**
 * 登录控制器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/4 16:10
 */
@Api(tags = "登录控制器")
@RestController
public class LoginController {

    @Resource
    LoginService loginService;
    @Resource
    ValidateCodeService validateCodeService;

    /**
     * 获取验证码
     * @param key 获取参数
     * @return 验证码
     */
    @ApiOperation(value = "获取验证码", notes = "根据")
    @GetMapping("captcha/{key}")
    @SentinelResource(
            value = "captcha",
            blockHandlerClass = CaptChaBlockHandler.class,
            blockHandler = "handleException",
            fallbackClass = CaptChaBlockHandler.class,
            fallback = "handleError"
    )
    public Result captcha(@NotNull(message = "验证码KEY不能为空!") @PathVariable String key) {
        return validateCodeService.create(key);
    }

    /**
     * 账号密码登录
     *
     * @param loginDto 登录参数
     * @return 登录视图
     */
    @PostMapping("/loginByPwd")
    public Result login(@RequestBody LoginDto loginDto) {
        LoginVo login = loginService.login(loginDto);
        return Result.ok(login);
    }

    @PostMapping("/registerByPwd")
    public Result registerByPwd(@RequestBody RegisterDto registerDto) {
        LoginVo login = loginService.register(registerDto);
        return Result.ok(login);
    }


    /**
     * 登出方法
     */
    @DeleteMapping("logout")
    public Result logout() {
        loginService.logout();
        return Result.ok();
    }

    public static void main(String[] args) {
        String hashpw = BCrypt.hashpw("123");
        System.err.println(hashpw);
    }

}
