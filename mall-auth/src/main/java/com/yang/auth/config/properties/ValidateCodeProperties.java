package com.yang.auth.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * 验证码基础属性实体类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/3 17:38
 */
@Data
@Component
@ConfigurationProperties(prefix = "validate-code")
public class ValidateCodeProperties {

    /**
     * 登录时是否启用验证码
     */
    private Boolean isEnabled = false;

    /**
     * 验证码有效时间，单位秒
     */
    private Long time = 180L;
    /**
     * 验证码类型 默认图片
     */
    private String type = "specCaptcha";
    /**
     * 图片宽度，px
     */
    private Integer width = 130;
    /**
     * 图片高度，px
     */
    private Integer height = 48;
    /**
     * 验证码位数
     */
    private Integer length = 4;
    /**
     * 验证码值的类型
     * 1. 数字加字母
     * 2. 纯数字
     * 3. 纯字母
     */
    private Integer charType = 2;

    /**
     * 数字加减乘除验证
     */
    public static final String ARITHMETIC_CAPTCHA = "arithmeticCaptcha";
    /**
     * 中文验证
     */
    public static final String CHINESE_CAPTCHA = "chineseCaptcha";
    /**
     * 中文动态验证
     */
    public static final String CHINESE_GIF_CAPTCHA = "chineseGifCaptcha";
    /**
     * 动态字符验证
     */
    public static final String GIF_CAPTCHA = "gifCaptcha";
    /**
     * 字符验证
     */
    public static final String SPEC_CAPTCHA = "specCaptcha";
    /**
     * 验证码 key前缀
     */
    public static final String CODE_PREFIX = "captcha.";
}
