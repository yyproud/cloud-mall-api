/*
 Navicat Premium Data Transfer

 Source Server         : 211.101.247.16
 Source Server Type    : MySQL
 Source Server Version : 80035
 Source Host           : 211.101.247.16:3306
 Source Schema         : cloud_mall

 Target Server Type    : MySQL
 Target Server Version : 80035
 File Encoding         : 65001

 Date: 28/12/2023 13:56:07
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for generator_table
-- ----------------------------
DROP TABLE IF EXISTS `generator_table`;
CREATE TABLE `generator_table`  (
  `table_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表名称',
  `table_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表描述',
  `sub_table_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '实体类名称',
  `tpl_category` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'crud' COMMENT '使用的模板（crud:单表操作 tree:树表操作 sub:主子表）',
  `package_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成功能作者',
  `generator_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '生成代码方式（0:zip压缩包 1:自定义路径）',
  `generator_path` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '其它生成选项',
  `auto_remove_pre` tinyint(1) NULL DEFAULT NULL COMMENT '自动去除表前缀 (1:是 2:否)',
  `table_prefix` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表前缀',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `is_del` tinyint NULL DEFAULT 0 COMMENT '删除标识',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户描述',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of generator_table
-- ----------------------------
INSERT INTO `generator_table` VALUES (1, 'sys_dict_data', '字典数据表', NULL, NULL, 'SysDictData', 'crud', 'com.yang.system', 'system', 'dictData', '字典数据', '杨旭', '0', NULL, NULL, NULL, NULL, 1, '2023-10-09 13:48:22', -1, NULL);
INSERT INTO `generator_table` VALUES (2, 'sys_dict_type', '字典类型表', NULL, NULL, 'SysDictType', 'crud', 'com.yang.system', 'system', 'dictType', '字典类型', '杨旭', '0', NULL, '{\"treeCode\":null,\"treeName\":null,\"treeParentCode\":null,\"parentMenuId\":\"2\"}', NULL, NULL, 1, '2023-10-09 13:48:23', -1, NULL);
INSERT INTO `generator_table` VALUES (3, 'sys_dict_data', '字典数据表', NULL, NULL, 'SysDictData', 'crud', 'com.yang.system', 'system', 'dictData', '字典数据', '杨旭', '0', NULL, NULL, NULL, NULL, 1, '2023-10-16 15:08:06', -1, NULL);
INSERT INTO `generator_table` VALUES (4, 'sys_dict_type', '字典类型表', NULL, NULL, 'SysDictType', 'crud', 'com.yang.system', 'system', 'dictType', '字典类型', '杨旭', '0', NULL, NULL, NULL, NULL, 1, '2023-10-16 15:08:07', -1, NULL);
INSERT INTO `generator_table` VALUES (5, 'sys_menu', '系统菜单', NULL, NULL, 'SysMenu', 'crud', 'com.yang.system', 'system', 'menu', '系统菜单', '杨旭', '0', NULL, NULL, NULL, NULL, 1, '2023-10-16 15:09:53', -1, NULL);
INSERT INTO `generator_table` VALUES (6, 'sys_dict_data', '字典数据表', NULL, NULL, 'SysDictData', 'crud', 'com.yang.system', 'system', 'dictData', '字典数据', '杨旭', '0', NULL, '{\"treeCode\":null,\"treeName\":null,\"treeParentCode\":null}', NULL, NULL, 1, '2023-10-16 15:11:18', 0, NULL);
INSERT INTO `generator_table` VALUES (7, 'sys_dict_type', '字典类型表', NULL, NULL, 'SysDictType', 'crud', 'com.yang.system', 'system', 'dictType', '字典类型', '杨旭', '0', NULL, '{\"treeCode\":null,\"treeName\":null,\"treeParentCode\":null,\"parentMenuId\":null}', NULL, NULL, 1, '2023-10-16 15:11:19', 0, NULL);

-- ----------------------------
-- Table structure for generator_table_column
-- ----------------------------
DROP TABLE IF EXISTS `generator_table_column`;
CREATE TABLE `generator_table_column`  (
  `column_id` bigint NOT NULL AUTO_INCREMENT,
  `table_id` bigint NULL DEFAULT NULL COMMENT '业务表id',
  `column_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否主键',
  `is_increment` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否自增',
  `is_required` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否必填',
  `is_insert` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否为插入字段',
  `is_edit` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否编辑字段',
  `is_list` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否列表字段',
  `is_query` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '是否查询字段',
  `query_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '查询方式',
  `html_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '显示类型',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字典类型',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 82 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '代码生成业务字段表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of generator_table_column
-- ----------------------------
INSERT INTO `generator_table_column` VALUES (61, 6, 'dict_code', '字典编码', 'bigint', 'Long', 'dictCode', '1', '0', '1', '0', '1', '1', '0', 'EQ', 'input', NULL, 1);
INSERT INTO `generator_table_column` VALUES (62, 6, 'dict_sort', '字典排序', 'int', 'Long', 'dictSort', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'input', NULL, 2);
INSERT INTO `generator_table_column` VALUES (63, 6, 'dict_label', '字典标签', 'varchar(100)', 'String', 'dictLabel', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', NULL, 3);
INSERT INTO `generator_table_column` VALUES (64, 6, 'dict_value', '字典键值', 'varchar(100)', 'String', 'dictValue', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'input', NULL, 4);
INSERT INTO `generator_table_column` VALUES (65, 6, 'dict_type', '字典类型', 'varchar(100)', 'String', 'dictType', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'select', NULL, 5);
INSERT INTO `generator_table_column` VALUES (66, 6, 'css_class', '样式属性（其他样式扩展）', 'varchar(100)', 'String', 'cssClass', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'input', NULL, 6);
INSERT INTO `generator_table_column` VALUES (67, 6, 'list_class', '表格回显样式', 'varchar(100)', 'String', 'listClass', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'input', NULL, 7);
INSERT INTO `generator_table_column` VALUES (68, 6, 'is_default', '是否默认（Y是 N否）', 'char(1)', 'String', 'isDefault', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'input', NULL, 8);
INSERT INTO `generator_table_column` VALUES (69, 6, 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', NULL, 9);
INSERT INTO `generator_table_column` VALUES (70, 6, 'is_del', '删除标识', 'tinyint', 'Long', 'isDel', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', NULL, 10);
INSERT INTO `generator_table_column` VALUES (71, 6, 'create_id', '创建者', 'varchar(64)', 'String', 'createId', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', NULL, 11);
INSERT INTO `generator_table_column` VALUES (72, 6, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'datetime', NULL, 12);
INSERT INTO `generator_table_column` VALUES (73, 6, 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', '0', '1', '1', '1', '0', 'EQ', 'textarea', NULL, 13);
INSERT INTO `generator_table_column` VALUES (74, 7, 'dict_id', '字典主键', 'bigint', 'Long', 'dictId', '1', '1', '1', '0', '1', '1', '0', 'EQ', 'input', NULL, 1);
INSERT INTO `generator_table_column` VALUES (75, 7, 'dict_name', '字典名称', 'varchar(100)', 'String', 'dictName', '0', '0', '1', '1', '1', '1', '1', 'LIKE', 'input', NULL, 2);
INSERT INTO `generator_table_column` VALUES (76, 7, 'dict_type', '字典类型', 'varchar(100)', 'String', 'dictType', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'select', NULL, 3);
INSERT INTO `generator_table_column` VALUES (77, 7, 'status', '状态（0正常 1停用）', 'char(1)', 'String', 'status', '0', '0', '1', '1', '1', '1', '1', 'EQ', 'radio', NULL, 4);
INSERT INTO `generator_table_column` VALUES (78, 7, 'is_del', '删除标识', 'tinyint', 'Long', 'isDel', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', NULL, 5);
INSERT INTO `generator_table_column` VALUES (79, 7, 'create_id', '创建者', 'varchar(64)', 'String', 'createId', '0', '0', '0', '0', '0', '0', '0', 'EQ', 'input', NULL, 6);
INSERT INTO `generator_table_column` VALUES (80, 7, 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', '0', '0', '0', '0', '1', 'BETWEEN', 'datetime', NULL, 7);
INSERT INTO `generator_table_column` VALUES (81, 7, 'remark', '备注', 'varchar(500)', 'String', 'remark', '0', '0', '1', '1', '1', '1', '0', 'EQ', 'textarea', NULL, 8);

-- ----------------------------
-- Table structure for sys_dict_data
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data`  (
  `dict_code` bigint NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int NULL DEFAULT NULL COMMENT '字典排序',
  `dict_label` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `is_del` tinyint NULL DEFAULT 0 COMMENT '删除标识',
  `create_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE,
  UNIQUE INDEX `dict_value`(`dict_value` ASC, `dict_type` ASC, `is_del` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典数据表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES (1, 0, '正常', '0', 'sys_normal_disable', NULL, 'primary', 'Y', '0', 0, '1', '2023-10-18 15:01:59', '正常状态');
INSERT INTO `sys_dict_data` VALUES (2, 1, '停用', '1', 'sys_normal_disable', NULL, 'danger', 'N', '0', 0, '1', '2023-10-18 15:02:57', '停用状态');
INSERT INTO `sys_dict_data` VALUES (3, 1, '男', '1', 'sys_user_sex', NULL, 'primary', 'Y', '0', 0, '1', '2023-10-19 17:22:25', NULL);
INSERT INTO `sys_dict_data` VALUES (5, 2, '女', '2', 'sys_user_sex', NULL, 'success', 'N', '0', 0, '1', '2023-10-19 17:12:21', '');
INSERT INTO `sys_dict_data` VALUES (6, 0, '未知', '-1', 'sys_user_sex', NULL, 'warning', 'N', '0', 0, '1', '2023-10-19 17:18:27', NULL);
INSERT INTO `sys_dict_data` VALUES (10, 0, '显示', '0', 'sys_show_hide', NULL, 'primary', 'Y', '0', 0, '1', '2023-10-27 15:17:43', NULL);
INSERT INTO `sys_dict_data` VALUES (11, 0, '隐藏', '1', 'sys_show_hide', NULL, 'default', 'N', '0', 0, '1', '2023-10-27 15:18:52', NULL);

-- ----------------------------
-- Table structure for sys_dict_type
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type`  (
  `dict_id` bigint NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '字典类型',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `is_del` tinyint NULL DEFAULT 0 COMMENT '删除标识',
  `create_id` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE INDEX `dict_type`(`dict_type` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '字典类型表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES (1, '系统开关', 'sys_normal_disable', '0', 0, '1', '2023-10-18 15:00:21', '停用/启用');
INSERT INTO `sys_dict_type` VALUES (2, '用户性别', 'sys_user_sex', '0', 0, '1', '2023-10-19 15:14:42', '');
INSERT INTO `sys_dict_type` VALUES (6, '菜单状态', 'sys_show_hide', '0', 0, '1', '2023-10-27 15:16:17', '菜单状态列表');

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `menu_id` bigint NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单名称',
  `parent_id` bigint NULL DEFAULT 0 COMMENT '父菜单ID',
  `menu_sort` int NULL DEFAULT NULL COMMENT '显示顺序',
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由地址',
  `path_param` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '路由参数',
  `component` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '组件路径',
  `is_frame` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` tinyint NULL DEFAULT 0 COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '显示状态（0显示 1隐藏）',
  `menu_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '菜单图标',
  `is_del` tinyint NULL DEFAULT 0 COMMENT '删除标识',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统菜单' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, '系统管理', 0, 0, 'system', NULL, NULL, '1', 0, 'M', '0', '0', NULL, 'system', 0, 1, '2023-08-27 09:02:47', NULL);
INSERT INTO `sys_menu` VALUES (2, '用户管理', 1, 0, 'user', '', 'system/user/index', '1', 1, 'C', '0', '0', 'system:user:list', 'peoples', 0, 1, '2023-08-27 09:05:25', NULL);
INSERT INTO `sys_menu` VALUES (3, '用户查询', 2, 0, NULL, NULL, NULL, '1', 0, 'F', '0', '0', 'system:user:query', '#', 0, 1, '2023-08-27 09:13:42', NULL);
INSERT INTO `sys_menu` VALUES (4, '用户修改', 2, 1, NULL, NULL, NULL, '1', 0, 'F', '0', '0', 'system:user:edit', '#', 0, 1, '2023-09-05 10:46:04', NULL);
INSERT INTO `sys_menu` VALUES (5, '系统工具', 0, 1, 'tool', NULL, NULL, '1', 0, 'M', '0', '0', NULL, 'tool', 0, 1, '2023-09-06 14:40:10', NULL);
INSERT INTO `sys_menu` VALUES (6, '代码生成', 5, 1, 'gen', NULL, 'tool/gen/index', '1', 0, 'C', '0', '0', 'tool:gen:list', 'code', 0, 1, '2023-09-06 14:41:55', NULL);
INSERT INTO `sys_menu` VALUES (7, '仪表盘', 0, 2, 'dashboard', NULL, '', '1', 0, 'M', '0', '0', NULL, '#', -1, 1, '2023-09-16 18:20:48', NULL);
INSERT INTO `sys_menu` VALUES (8, '分析页', 7, 0, 'dashboard_analysis', NULL, '/dashboard/analysis', '1', 0, 'C', '0', '0', NULL, '#', -1, 1, '2023-09-16 18:20:50', NULL);
INSERT INTO `sys_menu` VALUES (9, '导入', 6, 0, NULL, NULL, NULL, '1', 0, 'F', '0', '0', 'tool:gen:import', '#', 0, 1, '2023-09-18 09:43:34', NULL);
INSERT INTO `sys_menu` VALUES (10, '字典管理', 1, 1, 'dictType', NULL, 'system/dictType/index', '1', 0, 'C', '0', '0', 'system:dictType:list', 'dict', 0, 1, '2023-10-09 14:27:10', '字典类型菜单');
INSERT INTO `sys_menu` VALUES (11, '字典类型查询', 10, 1, '#', NULL, '', '1', 0, 'F', '0', '0', 'system:dictType:query', '#', 0, 1, '2023-10-09 14:27:10', '');
INSERT INTO `sys_menu` VALUES (12, '字典类型新增', 10, 2, '#', NULL, '', '1', 0, 'F', '0', '0', 'system:dictType:add', '#', 0, 1, '2023-10-09 14:27:10', '');
INSERT INTO `sys_menu` VALUES (13, '字典类型修改', 10, 3, '#', NULL, '', '1', 0, 'F', '0', '0', 'system:dictType:edit', '#', 0, 1, '2023-10-09 14:27:10', '');
INSERT INTO `sys_menu` VALUES (14, '字典类型删除', 10, 4, '#', NULL, '', '1', 0, 'F', '0', '0', 'system:dictType:remove', '#', 0, 1, '2023-10-09 14:27:10', '');
INSERT INTO `sys_menu` VALUES (15, '字典类型导出', 10, 5, '#', NULL, '', '1', 0, 'F', '0', '0', 'system:dictType:export', '#', 0, 1, '2023-10-09 14:27:11', '');
INSERT INTO `sys_menu` VALUES (16, '菜单管理', 1, 2, 'menu', NULL, 'system/menu/index', '1', 0, 'C', '0', '0', 'system:menu:list', 'tree', 0, 1, '2023-10-27 13:37:20', NULL);
INSERT INTO `sys_menu` VALUES (17, '1', 0, 2, '2', NULL, NULL, '1', 0, 'M', '0', '0', NULL, 'button', -1, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (18, '聊天室', 0, 2, 'chat', NULL, NULL, '1', 0, 'M', '0', '0', NULL, 'message', 0, NULL, NULL, NULL);
INSERT INTO `sys_menu` VALUES (19, '聊天', 18, 0, 'ChatMse', NULL, 'chat/chat/chat', '1', 0, 'C', '0', '0', NULL, NULL, 0, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role`  (
  `role_id` bigint NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色名称',
  `role_key` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '角色权限字符串',
  `role_sort` int NULL DEFAULT NULL COMMENT '显示顺序',
  `data_scope` tinyint NULL DEFAULT NULL COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint NULL DEFAULT NULL COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint NULL DEFAULT NULL COMMENT '部门树选择项是否关联显示',
  `role_status` tinyint NULL DEFAULT 0 COMMENT '角色状态（0正常 1停用）',
  `is_del` tinyint NULL DEFAULT 0 COMMENT '删除标识',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统角色表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES (1, '超级管理员', 'Admin', 1, 1, 1, 1, 0, 0, 1, '2023-08-26 19:42:29', '超级管理员');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu`  (
  `role_id` bigint NOT NULL COMMENT '角色ID',
  `menu_id` bigint NOT NULL COMMENT '菜单ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色和菜单关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES (1, 1);
INSERT INTO `sys_role_menu` VALUES (1, 2);
INSERT INTO `sys_role_menu` VALUES (1, 3);
INSERT INTO `sys_role_menu` VALUES (1, 4);
INSERT INTO `sys_role_menu` VALUES (1, 5);
INSERT INTO `sys_role_menu` VALUES (1, 6);
INSERT INTO `sys_role_menu` VALUES (1, 7);
INSERT INTO `sys_role_menu` VALUES (1, 8);
INSERT INTO `sys_role_menu` VALUES (1, 9);
INSERT INTO `sys_role_menu` VALUES (1, 10);
INSERT INTO `sys_role_menu` VALUES (1, 11);
INSERT INTO `sys_role_menu` VALUES (1, 12);
INSERT INTO `sys_role_menu` VALUES (1, 13);
INSERT INTO `sys_role_menu` VALUES (1, 14);
INSERT INTO `sys_role_menu` VALUES (1, 15);
INSERT INTO `sys_role_menu` VALUES (1, 16);
INSERT INTO `sys_role_menu` VALUES (1, 17);
INSERT INTO `sys_role_menu` VALUES (1, 18);
INSERT INTO `sys_role_menu` VALUES (1, 19);

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user`  (
  `user_id` bigint NOT NULL AUTO_INCREMENT,
  `user_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户类型',
  `nick_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户姓名',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户账号',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户密码',
  `gender` tinyint NULL DEFAULT NULL COMMENT '用户性别',
  `mobile` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '联系方式',
  `avatar` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户头像',
  `status` tinyint NULL DEFAULT 0 COMMENT '用户状态 0:正常 1停用',
  `create_id` bigint NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `last_login_time` datetime NULL DEFAULT NULL COMMENT '最后登陆时间',
  `last_login_ip` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '最后登陆ip',
  `is_del` tinyint NULL DEFAULT 0 COMMENT '删除标识',
  `remark` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '用户描述',
  PRIMARY KEY (`user_id`) USING BTREE,
  UNIQUE INDEX `user_name`(`user_name` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统用户表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES (1, 'sys_user', '超级管理员', 'admin', '$2a$10$35xtn/DXoakVIAFGy.NBuOo7k5DwqHHGV5AX3osrhNHIYBIkA2ZiO', 1, '13144611866', NULL, 0, 1, '2023-08-25 15:35:04', NULL, NULL, 0, NULL);
INSERT INTO `sys_user` VALUES (2, 'sys_user', '杨旭', 'yangxu', '$2a$10$35xtn/DXoakVIAFGy.NBuOo7k5DwqHHGV5AX3osrhNHIYBIkA2ZiO', 1, '13144611866', NULL, 0, 1, '2023-11-27 16:04:50', NULL, NULL, 0, NULL);
INSERT INTO `sys_user` VALUES (3, 'sys_user', '张三', 'zhansan', '$2a$10$35xtn/DXoakVIAFGy.NBuOo7k5DwqHHGV5AX3osrhNHIYBIkA2ZiO', 1, '13144611866', NULL, 0, 1, '2023-11-28 14:58:46', NULL, NULL, 0, NULL);

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role`  (
  `user_id` bigint NOT NULL COMMENT '用户ID',
  `role_id` bigint NOT NULL COMMENT '角色ID'
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '用户和角色关联表' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES (1, 1);
INSERT INTO `sys_user_role` VALUES (2, 1);

SET FOREIGN_KEY_CHECKS = 1;
