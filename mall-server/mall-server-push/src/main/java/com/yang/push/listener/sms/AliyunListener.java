package com.yang.push.listener.sms;

import com.alibaba.fastjson.JSON;
import com.yang.system.entity.SysDictTypeEntity;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 阿里云短信监听器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/14 16:09
 */
@Component
@RocketMQMessageListener(topic = "mallPush", consumerGroup = "aliyun", messageModel = MessageModel.BROADCASTING, selectorExpression = "aliyun")
public class AliyunListener implements RocketMQListener<MessageExt> {
    @Override
    public void onMessage(MessageExt messageExt) {

        // 获取消息内容转成实体类
        int reconsumeTimes = messageExt.getReconsumeTimes();
        System.err.println("reconsumeTimes:" + reconsumeTimes);
        if (reconsumeTimes > 3) {
            return;
        }
        String a = "gdyujas";
        Integer b = Integer.parseInt(a);
        SysDictTypeEntity aliyunMessage = JSON.parseObject(new String(messageExt.getBody()), SysDictTypeEntity.class);
        System.err.println(aliyunMessage);
    }
}
