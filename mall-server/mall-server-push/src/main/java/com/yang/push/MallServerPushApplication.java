package com.yang.push;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 推送服务
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/1 10:12
 */
@EnableDubbo
@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
public class MallServerPushApplication {
    public static void main(String[] args) {
        System.setProperty("rocketmq.client.logUseSlf4j", "true");
        SpringApplication.run(MallServerPushApplication.class, args);
    }
}
