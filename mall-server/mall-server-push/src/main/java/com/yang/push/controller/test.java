package com.yang.push.controller;


import com.yang.common.core.utils.Result;
import com.yang.system.entity.SysMenuEntity;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/14 16:49
 */
@RestController
@RequestMapping("/test")
public class test {

    @Resource
    private RocketMQTemplate rocketmqtemplate;

    @GetMapping("/list")
    public Result list() {
        SysMenuEntity sysMenuEntity = new SysMenuEntity();
        sysMenuEntity.setMenuId(10L);
            rocketmqtemplate.send("mallPush:aliyun", MessageBuilder.withPayload(sysMenuEntity).build());
        return Result.ok();
    }
}
