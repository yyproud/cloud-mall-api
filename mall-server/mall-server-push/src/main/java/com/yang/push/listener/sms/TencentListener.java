package com.yang.push.listener.sms;

import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

/**
 * 腾讯云短信监听器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/14 16:12
 */
@Component
@RocketMQMessageListener(topic = "mallPush", consumerGroup = "tencent", messageModel = MessageModel.CLUSTERING, selectorExpression = "tencent")
public class TencentListener implements RocketMQListener<String> {
    @Override
    public void onMessage(String s) {

    }
}
