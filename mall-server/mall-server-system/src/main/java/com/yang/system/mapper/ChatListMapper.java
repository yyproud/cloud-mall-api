package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.ChatListEntity;
import com.yang.im.vo.ChatListVo;
import com.yang.im.dto.ChatListDto;

import java.util.List;

/**
 * chatListMapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface ChatListMapper extends BaseMapperPlus<ChatListMapper, ChatListEntity, ChatListVo> {

    /**
     * 查询chatList分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<ChatListVo> queryPageList(Page<ChatListVo> page, @Param("dto") ChatListDto dto);

    /**
     * 查询chatList列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<ChatListVo> queryList(@Param("dto") ChatListDto dto);
}
