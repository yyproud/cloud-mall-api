package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.yang.im.entity.ChatEntity;
import com.yang.im.vo.ChatVo;
import com.yang.im.dto.ChatDto;
import com.yang.system.mapper.ChatMapper;
import com.yang.system.service.IChatService;

import java.util.List;
import java.util.Collection;

/**
 * chatService业务层处理
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
@Service
public class ChatServiceImpl extends ServiceImpl<ChatMapper, ChatEntity> implements IChatService {

    /**
     * 查询chat
     */
    @Override
    public ChatVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询chat列表
     */
    @Override
    public PageView<ChatVo> queryPageList(ChatDto dto) {
        Page<ChatVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询chat列表
     */
    @Override
    public List<ChatVo> queryList(ChatDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增chat
     */
    @Override
    public Boolean insert(ChatDto dto) {
        ChatEntity add = BeanUtil.toBean(dto, ChatEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改chat
     */
    @Override
    public Boolean update(ChatDto dto) {
        ChatEntity update = BeanUtil.toBean(dto, ChatEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除chat信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
