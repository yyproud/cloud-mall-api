package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.ChatEntity;
import com.yang.im.vo.ChatVo;
import com.yang.im.dto.ChatDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * chatService接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface IChatService extends IService<ChatEntity>{

    /**
     * 查询chat
     *
     * @param id 参数
     * @return chat实体
     */
    ChatVo queryById(Long id);

    /**
     * 查询chat列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<ChatVo> queryPageList(ChatDto dto);

    /**
     * 查询chat列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<ChatVo> queryList(ChatDto dto);

    /**
     * 新增chat
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(ChatDto dto);

    /**
     * 修改chat
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(ChatDto dto);

    /**
     * 批量删除chat信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
