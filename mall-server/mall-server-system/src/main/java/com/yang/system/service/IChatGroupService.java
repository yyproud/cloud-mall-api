package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.ChatGroupEntity;
import com.yang.im.vo.ChatGroupVo;
import com.yang.im.dto.ChatGroupDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * chatGroupService接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface IChatGroupService extends IService<ChatGroupEntity>{

    /**
     * 查询chatGroup
     *
     * @param id 参数
     * @return chatGroup实体
     */
    ChatGroupVo queryById(Long id);

    /**
     * 查询chatGroup列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<ChatGroupVo> queryPageList(ChatGroupDto dto);

    /**
     * 查询chatGroup列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<ChatGroupVo> queryList(ChatGroupDto dto);

    /**
     * 新增chatGroup
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(ChatGroupDto dto);

    /**
     * 修改chatGroup
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(ChatGroupDto dto);

    /**
     * 批量删除chatGroup信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
