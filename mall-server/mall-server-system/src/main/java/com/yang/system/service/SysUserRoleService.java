package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.system.entity.SysUserRoleEntity;

/**
 * 用户和角色关联表
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/26 20:18
 */
public interface SysUserRoleService extends IService<SysUserRoleEntity> {
}
