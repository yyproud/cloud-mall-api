package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.yang.common.satoken.utils.LoginHelper;
import org.springframework.stereotype.Service;
import com.yang.system.entity.SysDictDataEntity;
import com.yang.system.vo.SysDictDataVo;
import com.yang.system.dto.SysDictDataDto;
import com.yang.system.mapper.SysDictDataMapper;
import com.yang.system.service.ISysDictDataService;

import java.util.Date;
import java.util.List;
import java.util.Collection;

/**
 * 字典数据Service业务层处理
 *
 * @author 杨旭
 * @email
 * @create 2023/10/17 16:33
 */
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictDataEntity> implements ISysDictDataService {

    /**
     * 查询字典数据
     */
    @Override
    public SysDictDataVo queryById(Long dictCode) {
        return this.baseMapper.selectVoById(dictCode);
    }

    /**
     * 查询字典数据列表
     */
    @Override
    public PageView<SysDictDataVo> queryPageList(SysDictDataDto dto) {
        return null;
    }

    /**
     * 查询字典数据列表
     */
    @Override
    public List<SysDictDataVo> queryList(SysDictDataDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增字典数据
     */
    @Override
    public Boolean insert(SysDictDataDto dto) {
        SysDictDataEntity add = BeanUtil.toBean(dto, SysDictDataEntity.class);
        add.setCreateId(LoginHelper.getUserId());
        add.setCreateTime(new Date());
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改字典数据
     */
    @Override
    public Boolean update(SysDictDataDto dto) {
        SysDictDataEntity update = BeanUtil.toBean(dto, SysDictDataEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除字典数据信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<SysDictDataVo> selectDictDataByType(String dictType) {
        return this.baseMapper.selectVoList(new LambdaQueryWrapper<SysDictDataEntity>()
                .eq(SysDictDataEntity::getStatus, 0)
                .eq(SysDictDataEntity::getDictType, dictType)
                .orderByAsc(SysDictDataEntity::getDictSort));
    }
}
