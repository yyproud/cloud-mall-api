package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.system.service.IVideoCommentService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.yang.im.entity.VideoCommentEntity;
import com.yang.im.vo.VideoCommentVo;
import com.yang.im.dto.VideoCommentDto;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import javax.annotation.Resource;
import java.util.List;
import java.util.Arrays;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 视频评论控制器
 * 前端访问路由地址为:/im/videoComment
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:44
 */
@Validated
@RestController
@RequestMapping("/videoComment")
public class VideoCommentController {

    @Resource
    private IVideoCommentService iVideoCommentService;

    /**
     * 查询视频评论列表
     */
    @SaCheckPermission("im:videoComment:list")
    @GetMapping("/list")
    public Result list(VideoCommentDto dto) {
        PageView<VideoCommentVo> list = iVideoCommentService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取视频评论详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:videoComment:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iVideoCommentService.queryById(id));
    }

    /**
     * 新增视频评论
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:videoComment:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody VideoCommentDto dto) {
        return iVideoCommentService.insert(dto) ? Result.ok() :Result.error();
    }

    /**
     * 修改视频评论
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:videoComment:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody VideoCommentDto dto) {
        return iVideoCommentService.update(dto) ? Result.ok() :Result.error();
    }

    /**
     * 删除视频评论
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:videoComment:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iVideoCommentService.deleteIds(Arrays.asList(ids)) ? Result.ok() :Result.error();
    }
}
