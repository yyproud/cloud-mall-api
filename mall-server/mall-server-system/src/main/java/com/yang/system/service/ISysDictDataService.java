package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.system.entity.SysDictDataEntity;
import com.yang.system.vo.SysDictDataVo;
import com.yang.system.dto.SysDictDataDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * 字典数据Service接口
 *
 * @author 杨旭
 * @email  
 * @create 2023/10/17 16:33
 */
public interface ISysDictDataService extends IService<SysDictDataEntity>{

    /**
     * 查询字典数据
     *
     * @param dictCode 参数
     * @return 字典数据实体
     */
    SysDictDataVo queryById(Long dictCode);

    /**
     * 查询字典数据列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<SysDictDataVo> queryPageList(SysDictDataDto dto);

    /**
     * 查询字典数据列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<SysDictDataVo> queryList(SysDictDataDto dto);

    /**
     * 新增字典数据
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(SysDictDataDto dto);

    /**
     * 修改字典数据
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(SysDictDataDto dto);

    /**
     * 批量删除字典数据信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);

    /**
     * 根据字典类型查询字典数据信息
     * @param dictType 字典类型
     * @return 字典数据信息
     */
    List<SysDictDataVo> selectDictDataByType(String dictType);
}
