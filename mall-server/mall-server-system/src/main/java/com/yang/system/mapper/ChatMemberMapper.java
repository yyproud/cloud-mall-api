package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.ChatMemberEntity;
import com.yang.im.vo.ChatMemberVo;
import com.yang.im.dto.ChatMemberDto;

import java.util.List;

/**
 * 成员Mapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 01:24
 */
public interface ChatMemberMapper extends BaseMapperPlus<ChatMemberMapper, ChatMemberEntity, ChatMemberVo> {

    /**
     * 查询成员分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<ChatMemberVo> queryPageList(Page<ChatMemberVo> page, @Param("dto") ChatMemberDto dto);

    /**
     * 查询成员列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<ChatMemberVo> queryList(@Param("dto") ChatMemberDto dto);
}
