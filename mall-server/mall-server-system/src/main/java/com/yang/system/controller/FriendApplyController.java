package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import com.yang.im.dto.FriendApplyDto;
import com.yang.im.vo.FriendApplyVo;
import com.yang.system.service.IFriendApplyService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * friendApply控制器
 * 前端访问路由地址为:/im/friendApply
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Validated
@RestController
@RequestMapping("/friendApply")
public class FriendApplyController {

    @Resource
    private IFriendApplyService iFriendApplyService;

    /**
     * 查询friendApply列表
     */
    @SaCheckPermission("im:friendApply:list")
    @GetMapping("/list")
    public Result list(FriendApplyDto dto) {
        PageView<FriendApplyVo> list = iFriendApplyService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取friendApply详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:friendApply:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iFriendApplyService.queryById(id));
    }

    /**
     * 新增friendApply
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:friendApply:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody FriendApplyDto dto) {
        return iFriendApplyService.insert(dto) ? Result.ok() : Result.error();
    }

    /**
     * 修改friendApply
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:friendApply:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody FriendApplyDto dto) {
        return iFriendApplyService.update(dto) ? Result.ok() : Result.error();
    }

    /**
     * 删除friendApply
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:friendApply:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iFriendApplyService.deleteIds(Arrays.asList(ids)) ? Result.ok() : Result.error();
    }
}
