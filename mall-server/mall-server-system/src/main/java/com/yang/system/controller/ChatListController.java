package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import com.yang.im.dto.ChatListDto;
import com.yang.im.vo.ChatListVo;
import com.yang.system.service.IChatListService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * chatList控制器
 * 前端访问路由地址为:/im/chatList
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Validated
@RestController
@RequestMapping("/chatList")
public class ChatListController {

    @Resource
    private IChatListService iChatListService;

    /**
     * 查询chatList列表
     */
    @SaCheckPermission("im:chatList:list")
    @GetMapping("/list")
    public Result list(ChatListDto dto) {
        PageView<ChatListVo> list = iChatListService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取chatList详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:chatList:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iChatListService.queryById(id));
    }

    /**
     * 新增chatList
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:chatList:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody ChatListDto dto) {
        return iChatListService.insert(dto) ? Result.ok() : Result.error();
    }

    /**
     * 修改chatList
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:chatList:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody ChatListDto dto) {
        return iChatListService.update(dto) ? Result.ok() : Result.error();
    }

    /**
     * 删除chatList
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:chatList:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iChatListService.deleteIds(Arrays.asList(ids)) ? Result.ok() : Result.error();
    }
}
