package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.CustomUrlEntity;
import com.yang.im.vo.CustomUrlVo;
import com.yang.im.dto.CustomUrlDto;

import java.util.List;

/**
 * 自定义网址Mapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/19 23:28
 */
public interface CustomUrlMapper extends BaseMapperPlus<CustomUrlMapper, CustomUrlEntity, CustomUrlVo> {

    /**
     * 查询自定义网址分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<CustomUrlVo> queryPageList(Page<CustomUrlVo> page, @Param("dto") CustomUrlDto dto);

    /**
     * 查询自定义网址列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<CustomUrlVo> queryList(@Param("dto") CustomUrlDto dto);
}
