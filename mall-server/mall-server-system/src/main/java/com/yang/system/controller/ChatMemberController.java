package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.yang.im.entity.ChatMemberEntity;
import com.yang.im.vo.ChatMemberVo;
import com.yang.im.dto.ChatMemberDto;
import com.yang.system.service.IChatMemberService;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import javax.annotation.Resource;
import java.util.List;
import java.util.Arrays;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 成员控制器
 * 前端访问路由地址为:/im/chatMember
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 01:24
 */
@Validated
@RestController
@RequestMapping("/chatMember")
public class ChatMemberController {

    @Resource
    private IChatMemberService iChatMemberService;

    /**
     * 查询成员列表
     */
    @SaCheckPermission("im:chatMember:list")
    @GetMapping("/list")
    public Result list(ChatMemberDto dto) {
        PageView<ChatMemberVo> list = iChatMemberService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取成员详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:chatMember:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iChatMemberService.queryById(id));
    }

    /**
     * 新增成员
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:chatMember:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody ChatMemberDto dto) {
        return iChatMemberService.insert(dto) ? Result.ok() :Result.error();
    }

    /**
     * 修改成员
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:chatMember:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody ChatMemberDto dto) {
        return iChatMemberService.update(dto) ? Result.ok() :Result.error();
    }

    /**
     * 删除成员
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:chatMember:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iChatMemberService.deleteIds(Arrays.asList(ids)) ? Result.ok() :Result.error();
    }
}
