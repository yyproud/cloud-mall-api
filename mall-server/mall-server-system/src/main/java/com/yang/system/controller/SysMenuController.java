package com.yang.system.controller;

import com.yang.common.core.utils.StringUtils;
import com.yang.common.satoken.utils.LoginHelper;
import com.yang.common.core.utils.Result;
import com.yang.system.dto.SysMenuDto;
import com.yang.system.entity.SysMenuEntity;
import com.yang.system.service.SysMenuService;
import com.yang.system.vo.RouterVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * 菜单控制类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 9:04
 */
@RestController
@RequestMapping("/menu")
public class SysMenuController {

    @Resource
    SysMenuService sysMenuService;

    /**
     * 获取菜单列表
     *
     * @param sysMenuDto 查询条件
     * @return 列表数据
     */
    @GetMapping("/list")
    public Result list(SysMenuDto sysMenuDto) {
        List<SysMenuEntity> sysMenuEntities = sysMenuService.selectMenuList(sysMenuDto);
        return Result.ok(sysMenuEntities);
    }

    /**
     * 根据菜单编号获取详细信息
     *
     * @param menuId 菜单ID
     */
    @GetMapping(value = "/{menuId}")
    public Result getInfo(@PathVariable Long menuId) {
        return Result.ok(sysMenuService.selectMenuById(menuId));
    }

    /**
     * 菜单添加
     *
     * @param menu 参数
     * @return 结果
     */
    @PostMapping
    public Result add(@Validated @RequestBody SysMenuEntity menu) {
        if (sysMenuService.checkMenuNameUnique(menu)) {
            return Result.error("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        } else if (SysMenuEntity.YES_FRAME.equals(menu.getIsFrame()) && !StringUtils.isHttp(menu.getPath())) {
            return Result.error("新增菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        }
        return sysMenuService.insertMenu(menu);
    }

    /**
     * 菜单修改
     *
     * @param menu 参数
     * @return 结果
     */
    @PutMapping
    public Result edit(@Validated @RequestBody SysMenuEntity menu) {
        if (sysMenuService.checkMenuNameUnique(menu)) {
            return Result.error("修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
        } else if (SysMenuEntity.YES_FRAME.equals(menu.getIsFrame()) && !StringUtils.isHttp(menu.getPath())) {
            return Result.error("修改菜单'" + menu.getMenuName() + "'失败，地址必须以http(s)://开头");
        } else if (menu.getMenuId().equals(menu.getParentId())) {
            return Result.error("修改菜单'" + menu.getMenuName() + "'失败，上级菜单不能选择自己");
        }
        return sysMenuService.updateMenu(menu);
    }

    /**
     * 删除菜单
     *
     * @param menuId 菜单ID
     */
    @DeleteMapping("/{menuId}")
    public Result remove(@PathVariable("menuId") Long menuId) {
        if (sysMenuService.hasChildByMenuId(menuId)) {
            return Result.error("存在子菜单,不允许删除");
        }
        if (sysMenuService.checkMenuExistRole(menuId)) {
            return Result.error("菜单已分配,不允许删除");
        }
        return sysMenuService.deleteMenuById(menuId);
    }

    @GetMapping("/getRouters")
    public Result getRouters() {
        Long userId = LoginHelper.getUserId();
        List<SysMenuEntity> menus = sysMenuService.selectMenuTreeByUserId(userId);
        List<RouterVo> routerVos = sysMenuService.buildMenus(menus);
        return Result.ok(routerVos);
    }
}


