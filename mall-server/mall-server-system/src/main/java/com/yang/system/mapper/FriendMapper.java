package com.yang.system.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.yang.im.dto.FriendDto;
import com.yang.im.entity.FriendEntity;
import com.yang.im.vo.FriendVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * friendMapper接口
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
public interface FriendMapper extends BaseMapperPlus<FriendMapper, FriendEntity, FriendVo> {

    /**
     * 查询friend分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<FriendVo> queryPageList(Page<FriendVo> page, @Param("dto") FriendDto dto);

    /**
     * 查询friend列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<FriendVo> queryList(@Param("dto") FriendDto dto);

    List<FriendVo> queryListByUserId(Long userId);

    FriendVo queryByUserId(@Param("userId") Long userId, @Param("friendId") Long friendId);
}
