package com.yang.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.system.entity.SysUserRoleEntity;
import com.yang.system.mapper.SysUserRoleMapper;
import com.yang.system.service.SysUserRoleService;
import org.springframework.stereotype.Service;

/**
 * 用户和角色关联
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/26 20:18
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRoleEntity> implements SysUserRoleService {
}
