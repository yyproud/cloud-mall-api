package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.system.entity.SysUserEntity;
import com.yang.system.vo.SysUserVo;

import java.util.List;

/**
 * 系统用户
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 11:23
 */
public interface SysUserService extends IService<SysUserEntity> {
    /**
     * 根据用户账号查询
     *
     * @param userName 用户账号
     * @return 用户实体
     */
    SysUserEntity getByUserName(String userName);

    /**
     * 根据用户id查询
     *
     * @param userId 用户id
     * @return 用户实体视图
     */
    SysUserVo getByUserId(Long userId);

}
