package com.yang.system.dubbo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yang.api.system.dubbo.RangedFriendService;
import com.yang.im.dto.FriendApplyDto;
import com.yang.im.dto.FriendDto;
import com.yang.im.entity.FriendApplyEntity;
import com.yang.im.entity.FriendEntity;
import com.yang.system.service.IFriendApplyService;
import com.yang.system.service.IFriendService;
import org.apache.dubbo.config.annotation.DubboService;

import javax.annotation.Resource;

/**
 * @Author: yang
 * @CreateTime: 2024-10-22
 */
@DubboService
public class RangedFriendServiceImpl implements RangedFriendService {

    @Resource
    private IFriendService friendService;
    @Resource
    private IFriendApplyService friendApplyService;


    @Override
    public FriendEntity queryFriendById(Long friendId, Long userId) {
        return friendService.getOne(new QueryWrapper<FriendEntity>()
                .eq("friend_id", friendId)
                .eq("user_id", userId));
    }

    @Override
    public Boolean insert(FriendDto dto) {
        return friendService.insert(dto);
    }

    @Override
    public FriendApplyEntity checkApply(Long friendId, Long userId) {
        return friendApplyService.getOne(new QueryWrapper<FriendApplyEntity>()
                .eq("apply_user_id", userId)
                .eq("friend_user_id", friendId)
                .eq("action", 0));
    }

    @Override
    public FriendApplyEntity getApplyById(Long id) {
        return friendApplyService.getById(id);
    }

    @Override
    public Boolean updateApply(FriendApplyDto friendApplyDto) {
        return friendApplyService.update(friendApplyDto);
    }

    @Override
    public Boolean insertApply(FriendApplyDto friendApplyDto) {
        friendApplyDto.setTime(System.currentTimeMillis() / 1000);
        return friendApplyService.insert(friendApplyDto);
    }


}
