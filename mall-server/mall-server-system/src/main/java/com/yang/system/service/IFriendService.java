package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.FriendEntity;
import com.yang.im.vo.FriendVo;
import com.yang.im.dto.FriendDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * friendService接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface IFriendService extends IService<FriendEntity>{

    /**
     * 查询friend
     *
     * @param id 参数
     * @return friend实体
     */
    FriendVo queryById(Long id);

    /**
     * 查询friend列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<FriendVo> queryPageList(FriendDto dto);

    /**
     * 查询friend列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<FriendVo> queryList(FriendDto dto);

    /**
     * 新增friend
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(FriendDto dto);

    /**
     * 修改friend
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(FriendDto dto);

    /**
     * 批量删除friend信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);

    List<FriendVo> queryListByUserId(Long userId);
    FriendVo queryByUserId(Long userId,Long friendId);
}
