package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.VideoShareEntity;
import com.yang.im.vo.VideoShareVo;
import com.yang.im.dto.VideoShareDto;

import java.util.List;

/**
 * 用户发布视频列Mapper接口
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:46
 */
public interface VideoShareMapper extends BaseMapperPlus<VideoShareMapper, VideoShareEntity, VideoShareVo> {

    /**
     * 查询用户发布视频列分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<VideoShareVo> queryPageList(Page<VideoShareVo> page, @Param("dto") VideoShareDto dto);

    /**
     * 查询用户发布视频列列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<VideoShareVo> queryList(@Param("dto") VideoShareDto dto);
}
