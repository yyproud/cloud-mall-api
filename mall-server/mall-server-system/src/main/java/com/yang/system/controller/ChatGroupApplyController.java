package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import com.yang.im.dto.ChatGroupApplyDto;
import com.yang.im.vo.ChatGroupApplyVo;
import com.yang.system.service.IChatGroupApplyService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * chatGroupApply控制器
 * 前端访问路由地址为:/im/chatGroupApply
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Validated
@RestController
@RequestMapping("/chatGroupApply")
public class ChatGroupApplyController {

    @Resource
    private IChatGroupApplyService iChatGroupApplyService;

    /**
     * 查询chatGroupApply列表
     */
    @SaCheckPermission("im:chatGroupApply:list")
    @GetMapping("/list")
    public Result list(ChatGroupApplyDto dto) {
        PageView<ChatGroupApplyVo> list = iChatGroupApplyService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取chatGroupApply详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:chatGroupApply:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iChatGroupApplyService.queryById(id));
    }

    /**
     * 新增chatGroupApply
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:chatGroupApply:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody ChatGroupApplyDto dto) {
        return iChatGroupApplyService.insert(dto) ? Result.ok() : Result.error();
    }

    /**
     * 修改chatGroupApply
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:chatGroupApply:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody ChatGroupApplyDto dto) {
        return iChatGroupApplyService.update(dto) ? Result.ok() : Result.error();
    }

    /**
     * 删除chatGroupApply
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:chatGroupApply:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iChatGroupApplyService.deleteIds(Arrays.asList(ids)) ? Result.ok() : Result.error();
    }
}
