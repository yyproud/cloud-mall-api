package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.yang.im.entity.FriendApplyEntity;
import com.yang.im.vo.FriendApplyVo;
import com.yang.im.dto.FriendApplyDto;
import com.yang.system.mapper.FriendApplyMapper;
import com.yang.system.service.IFriendApplyService;

import java.util.List;
import java.util.Collection;

/**
 * friendApplyService业务层处理
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
@Service
public class FriendApplyServiceImpl extends ServiceImpl<FriendApplyMapper, FriendApplyEntity> implements IFriendApplyService {

    /**
     * 查询friendApply
     */
    @Override
    public FriendApplyVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询friendApply列表
     */
    @Override
    public PageView<FriendApplyVo> queryPageList(FriendApplyDto dto) {
        Page<FriendApplyVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询friendApply列表
     */
    @Override
    public List<FriendApplyVo> queryList(FriendApplyDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增friendApply
     */
    @Override
    public Boolean insert(FriendApplyDto dto) {
        FriendApplyEntity add = BeanUtil.toBean(dto, FriendApplyEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改friendApply
     */
    @Override
    public Boolean update(FriendApplyDto dto) {
        FriendApplyEntity update = BeanUtil.toBean(dto, FriendApplyEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除friendApply信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
