package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.ChatListEntity;
import com.yang.im.entity.ChatMemberEntity;
import com.yang.im.vo.ChatListVo;
import com.yang.im.dto.ChatListDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * chatListService接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface IChatListService extends IService<ChatListEntity>{

    /**
     * 查询chatList
     *
     * @param id 参数
     * @return chatList实体
     */
    ChatListVo queryById(Long id);

    /**
     * 查询chatList列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<ChatListVo> queryPageList(ChatListDto dto);

    /**
     * 查询chatList列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<ChatListVo> queryList(ChatListDto dto);

    /**
     * 新增chatList
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(ChatListDto dto);

    /**
     * 修改chatList
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(ChatListDto dto);

    /**
     * 批量删除chatList信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);


}
