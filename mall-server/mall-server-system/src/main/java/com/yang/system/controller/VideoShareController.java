package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.system.service.IVideoShareService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.yang.im.entity.VideoShareEntity;
import com.yang.im.vo.VideoShareVo;
import com.yang.im.dto.VideoShareDto;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import javax.annotation.Resource;
import java.util.List;
import java.util.Arrays;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 用户发布视频列控制器
 * 前端访问路由地址为:/im/videoShare
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:46
 */
@Validated
@RestController
@RequestMapping("/videoShare")
public class VideoShareController {

    @Resource
    private IVideoShareService iVideoShareService;

    /**
     * 查询用户发布视频列列表
     */
    @SaCheckPermission("im:videoShare:list")
    @GetMapping("/list")
    public Result list(VideoShareDto dto) {
        PageView<VideoShareVo> list = iVideoShareService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取用户发布视频列详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:videoShare:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iVideoShareService.queryById(id));
    }

    /**
     * 新增用户发布视频列
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:videoShare:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody VideoShareDto dto) {
        return iVideoShareService.insert(dto) ? Result.ok() :Result.error();
    }

    /**
     * 修改用户发布视频列
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:videoShare:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody VideoShareDto dto) {
        return iVideoShareService.update(dto) ? Result.ok() :Result.error();
    }

    /**
     * 删除用户发布视频列
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:videoShare:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iVideoShareService.deleteIds(Arrays.asList(ids)) ? Result.ok() :Result.error();
    }
}
