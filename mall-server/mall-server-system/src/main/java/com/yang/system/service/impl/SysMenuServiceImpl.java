package com.yang.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.core.utils.Result;
import com.yang.common.satoken.utils.LoginHelper;
import com.yang.common.core.utils.StreamUtils;
import com.yang.common.core.utils.StringUtils;
import com.yang.system.dto.SysMenuDto;
import com.yang.system.entity.SysMenuEntity;
import com.yang.system.entity.SysRoleMenuEntity;
import com.yang.system.mapper.SysMenuMapper;
import com.yang.system.mapper.SysRoleMenuMapper;
import com.yang.system.service.SysMenuService;
import com.yang.system.vo.MetaVo;
import com.yang.system.vo.RouterVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**
 * 系统菜单
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/26 20:18
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenuEntity> implements SysMenuService {

    @Resource
    SysRoleMenuMapper roleMenuMapper;


    @Override
    public Set<String> selectMenuPermsByUserId(Long userId) {
        Set<String> permsSet = new HashSet<>();
        // 判断是否为超级管理员
        if (LoginHelper.isAdmin(userId)) {
            permsSet.add("*:*:*");
        } else {
            // 非管理员获取当前用户菜单
            List<String> perms = this.baseMapper.selectMenuPermsByUserId(userId);
            for (String perm : perms) {
                if (StringUtils.isNotEmpty(perm)) {
                    permsSet.addAll(StringUtils.splitList(perm.trim()));
                }
            }
        }
        return permsSet;
    }

    @Override
    public List<SysMenuEntity> selectMenuTreeByUserId(Long userId) {
        List<SysMenuEntity> sysMenuEntities;
        if (LoginHelper.isAdmin(userId)) {
            sysMenuEntities = this.baseMapper.selectMenuTree();
        } else {
            sysMenuEntities = this.baseMapper.selectMenuTreeByUserId(userId);
        }
        return getChildPerms(sysMenuEntities, 0);
    }

    @Override
    public List<RouterVo> buildMenus(List<SysMenuEntity> sysMenuEntities) {
        List<RouterVo> routers = new LinkedList<>();
        for (SysMenuEntity menu : sysMenuEntities) {
            RouterVo router = new RouterVo();
            router.setHidden("1".equals(menu.getVisible()));
            router.setName(getRouteName(menu));
            router.setPath(getRouterPath(menu));
            router.setComponent(getComponent(menu));
            router.setQuery(menu.getPathParam());
            router.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), "1".equals(menu.getIsCache()), menu.getPath()));
            List<SysMenuEntity> cMenus = menu.getChildren();
            if (!cMenus.isEmpty() && SysMenuEntity.TYPE_DIR.equals(menu.getMenuType())) {
                router.setAlwaysShow(true);
                router.setRedirect("noRedirect");
                router.setChildren(buildMenus(cMenus));
            } else if (isMenuFrame(menu)) {
                router.setMeta(null);
                List<RouterVo> childrenList = new ArrayList<>();
                RouterVo children = new RouterVo();
                children.setPath(menu.getPath());
                children.setComponent(menu.getComponent());
                children.setName(StringUtils.capitalize(menu.getPath()));
                children.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), "1".equals(menu.getIsCache()), menu.getPath()));
                children.setQuery(menu.getPathParam());
                childrenList.add(children);
                router.setChildren(childrenList);
            } else if (menu.getParentId().intValue() == 0 && isInnerLink(menu)) {
                router.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon()));
                router.setPath("/");
                List<RouterVo> childrenList = new ArrayList<>();
                RouterVo children = new RouterVo();
                String routerPath = innerLinkReplaceEach(menu.getPath());
                children.setPath(routerPath);
                children.setComponent(SysMenuEntity.INNER_LINK);
                children.setName(StringUtils.capitalize(routerPath));
                children.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon(), menu.getPath()));
                childrenList.add(children);
                router.setChildren(childrenList);
            }
            routers.add(router);
        }
        return routers;
    }

    @Override
    public List<SysMenuEntity> selectMenuList(SysMenuDto sysMenuDto) {
        List<SysMenuEntity> sysMenuEntityList;
        Long userId = LoginHelper.getUserId();
        if (LoginHelper.isAdmin(userId)) {
            sysMenuEntityList = this.baseMapper.selectAllMenuListByDto(sysMenuDto);
        } else {
            sysMenuDto.setUserId(userId);
            sysMenuEntityList = this.baseMapper.selectMenuList(sysMenuDto);
        }
        return sysMenuEntityList;
    }

    @Override
    public SysMenuEntity selectMenuById(Long menuId) {
        return this.baseMapper.selectById(menuId);
    }

    @Override
    public Boolean checkMenuNameUnique(SysMenuEntity sysMenuEntity) {
        return this.baseMapper.exists(new LambdaQueryWrapper<SysMenuEntity>()
                .eq(SysMenuEntity::getMenuName, sysMenuEntity.getMenuName())
                .eq(SysMenuEntity::getParentId, sysMenuEntity.getParentId())
                .ne(ObjectUtil.isNotNull(sysMenuEntity.getMenuId()), SysMenuEntity::getMenuId, sysMenuEntity.getMenuId())
        );
    }

    @Override
    public Boolean hasChildByMenuId(Long menuId) {
        return this.baseMapper.exists(new LambdaQueryWrapper<SysMenuEntity>().eq(SysMenuEntity::getParentId, menuId));
    }

    @Override
    public Boolean checkMenuExistRole(Long menuId) {
        return roleMenuMapper.exists(new LambdaQueryWrapper<SysRoleMenuEntity>().eq(SysRoleMenuEntity::getMenuId, menuId));
    }

    @Override
    public Result updateMenu(SysMenuEntity sysMenuEntity) {
        return this.baseMapper.updateById(sysMenuEntity) > 0 ? Result.ok() : Result.error();
    }

    @Override
    public Result insertMenu(SysMenuEntity sysMenuEntity) {
        return this.baseMapper.insert(sysMenuEntity) > 0 ? Result.ok() : Result.error();
    }

    @Override
    public Result deleteMenuById(Long menuId) {
        return this.baseMapper.deleteById(menuId) > 0 ? Result.ok() : Result.error();
    }

    /**
     * 根据父节点的ID获取所有子节点
     *
     * @param list     分类表
     * @param parentId 传入的父节点ID
     * @return String
     */
    public List<SysMenuEntity> getChildPerms(List<SysMenuEntity> list, int parentId) {
        List<SysMenuEntity> returnList = new ArrayList<>();
        for (SysMenuEntity t : list) {
            // 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
            if (t.getParentId() == parentId) {
                recursionFn(list, t);
                returnList.add(t);
            }
        }
        return returnList;
    }

    /**
     * 递归列表
     *
     * @param list 菜单集合
     * @param t    递归因子
     */
    private void recursionFn(List<SysMenuEntity> list, SysMenuEntity t) {
        // 得到子节点列表
        List<SysMenuEntity> childList = getChildList(list, t);
        t.setChildren(childList);
        for (SysMenuEntity tChild : childList) {
            if (hasChild(list, tChild)) {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<SysMenuEntity> getChildList(List<SysMenuEntity> list, SysMenuEntity t) {
        return StreamUtils.filter(list, n -> n.getParentId().equals(t.getMenuId()));
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<SysMenuEntity> list, SysMenuEntity t) {
        return getChildList(list, t).size() > 0;
    }

    /**
     * 获取路由名称
     *
     * @param menu 菜单信息
     * @return 路由名称
     */
    public String getRouteName(SysMenuEntity menu) {
        String routerName = StringUtils.capitalize(menu.getPath());
        // 非外链并且是一级目录（类型为目录）
        if (isMenuFrame(menu)) {
            routerName = StringUtils.EMPTY;
        }
        return routerName;
    }

    /**
     * 是否为菜单内部跳转
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isMenuFrame(SysMenuEntity menu) {
        return menu.getParentId().intValue() == 0 && SysMenuEntity.TYPE_MENU.equals(menu.getMenuType())
                && menu.getIsFrame().equals(SysMenuEntity.NO_FRAME);
    }

    /**
     * 获取路由地址
     *
     * @param menu 菜单信息
     * @return 路由地址
     */
    public String getRouterPath(SysMenuEntity menu) {
        String routerPath = menu.getPath();
        // 内链打开外网方式
        if (menu.getParentId().intValue() != 0 && isInnerLink(menu)) {
            routerPath = innerLinkReplaceEach(routerPath);
        }
        // 非外链并且是一级目录（类型为目录）
        if (0 == menu.getParentId().intValue() && SysMenuEntity.TYPE_DIR.equals(menu.getMenuType())
                && SysMenuEntity.NO_FRAME.equals(menu.getIsFrame())) {
            routerPath = "/" + menu.getPath();
        }
        // 非外链并且是一级目录（类型为菜单）
        else if (isMenuFrame(menu)) {
            routerPath = "/";
        }
        return routerPath;
    }

    /**
     * 是否为内链组件
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isInnerLink(SysMenuEntity menu) {
        return menu.getIsFrame().equals(SysMenuEntity.NO_FRAME) && StringUtils.isHttp(menu.getPath());
    }

    /**
     * 内链域名特殊字符替换
     */
    public String innerLinkReplaceEach(String path) {
        return StringUtils.replaceEach(path, new String[]{"http://", "https://", "www.", "."},
                new String[]{"", "", "", "/"});
    }

    /**
     * 获取组件信息
     *
     * @param menu 菜单信息
     * @return 组件信息
     */
    public String getComponent(SysMenuEntity menu) {
        String component = SysMenuEntity.LAYOUT;
        if (StringUtils.isNotEmpty(menu.getComponent()) && !isMenuFrame(menu)) {
            component = menu.getComponent();
        } else if (StringUtils.isEmpty(menu.getComponent()) && menu.getParentId().intValue() != 0 && isInnerLink(menu)) {
            component = SysMenuEntity.INNER_LINK;
        } else if (StringUtils.isEmpty(menu.getComponent()) && isParentView(menu)) {
            component = SysMenuEntity.PARENT_VIEW;
        }
        return component;
    }

    /**
     * 是否为parent_view组件
     *
     * @param menu 菜单信息
     * @return 结果
     */
    public boolean isParentView(SysMenuEntity menu) {
        return menu.getParentId().intValue() != 0 && SysMenuEntity.TYPE_DIR.equals(menu.getMenuType());
    }
}
