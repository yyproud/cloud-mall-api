package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.yang.system.vo.SysDictDataVo;
import com.yang.system.dto.SysDictDataDto;
import com.yang.system.service.ISysDictDataService;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import javax.annotation.Resource;
import java.util.List;
import java.util.Arrays;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 字典数据控制器
 * 前端访问路由地址为:/system/dictData
 *
 * @author 杨旭
 * @email  
 * @create 2023/10/17 16:33
 */
@Validated
@Api(tags = "字典数据控制器")
@RestController
@RequestMapping("/dictData")
public class SysDictDataController {

    @Resource
    private ISysDictDataService iSysDictDataService;

    /**
     * 查询字典数据列表
     */
    @ApiOperation("查询字典数据列表")
    @SaCheckPermission("system:dictData:list")
    @GetMapping("/list")
    public Result list(SysDictDataDto dto) {
        PageView<SysDictDataVo> list = iSysDictDataService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取字典数据详细信息
     *
     * @param dictCode 主键
     */
    @ApiOperation("获取字典数据详细信息")
    @SaCheckPermission("system:dictData:query")
    @GetMapping("/{dictCode}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long dictCode) {
        return Result.ok(iSysDictDataService.queryById(dictCode));
    }

    /**
     * 新增字典数据
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @ApiOperation("新增字典数据")
    @SaCheckPermission("system:dictData:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody SysDictDataDto dto) {
        return iSysDictDataService.insert(dto) ? Result.ok() :Result.error();
    }

    /**
     * 修改字典数据
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @ApiOperation("修改字典数据")
    @SaCheckPermission("system:dictData:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody SysDictDataDto dto) {
        return iSysDictDataService.update(dto) ? Result.ok() :Result.error();
    }

    /**
     * 删除字典数据
     *
     * @param dictCodes 主键串
     * @return 删除结果
     */
    @ApiOperation("删除字典数据")
    @SaCheckPermission("system:dictData:remove")
    @DeleteMapping("/{dictCodes}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] dictCodes) {
        return iSysDictDataService.deleteIds(Arrays.asList(dictCodes)) ? Result.ok() :Result.error();
    }

    /**
     * 根据字典类型查询字典数据信息
     *
     * @param dictType 字典类型
     */
    @ApiOperation("根据字典类型查询字典数据信息")
    @GetMapping(value = "/type/{dictType}")
    public Result dictType(@PathVariable String dictType) {
        List<SysDictDataVo> data = iSysDictDataService.selectDictDataByType(dictType);
        return Result.ok(data);
    }
}
