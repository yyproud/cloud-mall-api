package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.yang.im.entity.VideoShareEntity;
import com.yang.im.vo.VideoShareVo;
import com.yang.im.dto.VideoShareDto;
import com.yang.system.mapper.VideoShareMapper;
import com.yang.system.service.IVideoShareService;

import java.util.List;
import java.util.Collection;

/**
 * 用户发布视频列Service业务层处理
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:46
 */
@Service
public class VideoShareServiceImpl extends ServiceImpl<VideoShareMapper, VideoShareEntity> implements IVideoShareService {

    /**
     * 查询用户发布视频列
     */
    @Override
    public VideoShareVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询用户发布视频列列表
     */
    @Override
    public PageView<VideoShareVo> queryPageList(VideoShareDto dto) {
        Page<VideoShareVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询用户发布视频列列表
     */
    @Override
    public List<VideoShareVo> queryList(VideoShareDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增用户发布视频列
     */
    @Override
    public Boolean insert(VideoShareDto dto) {
        VideoShareEntity add = BeanUtil.toBean(dto, VideoShareEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改用户发布视频列
     */
    @Override
    public Boolean update(VideoShareDto dto) {
        VideoShareEntity update = BeanUtil.toBean(dto, VideoShareEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除用户发布视频列信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
