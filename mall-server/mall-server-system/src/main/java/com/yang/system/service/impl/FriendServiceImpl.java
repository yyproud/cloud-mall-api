package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.yang.im.entity.FriendEntity;
import com.yang.im.vo.FriendVo;
import com.yang.im.dto.FriendDto;
import com.yang.system.mapper.FriendMapper;
import com.yang.system.service.IFriendService;

import java.util.Date;
import java.util.List;
import java.util.Collection;

/**
 * friendService业务层处理
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
@Service
public class FriendServiceImpl extends ServiceImpl<FriendMapper, FriendEntity> implements IFriendService {

    /**
     * 查询friend
     */
    @Override
    public FriendVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询friend列表
     */
    @Override
    public PageView<FriendVo> queryPageList(FriendDto dto) {
        Page<FriendVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询friend列表
     */
    @Override
    public List<FriendVo> queryList(FriendDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增friend
     */
    @Override
    public Boolean insert(FriendDto dto) {
        FriendEntity add = BeanUtil.toBean(dto, FriendEntity.class);
        add.setTime(System.currentTimeMillis() / 1000);
        add.setCreateTime(new Date());
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改friend
     */
    @Override
    public Boolean update(FriendDto dto) {
        FriendEntity update = BeanUtil.toBean(dto, FriendEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除friend信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<FriendVo> queryListByUserId(Long userId) {
        return this.baseMapper.queryListByUserId(userId);
    }

    @Override
    public FriendVo queryByUserId(Long userId, Long friendId) {
        return this.baseMapper.queryByUserId(userId, friendId);
    }
}
