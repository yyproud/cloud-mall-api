package com.yang.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.system.entity.SysDictDataEntity;
import com.yang.system.vo.SysDictDataVo;
import com.yang.system.dto.SysDictDataDto;

import java.util.List;

/**
 * 字典数据Mapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2023/10/17 16:33
 */
public interface SysDictDataMapper extends BaseMapper<SysDictDataEntity> {

    /**
     * 查询字典数据分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<SysDictDataVo> queryPageList(Page<SysDictDataVo> page, @Param("dto") SysDictDataDto dto);

    /**
     * 查询字典数据列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<SysDictDataVo> queryList(@Param("dto") SysDictDataDto dto);
}
