package com.yang.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.system.entity.SysDictTypeEntity;
import com.yang.system.vo.SysDictTypeVo;
import com.yang.system.dto.SysDictTypeDto;

import java.util.List;

/**
 * 字典类型Mapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2023/10/16 17:11
 */
public interface SysDictTypeMapper extends BaseMapper<SysDictTypeEntity> {

    /**
     * 查询字典类型分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<SysDictTypeVo> queryPageList(Page<SysDictTypeVo> page, @Param("dto") SysDictTypeDto dto);

    /**
     * 查询字典类型列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<SysDictTypeVo> queryList(@Param("dto") SysDictTypeDto dto);
}
