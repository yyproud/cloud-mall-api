package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.ChatMemberEntity;
import com.yang.im.vo.ChatMemberVo;
import com.yang.im.dto.ChatMemberDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * 成员Service接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 01:24
 */
public interface IChatMemberService extends IService<ChatMemberEntity>{

    /**
     * 查询成员
     *
     * @param id 参数
     * @return 成员实体
     */
    ChatMemberVo queryById(Long id);

    /**
     * 查询成员列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<ChatMemberVo> queryPageList(ChatMemberDto dto);

    /**
     * 查询成员列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<ChatMemberVo> queryList(ChatMemberDto dto);

    /**
     * 新增成员
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(ChatMemberDto dto);

    /**
     * 修改成员
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(ChatMemberDto dto);

    /**
     * 批量删除成员信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
