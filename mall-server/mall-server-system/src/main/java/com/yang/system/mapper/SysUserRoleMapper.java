package com.yang.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.system.entity.SysUserRoleEntity;

/**
 * 用户和角色关联表
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/26 20:18
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRoleEntity> {
}
