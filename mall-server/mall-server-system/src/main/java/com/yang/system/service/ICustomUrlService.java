package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.CustomUrlEntity;
import com.yang.im.vo.CustomUrlVo;
import com.yang.im.dto.CustomUrlDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * 自定义网址Service接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/19 23:28
 */
public interface ICustomUrlService extends IService<CustomUrlEntity>{

    /**
     * 查询自定义网址
     *
     * @param id 参数
     * @return 自定义网址实体
     */
    CustomUrlVo queryById(Long id);

    /**
     * 查询自定义网址列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<CustomUrlVo> queryPageList(CustomUrlDto dto);

    /**
     * 查询自定义网址列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<CustomUrlVo> queryList(CustomUrlDto dto);

    /**
     * 新增自定义网址
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(CustomUrlDto dto);

    /**
     * 修改自定义网址
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(CustomUrlDto dto);

    /**
     * 批量删除自定义网址信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
