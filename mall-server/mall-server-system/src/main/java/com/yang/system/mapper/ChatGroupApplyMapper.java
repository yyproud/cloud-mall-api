package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.ChatGroupApplyEntity;
import com.yang.im.vo.ChatGroupApplyVo;
import com.yang.im.dto.ChatGroupApplyDto;

import java.util.List;

/**
 * chatGroupApplyMapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface ChatGroupApplyMapper extends BaseMapperPlus<ChatGroupApplyMapper, ChatGroupApplyEntity, ChatGroupApplyVo> {

    /**
     * 查询chatGroupApply分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<ChatGroupApplyVo> queryPageList(Page<ChatGroupApplyVo> page, @Param("dto") ChatGroupApplyDto dto);

    /**
     * 查询chatGroupApply列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<ChatGroupApplyVo> queryList(@Param("dto") ChatGroupApplyDto dto);
}
