package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.VideoCategoryEntity;
import com.yang.im.vo.VideoCategoryVo;
import com.yang.im.dto.VideoCategoryDto;

import java.util.List;

/**
 * 短视频分类Mapper接口
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:11
 */
public interface VideoCategoryMapper extends BaseMapperPlus<VideoCategoryMapper, VideoCategoryEntity, VideoCategoryVo> {

    /**
     * 查询短视频分类分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<VideoCategoryVo> queryPageList(Page<VideoCategoryVo> page, @Param("dto") VideoCategoryDto dto);

    /**
     * 查询短视频分类列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<VideoCategoryVo> queryList(@Param("dto") VideoCategoryDto dto);
}
