package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.VideoShareEntity;
import com.yang.im.vo.VideoShareVo;
import com.yang.im.dto.VideoShareDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * 用户发布视频列Service接口
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:46
 */
public interface IVideoShareService extends IService<VideoShareEntity>{

    /**
     * 查询用户发布视频列
     *
     * @param id 参数
     * @return 用户发布视频列实体
     */
    VideoShareVo queryById(Long id);

    /**
     * 查询用户发布视频列列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<VideoShareVo> queryPageList(VideoShareDto dto);

    /**
     * 查询用户发布视频列列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<VideoShareVo> queryList(VideoShareDto dto);

    /**
     * 新增用户发布视频列
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(VideoShareDto dto);

    /**
     * 修改用户发布视频列
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(VideoShareDto dto);

    /**
     * 批量删除用户发布视频列信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
