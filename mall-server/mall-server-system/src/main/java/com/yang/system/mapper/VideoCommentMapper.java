package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.VideoCommentEntity;
import com.yang.im.vo.VideoCommentVo;
import com.yang.im.dto.VideoCommentDto;

import java.util.List;

/**
 * 视频评论Mapper接口
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:44
 */
public interface VideoCommentMapper extends BaseMapperPlus<VideoCommentMapper, VideoCommentEntity, VideoCommentVo> {

    /**
     * 查询视频评论分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<VideoCommentVo> queryPageList(Page<VideoCommentVo> page, @Param("dto") VideoCommentDto dto);

    /**
     * 查询视频评论列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<VideoCommentVo> queryList(@Param("dto") VideoCommentDto dto);
}
