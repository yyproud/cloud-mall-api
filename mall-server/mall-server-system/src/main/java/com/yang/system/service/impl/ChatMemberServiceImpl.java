package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.yang.im.entity.ChatMemberEntity;
import com.yang.im.vo.ChatMemberVo;
import com.yang.im.dto.ChatMemberDto;
import com.yang.system.mapper.ChatMemberMapper;
import com.yang.system.service.IChatMemberService;

import java.util.List;
import java.util.Collection;

/**
 * 成员Service业务层处理
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 01:24
 */
@Service
public class ChatMemberServiceImpl extends ServiceImpl<ChatMemberMapper, ChatMemberEntity> implements IChatMemberService {

    /**
     * 查询成员
     */
    @Override
    public ChatMemberVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询成员列表
     */
    @Override
    public PageView<ChatMemberVo> queryPageList(ChatMemberDto dto) {
        Page<ChatMemberVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询成员列表
     */
    @Override
    public List<ChatMemberVo> queryList(ChatMemberDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增成员
     */
    @Override
    public Boolean insert(ChatMemberDto dto) {
        ChatMemberEntity add = BeanUtil.toBean(dto, ChatMemberEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改成员
     */
    @Override
    public Boolean update(ChatMemberDto dto) {
        ChatMemberEntity update = BeanUtil.toBean(dto, ChatMemberEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除成员信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
