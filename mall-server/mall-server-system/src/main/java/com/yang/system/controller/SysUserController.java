package com.yang.system.controller;

import com.yang.common.core.utils.Result;
import com.yang.common.satoken.utils.LoginHelper;
import com.yang.system.entity.SysUserEntity;
import com.yang.system.service.SysUserService;
import com.yang.system.vo.LoginVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;

/**
 * 系统用户控制器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 13:25
 */
@Api(tags = "系统用户控制器", value = "获取用户信息")
@RestController
@RequestMapping("/user")
public class SysUserController {
    @Resource
    SysUserService sysUserService;

    @ApiOperation("获取用户信息详情")
    @GetMapping("getInfo")
    public Result getInfo() {
        Map<String, Object> ajax = new HashMap<>(16);
        LoginVo loginUser = LoginHelper.getLoginUser();
        SysUserEntity user = sysUserService.getById(loginUser.getUserId());
        ajax.put("user", user);
        ajax.put("roles", loginUser.getRolePermission());
        ajax.put("permissions", loginUser.getMenuPermission());
        return Result.ok(ajax);
    }
}
