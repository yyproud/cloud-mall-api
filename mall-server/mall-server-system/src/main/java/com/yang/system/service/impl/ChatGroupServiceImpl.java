package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.yang.im.entity.ChatGroupEntity;
import com.yang.im.vo.ChatGroupVo;
import com.yang.im.dto.ChatGroupDto;
import com.yang.system.mapper.ChatGroupMapper;
import com.yang.system.service.IChatGroupService;

import java.util.List;
import java.util.Collection;

/**
 * chatGroupService业务层处理
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
@Service
public class ChatGroupServiceImpl extends ServiceImpl<ChatGroupMapper, ChatGroupEntity> implements IChatGroupService {

    /**
     * 查询chatGroup
     */
    @Override
    public ChatGroupVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询chatGroup列表
     */
    @Override
    public PageView<ChatGroupVo> queryPageList(ChatGroupDto dto) {
        Page<ChatGroupVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询chatGroup列表
     */
    @Override
    public List<ChatGroupVo> queryList(ChatGroupDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增chatGroup
     */
    @Override
    public Boolean insert(ChatGroupDto dto) {
        ChatGroupEntity add = BeanUtil.toBean(dto, ChatGroupEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改chatGroup
     */
    @Override
    public Boolean update(ChatGroupDto dto) {
        ChatGroupEntity update = BeanUtil.toBean(dto, ChatGroupEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除chatGroup信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
