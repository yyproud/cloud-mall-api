package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import com.yang.im.dto.FriendDto;
import com.yang.im.vo.FriendVo;
import com.yang.system.service.IFriendService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * friend控制器
 * 前端访问路由地址为:/im/friend
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Validated
@RestController
@RequestMapping("/friend")
public class FriendController {

    @Resource
    private IFriendService iFriendService;

    /**
     * 查询friend列表
     */
    @SaCheckPermission("im:friend:list")
    @GetMapping("/list")
    public Result list(FriendDto dto) {
        PageView<FriendVo> list = iFriendService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取friend详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:friend:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iFriendService.queryById(id));
    }

    /**
     * 新增friend
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:friend:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody FriendDto dto) {
        return iFriendService.insert(dto) ? Result.ok() : Result.error();
    }

    /**
     * 修改friend
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:friend:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody FriendDto dto) {
        return iFriendService.update(dto) ? Result.ok() : Result.error();
    }

    /**
     * 删除friend
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:friend:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iFriendService.deleteIds(Arrays.asList(ids)) ? Result.ok() : Result.error();
    }
}
