package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.common.core.utils.Result;
import com.yang.system.dto.SysMenuDto;
import com.yang.system.entity.SysMenuEntity;
import com.yang.system.vo.RouterVo;

import java.util.List;
import java.util.Set;

/**
 * 系统菜单
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/26 20:18
 */
public interface SysMenuService extends IService<SysMenuEntity> {
    /**
     * 根据userId查询菜单权限
     *
     * @param userId 用户id
     * @return 菜单权限集合
     */
    Set<String> selectMenuPermsByUserId(Long userId);

    /**
     * 根据用户ID查询菜单树信息
     *
     * @param userId 用户ID
     * @return 菜单树
     */
    List<SysMenuEntity> selectMenuTreeByUserId(Long userId);

    /**
     * 构建前端路由所需要的菜单
     *
     * @param sysMenuEntities 菜单集合
     * @return 路由集合
     */
    List<RouterVo> buildMenus(List<SysMenuEntity> sysMenuEntities);

    /**
     * 获取菜单列表
     *
     * @param sysMenuDto 查询条件
     * @return 菜单列表
     */
    List<SysMenuEntity> selectMenuList(SysMenuDto sysMenuDto);

    /**
     * 根据id获取详情
     *
     * @param menuId 菜单id
     * @return 详情
     */
    SysMenuEntity selectMenuById(Long menuId);

    /**
     * 修改菜单
     *
     * @param sysMenuEntity 菜单信息
     * @return 结果
     */
    Result updateMenu(SysMenuEntity sysMenuEntity);

    /**
     * 添加菜单
     *
     * @param sysMenuEntity 菜单信息
     * @return 结果
     */
    Result insertMenu(SysMenuEntity sysMenuEntity);

    /**
     * 根据菜单id删除
     *
     * @param menuId 菜单id
     * @return 结果
     */
    Result deleteMenuById(Long menuId);

    /**
     * 校验菜单名称是否唯一
     *
     * @param sysMenuEntity 菜单信息
     * @return 结果
     */
    Boolean checkMenuNameUnique(SysMenuEntity sysMenuEntity);

    /**
     * 查看是否存在子菜单
     *
     * @param menuId 菜单id
     * @return 结果
     */
    Boolean hasChildByMenuId(Long menuId);

    /**
     * 查看是否分配
     * @param menuId 菜单id
     * @return 结果
     */
    Boolean checkMenuExistRole(Long menuId);
}
