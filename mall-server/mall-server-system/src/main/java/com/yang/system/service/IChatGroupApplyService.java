package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.ChatGroupApplyEntity;
import com.yang.im.vo.ChatGroupApplyVo;
import com.yang.im.dto.ChatGroupApplyDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * chatGroupApplyService接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface IChatGroupApplyService extends IService<ChatGroupApplyEntity>{

    /**
     * 查询chatGroupApply
     *
     * @param id 参数
     * @return chatGroupApply实体
     */
    ChatGroupApplyVo queryById(Long id);

    /**
     * 查询chatGroupApply列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<ChatGroupApplyVo> queryPageList(ChatGroupApplyDto dto);

    /**
     * 查询chatGroupApply列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<ChatGroupApplyVo> queryList(ChatGroupApplyDto dto);

    /**
     * 新增chatGroupApply
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(ChatGroupApplyDto dto);

    /**
     * 修改chatGroupApply
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(ChatGroupApplyDto dto);

    /**
     * 批量删除chatGroupApply信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
