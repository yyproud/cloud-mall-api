package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.VideoCommentEntity;
import com.yang.im.vo.VideoCommentVo;
import com.yang.im.dto.VideoCommentDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * 视频评论Service接口
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:44
 */
public interface IVideoCommentService extends IService<VideoCommentEntity>{

    /**
     * 查询视频评论
     *
     * @param id 参数
     * @return 视频评论实体
     */
    VideoCommentVo queryById(Long id);

    /**
     * 查询视频评论列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<VideoCommentVo> queryPageList(VideoCommentDto dto);

    /**
     * 查询视频评论列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<VideoCommentVo> queryList(VideoCommentDto dto);

    /**
     * 新增视频评论
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(VideoCommentDto dto);

    /**
     * 修改视频评论
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(VideoCommentDto dto);

    /**
     * 批量删除视频评论信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
