package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.im.dto.ChatListDto;
import com.yang.im.entity.ChatListEntity;
import com.yang.im.vo.ChatListVo;
import com.yang.system.mapper.ChatListMapper;
import com.yang.system.service.IChatListService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

/**
 * chatListService业务层处理
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Service
public class ChatListServiceImpl extends ServiceImpl<ChatListMapper, ChatListEntity> implements IChatListService {

    /**
     * 查询chatList
     */
    @Override
    public ChatListVo queryById(Long id) {
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询chatList列表
     */
    @Override
    public PageView<ChatListVo> queryPageList(ChatListDto dto) {
        Page<ChatListVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询chatList列表
     */
    @Override
    public List<ChatListVo> queryList(ChatListDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增chatList
     */
    @Override
    public Boolean insert(ChatListDto dto) {
        ChatListEntity add = BeanUtil.toBean(dto, ChatListEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改chatList
     */
    @Override
    public Boolean update(ChatListDto dto) {
        ChatListEntity update = BeanUtil.toBean(dto, ChatListEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除chatList信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
