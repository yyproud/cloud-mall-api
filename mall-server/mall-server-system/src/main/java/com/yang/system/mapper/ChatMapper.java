package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.ChatEntity;
import com.yang.im.vo.ChatVo;
import com.yang.im.dto.ChatDto;

import java.util.List;

/**
 * chatMapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface ChatMapper extends BaseMapperPlus<ChatMapper, ChatEntity, ChatVo> {

    /**
     * 查询chat分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<ChatVo> queryPageList(Page<ChatVo> page, @Param("dto") ChatDto dto);

    /**
     * 查询chat列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<ChatVo> queryList(@Param("dto") ChatDto dto);
}
