package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.system.entity.SysDictTypeEntity;
import com.yang.system.vo.SysDictTypeVo;
import com.yang.system.dto.SysDictTypeDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * 字典类型Service接口
 *
 * @author 杨旭
 * @email  
 * @create 2023/10/16 17:11
 */
public interface ISysDictTypeService extends IService<SysDictTypeEntity>{

    /**
     * 查询字典类型
     *
     * @param dictId 参数
     * @return 字典类型实体
     */
    SysDictTypeVo queryById(Long dictId);

    /**
     * 查询字典类型列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<SysDictTypeVo> queryPageList(SysDictTypeDto dto);

    /**
     * 查询字典类型列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<SysDictTypeVo> queryList(SysDictTypeDto dto);

    /**
     * 新增字典类型
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(SysDictTypeDto dto);

    /**
     * 修改字典类型
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(SysDictTypeDto dto);

    /**
     * 批量删除字典类型信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);

    /**
     * 获取全部字典类型
     * @return 字典类型集合
     */
    List<SysDictTypeVo> selectDictTypeAll();
}
