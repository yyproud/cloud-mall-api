package com.yang.system.dubbo;

import cn.dev33.satoken.secure.BCrypt;
import cn.hutool.core.util.ObjectUtil;
import com.yang.api.system.dubbo.RangedSysUserService;
import com.yang.common.core.constant.HttpStatus;
import com.yang.common.core.exception.ServiceException;
import com.yang.common.redis.utils.RedisUtils;
import com.yang.system.dto.LoginDto;
import com.yang.system.dto.RegisterDto;
import com.yang.system.entity.SysUserEntity;
import com.yang.system.mapper.SysUserMapper;
import com.yang.system.service.SysMenuService;
import com.yang.system.service.SysRoleService;
import com.yang.system.vo.LoginVo;
import org.apache.dubbo.config.annotation.DubboService;

import javax.annotation.Resource;
import java.time.Duration;
import java.util.Date;
import java.util.Set;
import java.util.function.Supplier;

import static com.yang.system.entity.SysUserEntity.STATUS_LOCK;

/**
 * 用户
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 13:57
 */
@DubboService
public class RangedSysUserServiceImpl implements RangedSysUserService {

    @Resource
    SysUserMapper sysUserMapper;
    @Resource
    SysMenuService sysMenuService;
    @Resource
    SysRoleService sysRoleService;

    @Override
    public LoginVo getByUserName(LoginDto loginDto) {
        SysUserEntity user = sysUserMapper.getByUserName(loginDto.getUserName());
        // 判断用户名是否存在
        if (null == user) {
            throw new ServiceException(HttpStatus.ACCOUNT_DOES_NOT_EXIST, "用户不存在!");
        }
        // 判断账号是否被锁定
        if (user.getStatus().equals(STATUS_LOCK)) {
            throw new ServiceException(HttpStatus.ACCOUNT_LOCK, "账号已被锁定,请稍后再试!");
        }
        // 登录校验
        checkLogin(user, () -> !BCrypt.checkpw(loginDto.getPassword(), user.getPassword()));
        if (!"H5".equals(loginDto.getUserType())) {
            // 查询权限信息
            Set<String> menuPerms = sysMenuService.selectMenuPermsByUserId(user.getUserId());
            Set<String> rolePermission = sysRoleService.selectRolePermissionByUserId(user.getUserId());
            // 返回组装的视图
            return buildLoginVo(user, menuPerms, rolePermission);
        } else {
            // 返回组装的视图
            return buildLoginVo(user, null, null);
        }


    }

    @Override
    public LoginVo register(RegisterDto registerDto) throws ServiceException {
        SysUserEntity user = sysUserMapper.getByUserName(registerDto.getUserName());
        // 判断用户名是否存在
        if (null != user) {
            throw new ServiceException(HttpStatus.ACCOUNT_DOES_NOT_EXIST, "用户已存在!");
        }
        // 判断邀请码是否存在
        SysUserEntity inviteUser = sysUserMapper.getByInviteCode(registerDto.getInviteCode());
        if (null == inviteUser) {
            throw new ServiceException(HttpStatus.ACCOUNT_DOES_NOT_EXIST, "邀请码不存在!");
        }
        // 注册用户
        SysUserEntity sysUserEntity = new SysUserEntity();
        sysUserEntity.setUserName(registerDto.getUserName());
        sysUserEntity.setPassword(BCrypt.hashpw(registerDto.getPassword()));
        sysUserEntity.setUserType(registerDto.getUserType());
        sysUserEntity.setParentId(inviteUser.getUserId());
        sysUserEntity.setCreateTime(new Date());
        sysUserEntity.setLastLoginTime(new Date());
        sysUserEntity.setAvatar("/photo/logo.png");
        sysUserEntity.setGender("0");
        sysUserEntity.setNickName(registerDto.getUserName());
        int insert = sysUserMapper.insert(sysUserEntity);
        if (insert > 0) {
            return buildLoginVo(sysUserEntity, null, null);
        }
        throw new ServiceException(HttpStatus.ACCOUNT_DOES_NOT_EXIST, "网络不可用请稍后再试!");
    }

    @Override
    public SysUserEntity getUserById(Long id) {
        return sysUserMapper.selectById(id);
    }

    /**
     * 登录校验
     *
     * @param user     用户
     * @param supplier 密码
     */
    private void checkLogin(SysUserEntity user, Supplier<Boolean> supplier) {
        String errorKey = "password_error_count:" + user.getUserName();
        Integer errorNumber = RedisUtils.getCacheObject(errorKey);
        errorNumber = ObjectUtil.isNull(errorNumber) ? 0 : errorNumber;
        // 判断是否达到错误次数
        if (errorNumber == 5) {
            // 执行自定义异常返回
            long timeToLive = (RedisUtils.getTimeToLive(errorKey) / 1000) / 60;
            throw new ServiceException("已达到规定错误次数账号锁定,请" + (timeToLive == 0 ? 1 : timeToLive) + "分钟后再试!");
        }
        if (supplier.get()) {
            errorNumber++;
            // 达到规定错误次数 则锁定登录
            if (errorNumber == 5) {
                RedisUtils.setCacheObject(errorKey, errorNumber, Duration.ofMinutes(5));
                throw new ServiceException("已达到规定错误次数账号锁定,请5分钟后再试!");
            } else {
                // 未达到规定错误次数 则递增
                RedisUtils.setCacheObject(errorKey, errorNumber);
                throw new ServiceException("密码错误，请重试!");
            }
        }
        // 登陆成功，清除错误次数
        RedisUtils.deleteObject(errorKey);
    }


    /**
     * 构建登录视图
     *
     * @param user           用户
     * @param menuPerms      菜单集合
     * @param rolePermission 角色集合
     * @return 视图
     */
    private LoginVo buildLoginVo(SysUserEntity user, Set<String> menuPerms, Set<String> rolePermission) {
        LoginVo loginVo = new LoginVo();
        loginVo.setUserId(user.getUserId());
        loginVo.setUserName(user.getUserName());
        loginVo.setUserType(user.getUserType());
        loginVo.setNickName(user.getNickName());
        loginVo.setMenuPermission(menuPerms);
        loginVo.setRolePermission(rolePermission);
        return loginVo;
    }
}
