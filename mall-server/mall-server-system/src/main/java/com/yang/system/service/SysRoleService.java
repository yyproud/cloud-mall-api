package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.system.entity.SysRoleEntity;

import java.util.Set;

/**
 * 系统角色
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/26 20:18
 */
public interface SysRoleService extends IService<SysRoleEntity> {
    /**
     * 根据userId查询角色权限
     *
     * @param userId 用户id
     * @return 角色权限集合
     */
    Set<String> selectRolePermissionByUserId(Long userId);
}
