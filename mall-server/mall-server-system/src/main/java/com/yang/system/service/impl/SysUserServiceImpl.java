package com.yang.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.system.entity.SysUserEntity;
import com.yang.system.mapper.SysUserMapper;
import com.yang.system.service.SysUserService;
import com.yang.system.vo.SysUserVo;
import org.springframework.stereotype.Service;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 11:28
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUserEntity> implements SysUserService {
    @Override
    public SysUserEntity getByUserName(String userName) {
        return this.baseMapper.getByUserName(userName);
    }

    @Override
    public SysUserVo getByUserId(Long userId) {
        return this.baseMapper.selectVoById(userId);
    }
}
