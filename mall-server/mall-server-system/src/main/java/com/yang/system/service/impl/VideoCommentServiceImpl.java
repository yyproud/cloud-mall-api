package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.yang.im.entity.VideoCommentEntity;
import com.yang.im.vo.VideoCommentVo;
import com.yang.im.dto.VideoCommentDto;
import com.yang.system.mapper.VideoCommentMapper;
import com.yang.system.service.IVideoCommentService;

import java.util.List;
import java.util.Collection;

/**
 * 视频评论Service业务层处理
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:44
 */
@Service
public class VideoCommentServiceImpl extends ServiceImpl<VideoCommentMapper, VideoCommentEntity> implements IVideoCommentService {

    /**
     * 查询视频评论
     */
    @Override
    public VideoCommentVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询视频评论列表
     */
    @Override
    public PageView<VideoCommentVo> queryPageList(VideoCommentDto dto) {
        Page<VideoCommentVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询视频评论列表
     */
    @Override
    public List<VideoCommentVo> queryList(VideoCommentDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增视频评论
     */
    @Override
    public Boolean insert(VideoCommentDto dto) {
        VideoCommentEntity add = BeanUtil.toBean(dto, VideoCommentEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改视频评论
     */
    @Override
    public Boolean update(VideoCommentDto dto) {
        VideoCommentEntity update = BeanUtil.toBean(dto, VideoCommentEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除视频评论信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
