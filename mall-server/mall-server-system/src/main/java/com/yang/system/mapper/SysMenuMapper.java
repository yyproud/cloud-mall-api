package com.yang.system.mapper;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.common.core.utils.StringUtils;
import com.yang.system.dto.SysMenuDto;
import com.yang.system.entity.SysMenuEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 系统菜单
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/26 20:18
 */
public interface SysMenuMapper extends BaseMapper<SysMenuEntity> {

    /**
     * 根据userId查询菜单权限
     *
     * @param userId 用户id
     * @return 菜单权限集合
     */
    List<String> selectMenuPermsByUserId(Long userId);

    /**
     * 额外实现根据查询条件查询
     * @param sysMenuDto 查询条件
     * @return 菜单集合
     */
    default List<SysMenuEntity> selectAllMenuListByDto(SysMenuDto sysMenuDto) {
        return this.selectList(new QueryWrapper<SysMenuEntity>()
                .like(StringUtils.isNotBlank(sysMenuDto.getMenuName()), "menu_name", sysMenuDto.getMenuName())
                .eq(StringUtils.isNotNull(sysMenuDto.getVisible()), "visible", sysMenuDto.getVisible())
                .eq(StringUtils.isNotNull(sysMenuDto.getMenuStatus()), "menu_status", sysMenuDto.getMenuStatus())
                .orderByAsc("parent_id")
                .orderByAsc("menu_sort")
        );
    }

    /**
     * 额外实现根据查询条件查询
     * @return 菜单集合
     */
    default List<SysMenuEntity> selectMenuTree() {
        return this.selectList(new QueryWrapper<SysMenuEntity>()
                .in("menu_type","M","C")
                .eq("menu_status", "0")
                .orderByAsc("parent_id")
                .orderByAsc("menu_sort")
        );
    }

    /**
     * 根据用户ID查询菜单树信息
     *
     * @param userId 户ID
     * @return 菜单树
     */
    List<SysMenuEntity> selectMenuTreeByUserId(Long userId);

    /**
     * 查询菜单集合
     * @param sysMenuDto 查询条件
     * @return 菜单集合
     */
    List<SysMenuEntity> selectMenuList(@Param("dto") SysMenuDto sysMenuDto);
}
