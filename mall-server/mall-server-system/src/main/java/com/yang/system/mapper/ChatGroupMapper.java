package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.ChatGroupEntity;
import com.yang.im.vo.ChatGroupVo;
import com.yang.im.dto.ChatGroupDto;

import java.util.List;

/**
 * chatGroupMapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface ChatGroupMapper extends BaseMapperPlus<ChatGroupMapper, ChatGroupEntity, ChatGroupVo> {

    /**
     * 查询chatGroup分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<ChatGroupVo> queryPageList(Page<ChatGroupVo> page, @Param("dto") ChatGroupDto dto);

    /**
     * 查询chatGroup列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<ChatGroupVo> queryList(@Param("dto") ChatGroupDto dto);
}
