package com.yang.system.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yang.common.core.utils.Result;
import com.yang.common.core.utils.StringUtils;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.im.dto.VideoShareDto;
import com.yang.im.entity.*;
import com.yang.im.entity.msg.TextMsgEntity;
import com.yang.im.vo.FriendVo;
import com.yang.im.vo.VideoShareVo;
import com.yang.system.dto.SysUserDto;
import com.yang.system.entity.SysUserEntity;
import com.yang.system.service.*;
import com.yang.system.vo.SysUserVo;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.*;

/**
 * im无交互控制器
 *
 * @author yang
 * @email
 * @create 2024/10/19
 */
@RestController
@RequestMapping("/imApp")
public class ImAppController {

    @Resource
    private SysUserService sysUserService;
    @Resource
    private ICustomUrlService iCustomUrlService;
    @Resource
    private IVideoCategoryService iVideoCategoryService;
    @Resource
    private IVideoShareService iVideoShareService;
    @Resource
    private IFriendService iFriendService;
    @Resource
    private IFriendApplyService friendApplyService;
    @Resource
    IChatService chatService;
    @Resource
    IChatListService chatListService;
    @Resource
    IFriendService friendService;
    @Resource
    IChatGroupService chatGroupService;
    @Resource
    IChatMemberService chatMemberService;
    @Resource
    IChatGroupApplyService chatGroupApplyService;

    @RequestMapping("/applyFriend")
    public Result applyFriend() {
        long loginId = StpUtil.getLoginIdAsLong();
        List<FriendApplyEntity> friendApplyEntityList = friendApplyService.list(new QueryWrapper<FriendApplyEntity>()
                .eq("friend_user_id", loginId)
                .eq("action", 0)
                .orderByDesc("time")
        );
        Map<String, Object> objectMap = new HashMap<>();
        List<Map<String, Object>> list = new ArrayList<>();
        for (FriendApplyEntity friendApplyEntity : friendApplyEntityList) {
            Map<String, Object> data = new HashMap<>();
            SysUserVo byUserId = sysUserService.getByUserId(friendApplyEntity.getApplyUserId());
            data.put("id", friendApplyEntity.getId());
            data.put("user_id", friendApplyEntity.getApplyUserId());
            data.put("content", friendApplyEntity.getContent());
            if (friendApplyEntity.getAction() == 0) {
                data.put("text", "未添加");
            } else {
                data.put("text", "已添加");
            }
            data.put("photo", byUserId.getAvatar());
            data.put("nickname", byUserId.getNickName());
            list.add(data);
        }
        objectMap.put("err", 0);
        objectMap.put("msg", "success");
        if (friendApplyEntityList.size() == 0) {
            objectMap.put("data", new String[]{});
        } else {
            objectMap.put("data", list);
        }

        FriendApplyEntity friendApplyEntity = new FriendApplyEntity();
        friendApplyEntity.setIsReader(1);
        friendApplyService.update(friendApplyEntity, new QueryWrapper<FriendApplyEntity>().eq("friend_user_id", loginId));
        return Result.ok(objectMap);
    }

    @RequestMapping("/chatData")
    public Result chatData(@RequestBody String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        String listId = jsonObject.getString("list_id");
        Long time = jsonObject.getLongValue("time");
        String isUp = jsonObject.getString("is_up");
        long loginId = StpUtil.getLoginIdAsLong();

        ChatListEntity chatListEntity = chatListService.getOne(new QueryWrapper<ChatListEntity>()
                .eq("list_id", listId)
                .eq("user_id", loginId)
        );
        // 查询消息
        List<ChatEntity> chatEntities = chatService.list(new QueryWrapper<ChatEntity>()
                .eq("chat_list_id", listId)
                .lt(time != 0, "time", time)
                .orderByDesc("time")
                .last("limit 15")
        );
        if (chatEntities.size() > 0) {
            Collections.reverse(chatEntities);
        }
        List<Map<String, Object>> list = new ArrayList<>();
        for (ChatEntity chatEntity : chatEntities) {
            Map<String, Object> map = new HashMap<>();
            map.put("type", chatEntity.getMsgType());
            Map<String, Object> msg = new HashMap<>();
            msg.put("id", chatEntity.getId());
            msg.put("type", chatEntity.getContentType());
            msg.put("time", chatEntity.getTime());
            msg.put("is_niming", 0);
            Map<String, Object> userInfo = new HashMap<>();
            SysUserVo byUserId = sysUserService.getByUserId(chatEntity.getUserId());
            userInfo.put("uid", chatEntity.getUserId());
            userInfo.put("name", byUserId.getNickName());
            userInfo.put("face", byUserId.getAvatar());
            msg.put("user_info", userInfo);
            int isAdmin = 0;
            if (chatListEntity.getType() == 1) {
                // 查询是不是管理员
                ChatGroupEntity chatGroupEntity = chatGroupService.getOne(new QueryWrapper<ChatGroupEntity>()
                        .eq("list_id", chatListEntity.getListId())
                        .eq("main_id", chatEntity.getUserId())
                );
                if (StringUtils.isNotNull(chatGroupEntity)) {
                    isAdmin = 2;
                } else {
                    ChatMemberEntity chatMemberEntity = chatMemberService.getOne(new QueryWrapper<ChatMemberEntity>()
                            .eq("list_id", chatEntity.getChatListId())
                            .eq("user_id", chatEntity.getUserId())
                    );
                    if (StringUtils.isNotNull(chatMemberEntity)) {
                        isAdmin = chatMemberEntity.getIsAdmin();
                    }
                }
            }
            msg.put("is_admin", isAdmin);
            if (StringUtils.isNotNull(chatEntity.getContent())) {
                TextMsgEntity textMsgEntity = JSONObject.parseObject(chatEntity.getContent(), TextMsgEntity.class);
                msg.put("content", textMsgEntity);
            } else {
                msg.put("content", "");
            }
            map.put("msg", msg);
            list.add(map);
        }

        // 让未阅读数为0
        if (StringUtils.isNotNull(isUp)) {
            ChatListEntity chatListDto = new ChatListEntity();
            chatListDto.setNoReaderNum(0);
            chatListService.update(chatListDto, new QueryWrapper<ChatListEntity>()
                    .eq("user_id", loginId)
                    .eq("list_id", listId));
        }
        int isMsg = 0;
        int isAction = 0;
        Long objId = null;
        String topNotice = "";
        String showName = "";
        switch (chatListEntity.getType()) {
            case 0:
                // 单聊
                objId = chatListEntity.getFriendId();
                FriendVo friendVo = friendService.queryByUserId(loginId, chatListEntity.getFriendId());
                showName = friendVo.getName();
                isAction = 1;
                break;
            case 1:
                break;
        }
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("stime", "");
        objectMap.put("etime", "");
        objectMap.put("ip", "");
        objectMap.put("region", "");
        objectMap.put("list_id", chatListEntity.getListId());
        objectMap.put("type", chatListEntity.getType());
        objectMap.put("show_name", showName);
        objectMap.put("list", list);
        objectMap.put("is_msg", isMsg);
        objectMap.put("is_action", isAction);
        objectMap.put("can_add_friend", "");
        objectMap.put("obj_id", objId);
        objectMap.put("online", 0);
        objectMap.put("receive_list", new String[]{});
        objectMap.put("top_notice", topNotice);

        return Result.ok(objectMap);
    }

    /**
     * 更新会话未读消息为已读
     *
     * @return Result
     */
    @RequestMapping("/updataNoReader")
    public Result updataNoReader(@RequestBody String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        String listId = jsonObject.getString("list_id");
        long loginId = StpUtil.getLoginIdAsLong();

        ChatListEntity chatListDto = new ChatListEntity();
        chatListDto.setNoReaderNum(0);
        chatListService.update(chatListDto, new QueryWrapper<ChatListEntity>()
                .eq("user_id", loginId)
                .eq("list_id", listId));
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("err", 0);
        objectMap.put("msg", "success");
        return Result.ok(objectMap);
    }

    /**
     * 获取会话列表
     *
     * @param json 请求参数
     * @return Result
     */
    @RequestMapping("/chatList")
    public Result chatList(@RequestBody String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        long loginId = StpUtil.getLoginIdAsLong();


        List<ChatListEntity> list = chatListService.list(new QueryWrapper<ChatListEntity>()
                .eq("user_id", loginId)
                .orderByDesc("top", "last_chat_time")
        );
        List<Map<String, Object>> mapList = new ArrayList<>();

        for (ChatListEntity chatListEntity : list) {
            ChatEntity chatEntity = chatService.getOne(new QueryWrapper<ChatEntity>()
                    .eq("chat_list_id", chatListEntity.getListId())
                    .orderByDesc("time")
                    .last("limit 1")
            );
            Map<String, Object> dataMap = new HashMap<>();

            if (chatListEntity.getType() == 0) {
                SysUserVo friendUser = sysUserService.getByUserId(chatListEntity.getFriendId());
                FriendEntity friend = friendService.getOne(new QueryWrapper<FriendEntity>()
                        .eq("user_id", loginId)
                        .eq("friend_id", friendUser.getUserId()));
                if (StringUtils.isNotNull(friend.getRemark())) {
                    dataMap.put("show_name", friend.getRemark());
                } else {
                    dataMap.put("show_name", friendUser.getNickName());
                }
                dataMap.put("photo_path", friendUser.getAvatar());
            } else if (chatListEntity.getType() == 1) {
                ChatGroupEntity chatGroupEntity = chatGroupService.getOne(new QueryWrapper<ChatGroupEntity>()
                        .eq("list_id", chatListEntity.getListId())
                );
                dataMap.put("show_name", chatGroupEntity.getName());
                if (chatGroupEntity.getIsPhoto() == 1) {
                    dataMap.put("photo_path", "/static/photo/group_photo/" + chatListEntity.getListId() + "/90.jpg");
                } else {
                    dataMap.put("photo_path", "/default_group_photo/90.jpg");
                }
            }
            String last_msg = "";
            switch (chatEntity.getContentType()) {
                case 0:
                    TextMsgEntity textMsgEntity = JSON.parseObject(chatEntity.getContent(), TextMsgEntity.class);
                    last_msg = textMsgEntity.getText();
                    break;
                default:
                    break;
            }

            dataMap.put("list_id", chatListEntity.getListId());
            dataMap.put("last_chat_time", chatListEntity.getLastChatTime());
            dataMap.put("chat_id", chatEntity.getId());
            dataMap.put("no_reader_num", chatListEntity.getNoReaderNum());
            dataMap.put("last_msg", last_msg);
            dataMap.put("time", chatEntity.getTime());
            dataMap.put("top", chatListEntity.getTop());
            dataMap.put("top_time", chatListEntity.getTopTime());
            dataMap.put("type", chatListEntity.getType());
            dataMap.put("is_disturb", chatListEntity.getIsDisturb());
            dataMap.put("sort", 0);
            mapList.add(dataMap);
        }
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("data", mapList);
        objectMap.put("err", 0);
        objectMap.put("msg", "");
        return Result.ok(objectMap);
    }

    /**
     * 获取当前登录人的基础信息
     *
     * @return Result
     */
    @RequestMapping("/getUserBase")
    public Result getUserBase() {
        long loginId = StpUtil.getLoginIdAsLong();
        SysUserVo byUserId = sysUserService.getByUserId(StpUtil.getLoginIdAsLong());

        Map<String, Object> data = new HashMap<>();

        Map<String, Object> userInfo = new HashMap<>();
        userInfo.put("id", byUserId.getUserId());
        userInfo.put("nickname", byUserId.getNickName());
        userInfo.put("username", byUserId.getUserName());
        userInfo.put("photo", byUserId.getAvatar());
        userInfo.put("doodling", byUserId.getDoodling());
        userInfo.put("sex", byUserId.getGender());
        userInfo.put("circle_img", "/static/photo/default_circle_img.jpg");
        userInfo.put("money", 1000);
        userInfo.put("trade_password", "10");
        userInfo.put("friend_status", 1);
        userInfo.put("validation", 100);
        data.put("user_info", userInfo);
        int newGroupTipsNum = 0;
        List<ChatListEntity> chatListEntities = chatListService.list(new QueryWrapper<ChatListEntity>()
                .eq("user_id", loginId)
                .eq("type", 1)
        );
        for (ChatListEntity chatListEntity : chatListEntities) {
            List<ChatMemberEntity> chatMemberEntities = chatMemberService.list(new QueryWrapper<ChatMemberEntity>()
                    .eq("list_id", chatListEntity.getListId())
                    .eq("user_id", loginId)
                    .eq("is_admin", 1)
            );
            if (chatMemberEntities.size() > 0) {
                List<ChatGroupApplyEntity> list = chatGroupApplyService.list(new QueryWrapper<ChatGroupApplyEntity>()
                        .eq("list_id", chatListEntity.getListId())
                        .eq("is_reader", 0)
                );
                newGroupTipsNum += list.size();
            }
        }
        data.put("new_group_tips_num", newGroupTipsNum);
        long newFriendTipsNum = friendApplyService.count(new QueryWrapper<FriendApplyEntity>()
                .eq("friend_user_id", loginId)
                .eq("is_reader", 0)
                .eq("action", 0)
        );
        data.put("new_friend_tips_num", newFriendTipsNum);
        int no_reader_chat_num = 0;
        List<ChatListEntity> list = chatListService.list(new QueryWrapper<ChatListEntity>()
                .eq("user_id", loginId)
        );
        for (ChatListEntity chatListEntity : list) {
            no_reader_chat_num += chatListEntity.getNoReaderNum();
        }
        data.put("no_reader_chat_num", no_reader_chat_num);
        data.put("no_reader_circle", 0);
        data.put("no_reader_circle_chat_num", 0);
        data.put("kefu_list_id", "");
        data.put("bottom_url", "https://blog.csdn.net/Blue_Pepsi_Cola/article/details/130614910");
        data.put("pc_page_title", "testaaa");
        data.put("red_envelope", 1);
        data.put("small_change", 2);


        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("err", 0);
        objectMap.put("msg", "success");
        objectMap.put("data", data);
        return Result.ok(objectMap);
    }

    /**
     * 获取外链列表
     *
     * @return Result
     */
    @RequestMapping("/getCustomUrlList")
    public Result getCustomUrlList() {
        List<CustomUrlEntity> status = iCustomUrlService.list(new QueryWrapper<CustomUrlEntity>()
                .eq("status", 1));
        return Result.ok(status);
    }

    /**
     * 获取视频分类列表
     *
     * @return Result
     */
    @RequestMapping("/getVideoCategoryList")
    public Result getVideoCategoryList() {
        List<VideoCategoryEntity> videoCategoryEntityList = iVideoCategoryService.list();
        return Result.ok(videoCategoryEntityList);
    }

    /**
     * 获取视频列表
     *
     * @param dto 查询条件
     * @return Result
     */
    @RequestMapping("/getVideoList")
    public Result getVideoList(@RequestBody VideoShareDto dto) {
        PageView<VideoShareVo> videoShareVoPageView = iVideoShareService.queryPageList(dto);
        return Result.ok(videoShareVoPageView);
    }

    @RequestMapping("/getFriendList")
    public Result getFriendList(@RequestBody String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        Long list_id = jsonObject.getLongValue("list_id");
        long loginId = StpUtil.getLoginIdAsLong();

        List<ChatListEntity> list = chatListService.list(new QueryWrapper<ChatListEntity>()
                .eq("user_id", loginId)
                .eq("type", 0)
        );

        List<FriendVo> friendEntities = friendService.queryListByUserId(loginId);
        for (FriendVo friendEntity : friendEntities) {
            for (ChatListEntity chatListEntity : list) {
                if (chatListEntity.getFriendId().equals(friendEntity.getUserId())) {
                    friendEntity.setListId(chatListEntity.getListId());
                }
            }
        }

        Map<Integer, Object> letterMap = new HashMap<>();
        for (FriendVo friendEntity : friendEntities) {
            int index = getIndex(friendEntity.getLetter());
            letterMap.put(index, "");
        }
        for (Integer integer : letterMap.keySet()) {
            List<Map<String, Object>> dataList = new ArrayList<>();
            for (FriendVo friendEntity : friendEntities) {
                int index = getIndex(friendEntity.getLetter());
                if (index == integer) {
                    Map<String, Object> dataMap = new HashMap<>();
                    dataMap.put("list_id", friendEntity.getListId());
                    dataMap.put("name", friendEntity.getName());
                    dataMap.put("photo", friendEntity.getAvatar());
                    dataMap.put("user_id", friendEntity.getUserId());
                    dataList.add(dataMap);
                }
            }
            Map<String, Object> objectMap1 = new HashMap<>();
            objectMap1.put("data", dataList);
            objectMap1.put("index", integer);
            objectMap1.put("letter", getValue(integer));
            letterMap.put(integer, objectMap1);
        }


        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("member", new String[]{});
        dataMap.put("data", letterMap);

        return Result.ok(dataMap);
    }

    public static void main(String[] args) {
        String[] usernames = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

        System.err.println(usernames[12]);
    }

    public static String getValue(int index) {
        String[] usernames = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        return usernames[index];
    }

    public static int getIndex(String chat) {
        String[] usernames = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
        int index = 0;
        for (int i = 0; i < usernames.length; i++) {
            if (usernames[i].equals(chat)) {
                index = i;
            }
        }
        return index;
    }

    @RequestMapping("/searchUser")
    public Result searchUser(@RequestBody SysUserDto sysUserDto) {
        SysUserEntity byUserName = sysUserService.getByUserName(sysUserDto.getUserName());
        List<SysUserEntity> list = new ArrayList<>();
        if (StringUtils.isNull(byUserName)) {
            return Result.ok(list);
        }
        if (byUserName.getUserId().equals(StpUtil.getLoginIdAsLong())) {
            byUserName.setUserId(0L);
        }
        list.add(byUserName);
        return Result.ok(list);
    }

    /**
     * 查看用户详情时获得数据
     *
     * @param json 请求参数
     * @return Result
     */
    @RequestMapping("/details")
    public Result details(@RequestBody String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        Long userId = jsonObject.getLong("userId");
        Integer in = jsonObject.getInteger("in");
        long loginId = StpUtil.getLoginIdAsLong();
        Map<String, Object> objectMap = new HashMap<>();

        SysUserVo user = sysUserService.getByUserId(userId);

        int isFriend = 0;
        FriendEntity friendEntity = iFriendService.getOne(new QueryWrapper<FriendEntity>()
                .eq("friend_id", userId)
                .eq("user_id", loginId)
        );
        if (StringUtils.isNotNull(friendEntity)) {
            isFriend = 1;
            // TODO 获取朋友圈最近的四张照片
        }
        String content = "";
        Long applyId = null;
        if (null != in && in == 1) {
            // 查询申请列表
            FriendApplyEntity friendApplyEntity = friendApplyService.getOne(new QueryWrapper<FriendApplyEntity>()
                    .eq("apply_user_id", userId)
                    .eq("friend_user_id", loginId)
            );
            if (StringUtils.isNotNull(friendApplyEntity)) {
                applyId = friendApplyEntity.getId();
                content = friendApplyEntity.getContent();
            }
        }


        objectMap.put("is_friend", isFriend);
        Map<String, Object> showFriendMap = new HashMap<>();
        showFriendMap.put("circle", new String[]{});
        objectMap.put("show_friend", showFriendMap);
        objectMap.put("from", "搜索登陆名添加");
        objectMap.put("userId", userId);
        objectMap.put("nickName", user.getNickName());
        objectMap.put("userName", user.getUserName());
        objectMap.put("doodling", user.getDoodling());
        objectMap.put("sex", user.getGender());
        objectMap.put("avatar", user.getAvatar());
        objectMap.put("content", content);
        objectMap.put("applyId", applyId);

        return Result.ok(objectMap);
    }


    // 获取首字母
    public static List<String> getInitials(String username) {
        List<String> initials = new ArrayList<>();
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);

        for (char c : username.toCharArray()) {
            if (Character.toString(c).matches("[\\u4e00-\\u9fa5]")) {
                try {
                    String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(c, format);
                    if (pinyinArray != null && pinyinArray.length > 0) {
                        initials.add(pinyinArray[0].substring(0, 1));
                    }
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            } else {
                initials.add(Character.toString(c));
            }
        }
        return initials;
    }

    // 判断是否是字母并过滤
    public static List<String> filterLetters(List<String> initials) {
        List<String> filteredInitials = new ArrayList<>();
        for (String initial : initials) {
            if (Character.isLetter(initial.charAt(0))) {
                filteredInitials.add(initial);
            }
        }
        return filteredInitials;
    }
}
