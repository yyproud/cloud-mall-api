package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import com.yang.im.dto.CustomUrlDto;
import com.yang.im.vo.CustomUrlVo;
import com.yang.system.service.ICustomUrlService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * 自定义网址控制器
 * 前端访问路由地址为:/im/customUrl
 *
 * @author 杨旭
 * @email
 * @create 2024/10/19 23:28
 */
@Validated
@RestController
@RequestMapping("/customUrl")
public class CustomUrlController {

    @Resource
    private ICustomUrlService iCustomUrlService;

    /**
     * 查询自定义网址列表
     */
    @SaCheckPermission("im:customUrl:list")
    @GetMapping("/list")
    public Result list(CustomUrlDto dto) {
        PageView<CustomUrlVo> list = iCustomUrlService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取自定义网址详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:customUrl:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iCustomUrlService.queryById(id));
    }

    /**
     * 新增自定义网址
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:customUrl:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody CustomUrlDto dto) {
        return iCustomUrlService.insert(dto) ? Result.ok() : Result.error();
    }

    /**
     * 修改自定义网址
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:customUrl:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody CustomUrlDto dto) {
        return iCustomUrlService.update(dto) ? Result.ok() : Result.error();
    }

    /**
     * 删除自定义网址
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:customUrl:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iCustomUrlService.deleteIds(Arrays.asList(ids)) ? Result.ok() : Result.error();
    }
}
