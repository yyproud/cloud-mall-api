package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import com.yang.im.dto.ChatGroupDto;
import com.yang.im.vo.ChatGroupVo;
import com.yang.system.service.IChatGroupService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * chatGroup控制器
 * 前端访问路由地址为:/im/chatGroup
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Validated
@RestController
@RequestMapping("/chatGroup")
public class ChatGroupController {

    @Resource
    private IChatGroupService iChatGroupService;

    /**
     * 查询chatGroup列表
     */
    @SaCheckPermission("im:chatGroup:list")
    @GetMapping("/list")
    public Result list(ChatGroupDto dto) {
        PageView<ChatGroupVo> list = iChatGroupService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取chatGroup详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:chatGroup:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iChatGroupService.queryById(id));
    }

    /**
     * 新增chatGroup
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:chatGroup:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody ChatGroupDto dto) {
        return iChatGroupService.insert(dto) ? Result.ok() : Result.error();
    }

    /**
     * 修改chatGroup
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:chatGroup:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody ChatGroupDto dto) {
        return iChatGroupService.update(dto) ? Result.ok() : Result.error();
    }

    /**
     * 删除chatGroup
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:chatGroup:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iChatGroupService.deleteIds(Arrays.asList(ids)) ? Result.ok() : Result.error();
    }
}
