package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yang.system.mapper.CustomUrlMapper;
import com.yang.system.service.ICustomUrlService;
import org.springframework.stereotype.Service;
import com.yang.im.entity.CustomUrlEntity;
import com.yang.im.vo.CustomUrlVo;
import com.yang.im.dto.CustomUrlDto;
import java.util.List;
import java.util.Collection;

/**
 * 自定义网址Service业务层处理
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/19 23:28
 */
@Service
public class CustomUrlServiceImpl extends ServiceImpl<CustomUrlMapper, CustomUrlEntity> implements ICustomUrlService {

    /**
     * 查询自定义网址
     */
    @Override
    public CustomUrlVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询自定义网址列表
     */
    @Override
    public PageView<CustomUrlVo> queryPageList(CustomUrlDto dto) {
        Page<CustomUrlVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询自定义网址列表
     */
    @Override
    public List<CustomUrlVo> queryList(CustomUrlDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增自定义网址
     */
    @Override
    public Boolean insert(CustomUrlDto dto) {
        CustomUrlEntity add = BeanUtil.toBean(dto, CustomUrlEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改自定义网址
     */
    @Override
    public Boolean update(CustomUrlDto dto) {
        CustomUrlEntity update = BeanUtil.toBean(dto, CustomUrlEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除自定义网址信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
