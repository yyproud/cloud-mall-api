package com.yang.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.yang.system.entity.SysUserEntity;
import com.yang.system.vo.SysUserVo;
import org.apache.ibatis.annotations.Param;

/**
 * 系统用户
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 10:56
 */
public interface SysUserMapper extends BaseMapperPlus<SysUserMapper,SysUserEntity, SysUserVo> {
    /**
     * 根据用户账号查询
     * @param userName 用户账号
     * @return 用户实体
     */
    SysUserEntity getByUserName(String userName);

    /**
     * 根据邀请码查询
     * @param inviteCode 邀请码
     * @return 用户实体
     */
    SysUserEntity getByInviteCode(String inviteCode);
}
