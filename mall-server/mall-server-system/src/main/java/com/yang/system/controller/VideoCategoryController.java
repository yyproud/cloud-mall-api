package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.system.service.IVideoCategoryService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.yang.im.entity.VideoCategoryEntity;
import com.yang.im.vo.VideoCategoryVo;
import com.yang.im.dto.VideoCategoryDto;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import javax.annotation.Resource;
import java.util.List;
import java.util.Arrays;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 短视频分类控制器
 * 前端访问路由地址为:/im/videoCategory
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:11
 */
@Validated
@RestController
@RequestMapping("/videoCategory")
public class VideoCategoryController {

    @Resource
    private IVideoCategoryService iVideoCategoryService;

    /**
     * 查询短视频分类列表
     */
    @SaCheckPermission("im:videoCategory:list")
    @GetMapping("/list")
    public Result list(VideoCategoryDto dto) {
        PageView<VideoCategoryVo> list = iVideoCategoryService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取短视频分类详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:videoCategory:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iVideoCategoryService.queryById(id));
    }

    /**
     * 新增短视频分类
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:videoCategory:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody VideoCategoryDto dto) {
        return iVideoCategoryService.insert(dto) ? Result.ok() :Result.error();
    }

    /**
     * 修改短视频分类
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:videoCategory:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody VideoCategoryDto dto) {
        return iVideoCategoryService.update(dto) ? Result.ok() :Result.error();
    }

    /**
     * 删除短视频分类
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:videoCategory:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iVideoCategoryService.deleteIds(Arrays.asList(ids)) ? Result.ok() :Result.error();
    }
}
