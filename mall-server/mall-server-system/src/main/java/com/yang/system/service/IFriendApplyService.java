package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.FriendApplyEntity;
import com.yang.im.vo.FriendApplyVo;
import com.yang.im.dto.FriendApplyDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * friendApplyService接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface IFriendApplyService extends IService<FriendApplyEntity>{

    /**
     * 查询friendApply
     *
     * @param id 参数
     * @return friendApply实体
     */
    FriendApplyVo queryById(Long id);

    /**
     * 查询friendApply列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<FriendApplyVo> queryPageList(FriendApplyDto dto);

    /**
     * 查询friendApply列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<FriendApplyVo> queryList(FriendApplyDto dto);

    /**
     * 新增friendApply
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(FriendApplyDto dto);

    /**
     * 修改friendApply
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(FriendApplyDto dto);

    /**
     * 批量删除friendApply信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
