package com.yang.system;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.metrics.buffering.BufferingApplicationStartup;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 系统服务
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 9:43
 */
@EnableDubbo
@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("com.yang.system.mapper")
public class MallServerSystemApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(MallServerSystemApplication.class);
        application.setApplicationStartup(new BufferingApplicationStartup(2048));
        application.run(args);
    }
}
