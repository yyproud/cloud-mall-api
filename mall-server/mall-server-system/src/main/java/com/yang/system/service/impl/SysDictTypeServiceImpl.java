package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yang.common.redis.utils.CacheUtils;
import com.yang.common.satoken.utils.LoginHelper;
import org.springframework.stereotype.Service;
import com.yang.system.entity.SysDictTypeEntity;
import com.yang.system.vo.SysDictTypeVo;
import com.yang.system.dto.SysDictTypeDto;
import com.yang.system.mapper.SysDictTypeMapper;
import com.yang.system.service.ISysDictTypeService;

import java.util.Date;
import java.util.List;
import java.util.Collection;

/**
 * 字典类型Service业务层处理
 *
 * @author 杨旭
 * @email
 * @create 2023/10/16 17:11
 */
@Service
public class SysDictTypeServiceImpl extends ServiceImpl<SysDictTypeMapper, SysDictTypeEntity> implements ISysDictTypeService {

    /**
     * 查询字典类型
     */
    @Override
    public SysDictTypeVo queryById(Long dictId) {
        return this.baseMapper.selectVoById(dictId);
    }

    /**
     * 查询字典类型列表
     */
    @Override
    public PageView<SysDictTypeVo> queryPageList(SysDictTypeDto dto) {
       return null;
    }

    /**
     * 查询字典类型列表
     */
    @Override
    public List<SysDictTypeVo> queryList(SysDictTypeDto dto) {
        return this.baseMapper.queryList(dto);
    }

    /**
     * 新增字典类型
     */
    @Override
    public Boolean insert(SysDictTypeDto dto) {
        SysDictTypeEntity add = BeanUtil.toBean(dto, SysDictTypeEntity.class);
        add.setCreateId(LoginHelper.getUserId());
        add.setCreateTime(new Date());
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改字典类型
     */
    @Override
    public Boolean update(SysDictTypeDto dto) {
        SysDictTypeEntity update = BeanUtil.toBean(dto, SysDictTypeEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除字典类型信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    public List<SysDictTypeVo> selectDictTypeAll() {
        List<SysDictTypeVo> sysDictTypeEntities = this.baseMapper.selectVoList(new LambdaQueryWrapper<SysDictTypeEntity>()
                .eq(SysDictTypeEntity::getStatus, SysDictTypeEntity.STATUS_VALID)
                .orderByAsc(SysDictTypeEntity::getDictId));
        return sysDictTypeEntities;
    }
}
