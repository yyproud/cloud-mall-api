package com.yang.system.dubbo;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yang.api.system.dubbo.RangedChatListService;
import com.yang.im.dto.ChatDto;
import com.yang.im.dto.ChatListDto;
import com.yang.im.dto.ChatMemberDto;
import com.yang.im.entity.ChatListEntity;
import com.yang.im.entity.ChatMemberEntity;
import com.yang.system.service.IChatListService;
import com.yang.system.service.IChatMemberService;
import com.yang.system.service.IChatService;
import org.apache.dubbo.config.annotation.DubboService;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: yang
 * @CreateTime: 2024-10-25
 */
@DubboService
public class RangedChatListServiceImpl implements RangedChatListService {

    @Resource
    IChatListService chatListService;
    @Resource
    IChatMemberService chatMemberService;
    @Resource
    IChatService chatService;

    @Override
    public ChatListEntity queryChatListId(Long userId, Long friendId) {
        return chatListService.getOne(new QueryWrapper<ChatListEntity>()
                .eq("user_id", userId)
                .eq("friend_id", friendId));
    }

    @Override
    public Boolean insert(ChatListDto dto) {
        return chatListService.insert(dto);
    }

    @Override
    public Boolean insertMember(ChatMemberDto chatMemberDto) {
        return chatMemberService.insert(chatMemberDto);
    }

    @Override
    public Boolean addChat(ChatDto chatDto) {
        return chatService.insert(chatDto);
    }

    @Override
    public List<ChatMemberEntity> queryMember(String listId) {
        return chatMemberService.list(new QueryWrapper<ChatMemberEntity>().eq("list_id", listId));
    }
}
