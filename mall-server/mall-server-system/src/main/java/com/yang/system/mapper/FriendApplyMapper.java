package com.yang.system.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.FriendApplyEntity;
import com.yang.im.vo.FriendApplyVo;
import com.yang.im.dto.FriendApplyDto;

import java.util.List;

/**
 * friendApplyMapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/24 23:52
 */
public interface FriendApplyMapper extends BaseMapperPlus<FriendApplyMapper, FriendApplyEntity, FriendApplyVo> {

    /**
     * 查询friendApply分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<FriendApplyVo> queryPageList(Page<FriendApplyVo> page, @Param("dto") FriendApplyDto dto);

    /**
     * 查询friendApply列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<FriendApplyVo> queryList(@Param("dto") FriendApplyDto dto);
}
