package com.yang.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.VideoCategoryEntity;
import com.yang.im.vo.VideoCategoryVo;
import com.yang.im.dto.VideoCategoryDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * 短视频分类Service接口
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:11
 */
public interface IVideoCategoryService extends IService<VideoCategoryEntity>{

    /**
     * 查询短视频分类
     *
     * @param id 参数
     * @return 短视频分类实体
     */
    VideoCategoryVo queryById(Long id);

    /**
     * 查询短视频分类列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<VideoCategoryVo> queryPageList(VideoCategoryDto dto);

    /**
     * 查询短视频分类列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<VideoCategoryVo> queryList(VideoCategoryDto dto);

    /**
     * 新增短视频分类
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(VideoCategoryDto dto);

    /**
     * 修改短视频分类
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(VideoCategoryDto dto);

    /**
     * 批量删除短视频分类信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
