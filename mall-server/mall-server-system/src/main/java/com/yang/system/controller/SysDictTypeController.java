package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.yang.system.vo.SysDictTypeVo;
import com.yang.system.dto.SysDictTypeDto;
import com.yang.system.service.ISysDictTypeService;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 字典类型控制器
 * 前端访问路由地址为:/system/dictType
 *
 * @author 杨旭
 * @email  
 * @create 2023/10/16 17:11
 */
@Validated
@RestController
@RequestMapping("/dictType")
public class SysDictTypeController {

    @Resource
    private ISysDictTypeService iSysDictTypeService;

    /**
     * 查询字典类型列表
     */
    @SaCheckPermission("system:dictType:list")
    @GetMapping("/list")
    public Result list(SysDictTypeDto dto) {
        PageView<SysDictTypeVo> list = iSysDictTypeService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取字典类型详细信息
     *
     * @param dictId 主键
     */
    @SaCheckPermission("system:dictType:query")
    @GetMapping("/{dictId}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long dictId) {
        return Result.ok(iSysDictTypeService.queryById(dictId));
    }

    /**
     * 新增字典类型
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("system:dictType:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody SysDictTypeDto dto) {
        return iSysDictTypeService.insert(dto) ? Result.ok() :Result.error();
    }

    /**
     * 修改字典类型
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("system:dictType:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody SysDictTypeDto dto) {
        return iSysDictTypeService.update(dto) ? Result.ok() :Result.error();
    }

    /**
     * 删除字典类型
     *
     * @param dictIds 主键串
     * @return 删除结果
     */
    @SaCheckPermission("system:dictType:remove")
    @DeleteMapping("/{dictIds}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] dictIds) {
        return iSysDictTypeService.deleteIds(Arrays.asList(dictIds)) ? Result.ok() :Result.error();
    }

    /**
     * 获取字典选择框列表
     */
    @SaCheckPermission("system:dictType:remove")
    @GetMapping("/optionSelect")
    public Result optionSelect() {
        List<SysDictTypeVo> dictTypes = iSysDictTypeService.selectDictTypeAll();
        return Result.ok(dictTypes);
    }

}
