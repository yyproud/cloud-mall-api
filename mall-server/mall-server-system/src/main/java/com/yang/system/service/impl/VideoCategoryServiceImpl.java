package com.yang.system.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.yang.im.entity.VideoCategoryEntity;
import com.yang.im.vo.VideoCategoryVo;
import com.yang.im.dto.VideoCategoryDto;
import com.yang.system.mapper.VideoCategoryMapper;
import com.yang.system.service.IVideoCategoryService;

import java.util.List;
import java.util.Collection;

/**
 * 短视频分类Service业务层处理
 *
 * @author yang
 * @email  
 * @create 2024/10/20 20:11
 */
@Service
public class VideoCategoryServiceImpl extends ServiceImpl<VideoCategoryMapper, VideoCategoryEntity> implements IVideoCategoryService {

    /**
     * 查询短视频分类
     */
    @Override
    public VideoCategoryVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询短视频分类列表
     */
    @Override
    public PageView<VideoCategoryVo> queryPageList(VideoCategoryDto dto) {
        Page<VideoCategoryVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询短视频分类列表
     */
    @Override
    public List<VideoCategoryVo> queryList(VideoCategoryDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增短视频分类
     */
    @Override
    public Boolean insert(VideoCategoryDto dto) {
        VideoCategoryEntity add = BeanUtil.toBean(dto, VideoCategoryEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改短视频分类
     */
    @Override
    public Boolean update(VideoCategoryDto dto) {
        VideoCategoryEntity update = BeanUtil.toBean(dto, VideoCategoryEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除短视频分类信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
