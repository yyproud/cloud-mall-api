package com.yang.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.satoken.utils.LoginHelper;
import com.yang.common.core.utils.StringUtils;
import com.yang.system.entity.SysRoleEntity;
import com.yang.system.mapper.SysRoleMapper;
import com.yang.system.service.SysRoleService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 系统角色
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/8/26 20:18
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRoleEntity> implements SysRoleService {
    @Override
    public Set<String> selectRolePermissionByUserId(Long userId) {
        Set<String> permsSet = new HashSet<>();
        // 判断是否为超级管理员
        if (LoginHelper.isAdmin(userId)) {
            permsSet.add("admin");
        }else {
            // 非管理员获取当前用户角色
            List<String> perms = this.baseMapper.selectRolePermissionByUserId(userId);
            for (String perm : perms) {
                if (StringUtils.isNotEmpty(perm)) {
                    permsSet.addAll(StringUtils.splitList(perm.trim()));
                }
            }
        }
        return permsSet;
    }
}
