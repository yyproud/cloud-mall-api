package com.yang.system.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import com.yang.im.dto.ChatDto;
import com.yang.im.vo.ChatVo;
import com.yang.system.service.IChatService;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Arrays;

/**
 * chat控制器
 * 前端访问路由地址为:/im/chat
 *
 * @author 杨旭
 * @email
 * @create 2024/10/24 23:52
 */
@Validated
@RestController
@RequestMapping("/chat")
public class ChatController {

    @Resource
    private IChatService iChatService;

    /**
     * 查询chat列表
     */
    @SaCheckPermission("im:chat:list")
    @GetMapping("/list")
    public Result list(ChatDto dto) {
        PageView<ChatVo> list = iChatService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取chat详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:chat:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iChatService.queryById(id));
    }

    /**
     * 新增chat
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:chat:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody ChatDto dto) {
        return iChatService.insert(dto) ? Result.ok() : Result.error();
    }

    /**
     * 修改chat
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:chat:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody ChatDto dto) {
        return iChatService.update(dto) ? Result.ok() : Result.error();
    }

    /**
     * 删除chat
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:chat:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iChatService.deleteIds(Arrays.asList(ids)) ? Result.ok() : Result.error();
    }
}
