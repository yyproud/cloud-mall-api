package com.yang.im.controller;

import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson2.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yang.api.system.dubbo.RangedChatListService;
import com.yang.api.system.dubbo.RangedFriendService;
import com.yang.api.system.dubbo.RangedSysUserService;
import com.yang.common.core.utils.Result;
import com.yang.common.core.utils.StringUtils;
import com.yang.im.dto.*;
import com.yang.im.entity.*;
import com.yang.im.entity.msg.TextMsgEntity;
import com.yang.im.service.IListTimeService;
import com.yang.im.service.SocketMessageService;
import com.yang.im.vo.msg.ChatMessageVO;
import com.yang.system.entity.SysUserEntity;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * im交互控制器
 *
 * @author 杨旭
 * @email
 * @create 2024/10/22
 */
@RestController
@RequestMapping("/imAppAction")
public class ActionController {

    @DubboReference
    RangedFriendService friendService;
    @DubboReference
    RangedSysUserService sysUserService;
    @DubboReference
    RangedChatListService chatListService;
    @Resource
    SocketMessageService socketMessageService;
    @Resource
    IListTimeService listTimeService;

    @RequestMapping("/getListTime")
    public Result getListTime(@RequestBody ListTimeDto listTimeDto) {
        Long loginId = StpUtil.getLoginIdAsLong();
        List<ChatMemberEntity> chatMemberEntities = chatListService.queryMember(listTimeDto.getListId());

        long readTime = 0;
        for (ChatMemberEntity chatMemberEntity : chatMemberEntities) {
            if (!chatMemberEntity.getUserId().equals(loginId)) {
                ListTimeEntity one = listTimeService.getOne(new QueryWrapper<ListTimeEntity>()
                        .eq("list_id", listTimeDto.getListId())
                        .eq("user_id", chatMemberEntity.getUserId())
                );
                if (null != one) {
                    readTime = one.getReadTime();
                }
            }
        }
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("err", 0);
        objectMap.put("msg", "success");
        objectMap.put("online", 0);
        objectMap.put("read_time", readTime);
        return Result.ok(objectMap);
    }

    @RequestMapping("/setListTime")
    public Result setListTime(@RequestBody ListTimeDto listTimeDto) {
        Long readTime = System.currentTimeMillis() / 1000;
        Long loginId = StpUtil.getLoginIdAsLong();
        ListTimeEntity one = listTimeService.getOne(new QueryWrapper<ListTimeEntity>()
                .eq("list_id", listTimeDto.getListId())
                .eq("user_id", loginId)
        );
        if (StringUtils.isNotNull(one)) {
            one.setReadTime(readTime);
            listTimeService.update(one, new QueryWrapper<ListTimeEntity>()
                    .eq("list_id", listTimeDto.getListId())
                    .eq("user_id", loginId)
            );
        } else {
            ListTimeDto listTimeEntity = new ListTimeDto();
            listTimeEntity.setListId(listTimeDto.getListId());
            listTimeEntity.setUserId(loginId);
            listTimeEntity.setReadTime(readTime);
            listTimeService.insert(listTimeEntity);
        }

        List<ChatMemberEntity> chatMemberEntities = chatListService.queryMember(listTimeDto.getListId());
        for (ChatMemberEntity chatMemberEntity : chatMemberEntities) {
            if (!chatMemberEntity.getUserId().equals(loginId)) {
                ChatMessageVO chatMessageVO = new ChatMessageVO();
                chatMessageVO.setAction("timeData");
                Map<String, Object> data = new HashMap<>();
                data.put("list_id", listTimeDto.getListId());
                Map<String,Object> objectMap = new HashMap<>();
                objectMap.put("type",2);
                Map<String,Object> msg = new HashMap<>();
                msg.put("time",readTime);
                objectMap.put("msg",msg);
                data.put("data",objectMap);
                chatMessageVO.setData(data);
                socketMessageService.sendMessage(String.valueOf(loginId), chatMessageVO);
            }
        }

        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("err", 0);
        objectMap.put("msg", "success");
        return Result.ok(objectMap);
    }

    @RequestMapping("/friendAddAction")
    public Result friendAddAction(@RequestBody String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        Long applyId = jsonObject.getLongValue("apply_id");
        long loginId = StpUtil.getLoginIdAsLong();

        FriendApplyEntity applyById = friendService.getApplyById(applyId);
        Map<String, Object> objectMap = new HashMap<>();
        objectMap.put("err", 1);
        if (StringUtils.isNull(applyById)) {
            objectMap.put("msg", "没有这条申请数据");
            return Result.ok(objectMap);
        }
        if (applyById.getAction() == 1) {
            objectMap.put("msg", "已经操作过这条数据了");
            return Result.ok(objectMap);
        }
        FriendEntity friendEntity = friendService.queryFriendById(applyById.getApplyUserId(), loginId);
        if (StringUtils.isNotNull(friendEntity)) {
            objectMap.put("msg", "对方已经是你的好友了");
            return Result.ok(objectMap);
        }
        FriendEntity friendDel = friendService.queryFriendById(loginId, applyById.getApplyUserId());
        String listId;
        if (StringUtils.isNotNull(friendDel)) {
            ChatListEntity chatListEntity = chatListService.queryChatListId(applyById.getApplyUserId(), loginId);
            listId = chatListEntity.getListId();
        } else {
            SecureRandom random = new SecureRandom();
            listId = String.valueOf(random.nextLong());
        }
        FriendApplyDto friendApplyDto = new FriendApplyDto();
        friendApplyDto.setId(applyId);
        friendApplyDto.setAction(1);
        friendService.updateApply(friendApplyDto);
        // 增加会话列表
        ChatListDto dto = new ChatListDto();
        dto.setUserId(loginId);
        dto.setFriendId(applyById.getApplyUserId());
        dto.setListId(listId);
        dto.setType(0);
        chatListService.insert(dto);
        // 增加到好友表
        FriendDto friendDto = new FriendDto();
        friendDto.setUserId(loginId);
        friendDto.setFriendId(applyById.getApplyUserId());
        friendDto.setFriendFrom(applyById.getFriendFrom());
        friendService.insert(friendDto);

        if (StringUtils.isNull(friendDel)) {
            // 增加会话列表
            ChatListDto newDto = new ChatListDto();
            newDto.setUserId(applyById.getApplyUserId());
            newDto.setFriendId(loginId);
            newDto.setListId(listId);
            newDto.setType(0);
            chatListService.insert(newDto);
            // 增加到成员表
            ChatMemberDto chatMemberDto = new ChatMemberDto();
            chatMemberDto.setUserId(applyById.getApplyUserId());
            chatMemberDto.setListId(listId);
            chatListService.insertMember(chatMemberDto);
            // 增加到好友表
            FriendDto newFriendDto = new FriendDto();
            newFriendDto.setUserId(applyById.getApplyUserId());
            newFriendDto.setFriendId(loginId);
            newFriendDto.setFriendFrom(applyById.getFriendFrom());
            friendService.insert(newFriendDto);
        }

        if (StringUtils.isNotNull(applyById.getContent())) {
            ChatDto chatDto = new ChatDto();
            chatDto.setChatListId(listId);
            chatDto.setUserId(applyById.getApplyUserId());
            chatDto.setContentType(0);
            chatDto.setMsgType(0);
            TextMsgEntity textMsgEntity = new TextMsgEntity();
            textMsgEntity.setText(applyById.getContent());
            chatDto.setContent(JSONObject.toJSONString(textMsgEntity));
            chatDto.setTime(applyById.getTime());
            chatListService.addChat(chatDto);
        }
        ChatDto chatDto = new ChatDto();
        chatDto.setChatListId(listId);
        chatDto.setUserId(loginId);
        chatDto.setContentType(0);
        chatDto.setMsgType(0);
        TextMsgEntity textMsgEntity = new TextMsgEntity();
        textMsgEntity.setText("我通过了你的朋友验证请求,现在我们可以开始聊天了");
        chatDto.setContent(JSONObject.toJSONString(textMsgEntity));
        chatDto.setTime(System.currentTimeMillis() / 1000);
        chatListService.addChat(chatDto);

        // 通知双方重新获取会话列表和通讯录数据
        ChatMessageVO getChatList = new ChatMessageVO();
        getChatList.setAction("getChatList");
        ChatMessageVO getFriendList = new ChatMessageVO();
        getFriendList.setAction("getFriendList");
        socketMessageService.sendMessage(String.valueOf(loginId), getChatList);
        socketMessageService.sendMessage(String.valueOf(loginId), getFriendList);
        socketMessageService.sendMessage(applyById.getApplyUserId().toString(), getChatList);
        socketMessageService.sendMessage(applyById.getApplyUserId().toString(), getFriendList);

        objectMap.put("err", 0);
        objectMap.put("msg", "success");
        return Result.ok(objectMap);
    }

    @RequestMapping("/getListId")
    public Result getListId(@RequestBody String json) {
        JSONObject jsonObject = JSONObject.parseObject(json);
        Long friendId = jsonObject.getLongValue("user_id");
        long loginId = StpUtil.getLoginIdAsLong();
        Map<String, Object> objectMap = new HashMap<>();
        ChatListEntity chatListEntity = chatListService.queryChatListId(loginId, friendId);
        if (StringUtils.isNotNull(chatListEntity)) {
            objectMap.put("err", 0);
            objectMap.put("msg", "success");
            Map<String, Object> dataMap = new HashMap<>();
            dataMap.put("list_id", chatListEntity.getListId());
            objectMap.put("data", dataMap);
        }

        return Result.ok(objectMap);
    }


    /**
     * 添加好友
     * RangedFriendListService
     *
     * @return Result
     */
    @RequestMapping("/friendAdd")
    public Result friendAdd(@RequestBody FriendApplyDto friendApplyDto) {
        Map<String, Object> objectMap = new HashMap<>();
        friendApplyDto.setApplyUserId(StpUtil.getLoginIdAsLong());

        // 查询是否为好友了
        FriendEntity friendEntity = friendService.queryFriendById(friendApplyDto.getFriendUserId(), friendApplyDto.getApplyUserId());
        FriendEntity userEntity = friendService.queryFriendById(friendApplyDto.getApplyUserId(), friendApplyDto.getFriendUserId());
        if (StringUtils.isNotNull(friendEntity) && StringUtils.isNotNull(userEntity)) {
            objectMap.put("err", 1);
            objectMap.put("msg", "对方已经是你的好友了");
            return Result.ok(objectMap);
        }

        SysUserEntity friendInfo = sysUserService.getUserById(friendApplyDto.getFriendUserId());

        if (StringUtils.isNotNull(userEntity)) {
            // 这里是我把对方删除了
            FriendDto dto = new FriendDto();
            dto.setFriendId(friendApplyDto.getFriendUserId());
            dto.setUserId(friendApplyDto.getApplyUserId());
            dto.setFriendFrom(friendApplyDto.getFriendFrom());
            List<String> initials = getInitials(friendInfo.getNickName());
            List<String> filteredInitials = filterLetters(initials);
            if (filteredInitials.size() > 0) {
                char upperCase = Character.toUpperCase(filteredInitials.get(0).charAt(0));
                dto.setLetter(Character.toString(upperCase));
            } else {
                dto.setLetter("Z");
            }
            Boolean insert = friendService.insert(dto);
            if (insert) {
                // 添加会话
                // 从成员表中查询出listId
                ChatListEntity listId = chatListService.queryChatListId(friendApplyDto.getFriendUserId(), friendApplyDto.getApplyUserId());
                ChatListDto chatListDto = new ChatListDto();
                chatListDto.setListId(listId.getListId());
                chatListDto.setUserId(friendApplyDto.getApplyUserId());
                chatListDto.setFriendId(friendApplyDto.getFriendUserId());
                chatListDto.setType(0);
                chatListService.insert(chatListDto);
                objectMap.put("err", 1);
                objectMap.put("msg", "添加成功");
                return Result.ok(objectMap);
            }
        }

        // 检查申请是否存在
        FriendApplyEntity friendApplyEntity = friendService.checkApply(friendApplyDto.getFriendUserId(), friendApplyDto.getApplyUserId());
        if (StringUtils.isNotNull(friendApplyEntity)) {
            friendApplyDto.setId(friendApplyEntity.getId());
            Boolean updated = friendService.updateApply(friendApplyDto);
            if (updated) {
                objectMap.put("err", 0);
                objectMap.put("msg", "已申请添加");
            }
        } else {
            Boolean insert = friendService.insertApply(friendApplyDto);
            if (insert) {
                objectMap.put("err", 0);
                objectMap.put("msg", "已申请添加");
            }
        }


        // 发送添加好友提示消息
        ChatMessageVO chatMessageVO = new ChatMessageVO();
        chatMessageVO.setAction("newFriend");
        Map<String, Object> data = new HashMap<>();
        data.put("num", 1);
        chatMessageVO.setData(data);
        socketMessageService.sendMessage(friendApplyDto.getFriendUserId().toString(), chatMessageVO);
        return Result.ok(objectMap);
    }


    // 获取首字母
    public static List<String> getInitials(String username) {
        List<String> initials = new ArrayList<>();
        HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
        format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
        format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);

        for (char c : username.toCharArray()) {
            if (Character.toString(c).matches("[\\u4e00-\\u9fa5]")) {
                try {
                    String[] pinyinArray = PinyinHelper.toHanyuPinyinStringArray(c, format);
                    if (pinyinArray != null && pinyinArray.length > 0) {
                        initials.add(pinyinArray[0].substring(0, 1));
                    }
                } catch (BadHanyuPinyinOutputFormatCombination e) {
                    e.printStackTrace();
                }
            } else {
                initials.add(Character.toString(c));
            }
        }
        return initials;
    }

    // 判断是否是字母并过滤
    public static List<String> filterLetters(List<String> initials) {
        List<String> filteredInitials = new ArrayList<>();
        for (String initial : initials) {
            if (Character.isLetter(initial.charAt(0))) {
                filteredInitials.add(initial);
            }
        }
        return filteredInitials;
    }
}
