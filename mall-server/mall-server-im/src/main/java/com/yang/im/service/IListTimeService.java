package com.yang.im.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yang.im.entity.ListTimeEntity;
import com.yang.im.vo.ListTimeVo;
import com.yang.im.dto.ListTimeDto;
import com.yang.common.mybatis.core.page.PageView;


import java.util.Collection;
import java.util.List;

/**
 * 消息最后阅读时间Service接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 23:03
 */
public interface IListTimeService extends IService<ListTimeEntity>{

    /**
     * 查询消息最后阅读时间
     *
     * @param id 参数
     * @return 消息最后阅读时间实体
     */
    ListTimeVo queryById(Long id);

    /**
     * 查询消息最后阅读时间列表
     *
     * @param dto 查询条件
     * @return 分页数据
     */
    PageView<ListTimeVo> queryPageList(ListTimeDto dto);

    /**
     * 查询消息最后阅读时间列表
     *
     * @param dto 查询条件
     * @return 数据
     */
    List<ListTimeVo> queryList(ListTimeDto dto);

    /**
     * 新增消息最后阅读时间
     *
     * @param dto 实体数据
     * @return 新增结果
     */
    Boolean insert(ListTimeDto dto);

    /**
     * 修改消息最后阅读时间
     *
     * @param dto 实体数据
     * @return 修改结果
     */
    Boolean update(ListTimeDto dto);

    /**
     * 批量删除消息最后阅读时间信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    Boolean deleteIds(Collection<Long> ids);
}
