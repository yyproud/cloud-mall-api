package com.yang.im;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 即时通讯服务启动类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/16 11:26
 */
@EnableAsync
@EnableDubbo
@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("${mybatis-plus.mapperPackage}")
public class MallServerImApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallServerImApplication.class, args);
    }
}
