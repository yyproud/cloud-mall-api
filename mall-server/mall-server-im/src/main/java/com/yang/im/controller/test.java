package com.yang.im.controller;

import cn.hutool.core.date.DateUtil;
import com.yang.common.core.utils.Result;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/14 16:49
 */
@Api(tags = "杨旭", value = "获取用户信息")
@RestController
@RequestMapping("/imtest")
public class test {


    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @ApiOperation(value = "getInfo", notes = "getInfo")
    @GetMapping("getInfo")
    public Result getInfo() {
        int maxRetries = 3;
        int retryCount = 0;
        boolean success = false;
        // 初始延迟为1秒
        int delay = 2000;
        while (retryCount < maxRetries) {
            try {
                // 尝试获取锁
                success = false;
                if (retryCount == 2) {
                    success = true;
                }
                if (success) {
                    return Result.error();
                } else {
                    // 重试
                    retryCount++;
                    Thread.sleep(delay);
                    delay *= 2;
                }
                System.err.println(DateUtil.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return Result.ok();
    }
}
