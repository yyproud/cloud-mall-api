package com.yang.im.service;

/**
 * IPService接口
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/30 13:42
 */
public interface IpService {

    /**
     * 更新用户的IP归属地
     *
     * @param userId
     */
    void updateIpHomeLocation(String userId);
}
