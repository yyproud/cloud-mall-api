package com.yang.im.strategy.msg;

import com.yang.im.dto.msg.ImgMsgDTO;
import com.yang.im.enums.ChatMessageTypeEnum;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2024/3/14 10:30
 */
@Component
@Slf4j
public class ImgMsgHandler extends AbstractMsgHandler<ImgMsgDTO>{
    @Override
    ChatMessageTypeEnum getMsgTypeEnum() {
        return ChatMessageTypeEnum.IMG;
    }

    @Override
    protected void saveMsg(ImgMsgDTO body, NioSocketChannel channel) {
        log.error("保存图片消息:{}",body);
    }
}
