package com.yang.im.listener;

import com.yang.common.redis.utils.RedisUtils;
import com.yang.im.event.UserOfflineEvent;
import com.yang.im.event.UserOnlineEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * 用户下线监听器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/29 17:17
 */
@Slf4j
@Component
public class UserOfflineListener {

    @Async
    @EventListener(classes = UserOfflineEvent.class)
    public void push(UserOfflineEvent offlineUser) {
        log.info("用户下线：{}", offlineUser.toString());
    }

    @Async
    @EventListener(classes = UserOfflineEvent.class)
    public void save(UserOfflineEvent offlineUser) {
        RedisUtils.delCacheMapValue(
                "chatLinkInfoList",
                offlineUser.getChatLinkInfoVo().getUserId());
        log.info("用户下线：{}", offlineUser.toString());
    }
}
