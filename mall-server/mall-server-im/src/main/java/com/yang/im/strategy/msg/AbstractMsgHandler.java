package com.yang.im.strategy.msg;

import cn.hutool.core.bean.BeanUtil;
import com.yang.im.dto.msg.ChatMessageDTO;
import com.yang.im.enums.ChatMessageTypeEnum;
import io.netty.channel.socket.nio.NioSocketChannel;

import javax.annotation.PostConstruct;
import java.lang.reflect.ParameterizedType;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2024/3/7 15:48
 */
public abstract class AbstractMsgHandler<T> {

    private Class<T> bodyClass;

    @PostConstruct
    private void init() {
        ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
        this.bodyClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
        MsgHandlerFactory.register(getMsgTypeEnum().getAction(), this);
    }

    abstract ChatMessageTypeEnum getMsgTypeEnum();


    public void checkAndSaveMsg(ChatMessageDTO request, NioSocketChannel channel) {
        T body = this.toBean(request.getData());

        //子类扩展保存
        saveMsg(body,channel);
    }

    protected abstract void saveMsg(T body,NioSocketChannel channel);

    private T toBean(Object body) {
        if (bodyClass.isAssignableFrom(body.getClass())) {
            return bodyClass.cast(body);
        }
        return BeanUtil.toBean(body, bodyClass);
    }
}
