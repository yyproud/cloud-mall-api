package com.yang.im.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yang.common.mybatis.core.page.PageView;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.stereotype.Service;
import com.yang.im.entity.ListTimeEntity;
import com.yang.im.vo.ListTimeVo;
import com.yang.im.dto.ListTimeDto;
import com.yang.im.mapper.ListTimeMapper;
import com.yang.im.service.IListTimeService;

import java.util.List;
import java.util.Collection;

/**
 * 消息最后阅读时间Service业务层处理
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 23:03
 */
@Service
public class ListTimeServiceImpl extends ServiceImpl<ListTimeMapper, ListTimeEntity> implements IListTimeService {

    /**
     * 查询消息最后阅读时间
     */
    @Override
    public ListTimeVo queryById(Long id){
        return this.baseMapper.selectVoById(id);
    }

    /**
     * 查询消息最后阅读时间列表
     */
    @Override
    public PageView<ListTimeVo> queryPageList(ListTimeDto dto) {
        Page<ListTimeVo> page = this.baseMapper.queryPageList(dto.build(), dto);
        return PageView.build(page);
    }

    /**
     * 查询消息最后阅读时间列表
     */
    @Override
    public List<ListTimeVo> queryList(ListTimeDto dto) {
        return this.baseMapper.queryList(dto);
    }


    /**
     * 新增消息最后阅读时间
     */
    @Override
    public Boolean insert(ListTimeDto dto) {
        ListTimeEntity add = BeanUtil.toBean(dto, ListTimeEntity.class);
        return this.baseMapper.insert(add) > 0;
    }

    /**
     * 修改消息最后阅读时间
     */
    @Override
    public Boolean update(ListTimeDto dto) {
        ListTimeEntity update = BeanUtil.toBean(dto, ListTimeEntity.class);
        return this.baseMapper.updateById(update) > 0;
    }

    /**
     * 批量删除消息最后阅读时间信息
     *
     * @param ids id集合
     * @return 删除结果
     */
    @Override
    public Boolean deleteIds(Collection<Long> ids) {
        return this.baseMapper.deleteBatchIds(ids) > 0;
    }
}
