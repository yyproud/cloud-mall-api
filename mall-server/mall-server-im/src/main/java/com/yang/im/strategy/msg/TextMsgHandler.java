package com.yang.im.strategy.msg;

import com.yang.im.dto.msg.BroadcastingMessageDTO;
import com.yang.im.dto.msg.TextMsgDTO;
import com.yang.im.enums.ChatMessageTypeEnum;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2024/3/14 10:30
 */
@Slf4j
@Component
public class TextMsgHandler extends AbstractMsgHandler<TextMsgDTO>{
    @Override
    ChatMessageTypeEnum getMsgTypeEnum() {
        return ChatMessageTypeEnum.TEXT;
    }
    @Resource
    private RocketMQTemplate rocketmqtemplate;

    @Override
    protected void saveMsg(TextMsgDTO body, NioSocketChannel channel) {

        BroadcastingMessageDTO broadcastingMessageDTO = new BroadcastingMessageDTO();
        broadcastingMessageDTO.setReceivedFriendId(1L);
        broadcastingMessageDTO.setTextMsgDTO(body);


        rocketmqtemplate.send("mallPush:im", MessageBuilder.withPayload(broadcastingMessageDTO).build());
    }
}
