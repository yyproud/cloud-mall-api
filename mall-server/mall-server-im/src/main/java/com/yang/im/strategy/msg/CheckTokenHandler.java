package com.yang.im.strategy.msg;

import com.yang.common.core.utils.Result;
import com.yang.im.dto.msg.HeartbeatMsgDTO;
import com.yang.im.enums.ChatMessageTypeEnum;
import com.yang.im.vo.msg.ChatMessageVO;
import com.yang.im.vo.msg.CheckTokenVO;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * 检查token
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2024/3/14 15:42
 */
@Slf4j
@Component
public class CheckTokenHandler extends AbstractMsgHandler<HeartbeatMsgDTO> {
    @Override
    ChatMessageTypeEnum getMsgTypeEnum() {
        return ChatMessageTypeEnum.CHECK_TOKEN;
    }

    @Override
    protected void saveMsg(HeartbeatMsgDTO heartbeatMsgDTO, NioSocketChannel channel) {
        ChatMessageVO chatMessageVO = new ChatMessageVO();
        chatMessageVO.setAction(ChatMessageTypeEnum.CHECK_TOKEN.getAction());

        CheckTokenVO checkTokenVO = new CheckTokenVO();
        checkTokenVO.setErr(null);
        chatMessageVO.setData(checkTokenVO);
        channel.writeAndFlush(new TextWebSocketFrame(Result.okJson(chatMessageVO)));
    }
}
