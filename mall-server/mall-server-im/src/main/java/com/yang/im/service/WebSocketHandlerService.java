package com.yang.im.service;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.net.url.UrlBuilder;
import cn.hutool.json.JSONUtil;
import com.yang.common.core.utils.StringUtils;
import com.yang.im.dto.msg.ChatMessageDTO;
import com.yang.im.strategy.msg.AbstractMsgHandler;
import com.yang.im.strategy.msg.MsgHandlerFactory;
import com.yang.im.vo.ChatLinkInfoVo;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.FullHttpRequest;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.websocketx.*;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.net.InetSocketAddress;
import java.util.Date;
import java.util.Optional;

/**
 * Netty业务实现类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/16 16:42
 */
@Slf4j
@Component
public class WebSocketHandlerService extends SimpleChannelInboundHandler<Object> {
    public static WebSocketHandlerService webSocketHandlerService;
    @Resource
    SocketMessageService socketMessageService;
    private WebSocketServerHandshaker handshake;

    /**
     * 解决启动加载不到自己业务类
     */
    @PostConstruct
    public void init() {
        webSocketHandlerService = this;
    }

    @Override
    protected void channelRead0(ChannelHandlerContext chx, Object msg) {
        if (msg instanceof HttpRequest) {
            // 处理请求连接
            handlerHttpRequest(chx, (HttpRequest) msg);
        } else if (msg instanceof WebSocketFrame) {
            // 处理报文
            handlerWebSocketFrame(chx, (WebSocketFrame) msg);
        }
    }

    /**
     * 用户心跳事件触发
     *
     * @param ctx 上下文
     * @param evt 事件
     * @throws Exception 异常
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception {
        NioSocketChannel channel = (NioSocketChannel) ctx.channel();
        log.error("【" + channel.remoteAddress() + "】未进行心跳，将进行踢出!");
        if (evt instanceof IdleStateEvent) {
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state() == IdleState.READER_IDLE) {
                webSocketHandlerService.socketMessageService.removeConnection(channel);
                channel.close();
                log.error("【" + channel.remoteAddress() + "】踢出完成!");
            }
        } else {
            super.userEventTriggered(ctx, evt);
        }
    }

    /**
     * 处理websocket消息
     *
     * @param ctx   上下文
     * @param frame 消息
     */
    private void handlerWebSocketFrame(ChannelHandlerContext ctx, WebSocketFrame frame) {
        NioSocketChannel channel = (NioSocketChannel) ctx.channel();
        // 判断是否是关闭链路的指令
        if (frame instanceof CloseWebSocketFrame) {
            log.info("【" + ctx.channel().remoteAddress() + "】已关闭（服务器端）");
            // 移除channel
            webSocketHandlerService.socketMessageService.removeConnection(channel);
            handshake.close(ctx.channel(), (CloseWebSocketFrame) frame.retain());
            return;
        }
        // 判断是否是ping消息
        if (frame instanceof PingWebSocketFrame) {
            log.info("【ping】");
            return;
        }
        // 判断实时是pong消息
        if (frame instanceof PongWebSocketFrame) {
            log.info("【pong】");
            return;
        }
        // 判断是否二进制
        if (!(frame instanceof TextWebSocketFrame)) {
            log.info("【不支持二进制】");
            throw new UnsupportedOperationException("不支持二进制");
        }
        // 解析传递的消息
        ChatMessageDTO chatMessageDto = JSONUtil.toBean(((TextWebSocketFrame) frame).text(), ChatMessageDTO.class);
        // 获取消息处理类
        AbstractMsgHandler<?> msgHandler = MsgHandlerFactory.getStrategyNoNull(chatMessageDto.getAction(),channel);
        if(StringUtils.isNotNull(msgHandler)){
            msgHandler.checkAndSaveMsg(chatMessageDto,channel);
        }
    }

    /**
     * 处理http连接请求
     *
     * @param ctx 上下文
     * @param req 请求
     */
    private void handlerHttpRequest(ChannelHandlerContext ctx, HttpRequest req) {
        //可以通过url获取其他参数
        WebSocketServerHandshakerFactory factory = new WebSocketServerHandshakerFactory(
                "wss://" + req.headers().get("Host") + "/" + req.uri(),
                null,
                false
        );
        handshake = factory.newHandshaker(req);
        if (handshake == null) {
            WebSocketServerHandshakerFactory.sendUnsupportedVersionResponse(ctx.channel());
        } else {
            //进行连接
            ChannelFuture channelFuture = handshake.handshake(ctx.channel(), (FullHttpRequest) req);
            if (channelFuture.isSuccess()) {
                FullHttpRequest request = (FullHttpRequest) req;
                UrlBuilder urlBuilder = UrlBuilder.ofHttp(request.uri());
                // 获取连接人Id
                String userId = Optional.ofNullable(urlBuilder.getQuery())
                        .map(k -> k.get("userId"))
                        .map(CharSequence::toString)
                        .orElse("");
                request.setUri(urlBuilder.getPath().toString());
                HttpHeaders headers = request.headers();
                String ip = headers.get("X-Real-IP");
                //如果没经过nginx，就直接获取远端地址
                if (StringUtils.isEmpty(ip)) {
                    InetSocketAddress address = (InetSocketAddress) ctx.channel().remoteAddress();
                    ip = address.getAddress().getHostAddress();
                }
                NioSocketChannel channel = (NioSocketChannel) ctx.channel();
                ChatLinkInfoVo chatLinkInfoVo = new ChatLinkInfoVo();
                chatLinkInfoVo.setUserId(userId);
                chatLinkInfoVo.setIp(ip);
                chatLinkInfoVo.setLandingTime(DateUtil.formatDateTime(new Date()));
                // 保存在线信息
                boolean putConnection = webSocketHandlerService.socketMessageService.putConnection(chatLinkInfoVo, channel);
//                if (putConnection) {
//                    log.error("【" + ctx.channel().remoteAddress() + "】已连接，请勿重复连接!");
//                    ctx.channel().close();
//                }
            }
        }
    }
}
