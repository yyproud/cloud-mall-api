package com.yang.im.mapper;

import com.yang.common.mybatis.core.mapper.BaseMapperPlus;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import com.yang.im.entity.ListTimeEntity;
import com.yang.im.vo.ListTimeVo;
import com.yang.im.dto.ListTimeDto;

import java.util.List;

/**
 * 消息最后阅读时间Mapper接口
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 23:03
 */
public interface ListTimeMapper extends BaseMapperPlus<ListTimeMapper, ListTimeEntity, ListTimeVo> {

    /**
     * 查询消息最后阅读时间分页列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<ListTimeVo> queryPageList(Page<ListTimeVo> page, @Param("dto") ListTimeDto dto);

    /**
     * 查询消息最后阅读时间列表
     *
     * @param dto  查询条件
     * @return 数据
     */
    List<ListTimeVo> queryList(@Param("dto") ListTimeDto dto);
}
