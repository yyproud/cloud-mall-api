package com.yang.im.service;

import com.alibaba.cloud.nacos.NacosDiscoveryProperties;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.naming.NamingFactory;
import com.alibaba.nacos.api.naming.NamingService;
import com.yang.im.config.properties.NettyWebSocketProperties;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.Future;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.net.InetAddress;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Netty启动类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/16 16:39
 */
@Slf4j
@Component
public class NettyWebSocketServer {

    @Resource
    NettyWebSocketProperties nwsProperties;
    @Resource
    private NacosDiscoveryProperties nacosDiscoveryProperties;

    /**
     * bossGroup连接线程组，主要负责接受客户端连接，一般一个线程足矣
     */
    EventLoopGroup boosGroup = new NioEventLoopGroup();
    /**
     * workerGroup工作线程组，主要负责网络IO读写
     */
    EventLoopGroup workGroup = new NioEventLoopGroup();


    /**
     * 启动时钩子
     */
    @PostConstruct
    public void start() {
        run();
    }

    /**
     * 关闭时钩子
     */
    @PreDestroy
    public void destroy() {
        Future<?> boosFuture = boosGroup.shutdownGracefully();
        Future<?> workFuture = workGroup.shutdownGracefully();
        boosFuture.syncUninterruptibly();
        workFuture.syncUninterruptibly();
        log.error("=================Netty服务关闭==================");
    }


    @Async
    public void run() {
        log.error("=================Netty 端口启动:{}==================", nwsProperties.getNettyPort());
        try {
            //绑定端口
            ServerBootstrap bootstrap = new ServerBootstrap();
            // 设置两个线程组
            bootstrap.group(boosGroup, workGroup)
                    // 非阻塞异步服务端TCP Socket 连接
                    .channel(NioServerSocketChannel.class)
                    // 临时存放已完成三次握手的请求的队列的最大长度
                    .option(ChannelOption.SO_BACKLOG, nwsProperties.getBacklogValue())
                    .option(ChannelOption.SO_KEEPALIVE, true)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    //初始化handler
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) {
                            log.info("=================收到新的链接:{}==================", socketChannel.localAddress());
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            // 进行设置心跳检测
                            pipeline.addLast(new IdleStateHandler(
                                    nwsProperties.getReaderIdleTime(),
                                    nwsProperties.getWriterIdleTime(),
                                    nwsProperties.getReaderIdleTime() * nwsProperties.getWriterIdleTime(),
                                    TimeUnit.SECONDS)
                            );
                            // 将请求和映带消息节码为HTTP消息
                            pipeline.addLast(new HttpServerCodec());
                            // 向客户端发送HTML5文件
                            pipeline.addLast(new ChunkedWriteHandler());
                            // 将HTTP消息的多个部分合成一条完整的HTTP消息
                            pipeline.addLast(new HttpObjectAggregator(nwsProperties.getMaxContentLength()));
                            // 配置通道处理 来进行业务处理
                            pipeline.addLast(new WebSocketHandlerService());
                        }
                    });
            // 将Netty服务注册进Nacos
            registerNamingService(nwsProperties.getNettyName(), nwsProperties.getNettyPort());
            // 绑定Netty的启动端口
            bootstrap.bind(nwsProperties.getNettyPort()).sync();
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }
    }

    /**
     * 将Netty服务注册进Nacos
     *
     * @param nettyName 服务名称
     * @param nettyPort 服务端口号
     */
    private void registerNamingService(String nettyName, Integer nettyPort) {
        try {
            Properties properties = new Properties();
            properties.setProperty(PropertyKeyConst.SERVER_ADDR, nacosDiscoveryProperties.getServerAddr());
            properties.setProperty(PropertyKeyConst.NAMESPACE, nacosDiscoveryProperties.getNamespace());
            NamingService namingService = NamingFactory.createNamingService(properties);
            InetAddress address = InetAddress.getLocalHost();
            namingService.registerInstance(nettyName, nacosDiscoveryProperties.getGroup(), address.getHostAddress(), nettyPort);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}



