package com.yang.im.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import com.yang.im.entity.ListTimeEntity;
import com.yang.im.vo.ListTimeVo;
import com.yang.im.dto.ListTimeDto;
import com.yang.im.service.IListTimeService;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.core.utils.Result;
import com.yang.common.mybatis.core.validate.AddGroup;
import com.yang.common.mybatis.core.validate.EditGroup;
import javax.annotation.Resource;
import java.util.List;
import java.util.Arrays;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 消息最后阅读时间控制器
 * 前端访问路由地址为:/im/listTime
 *
 * @author 杨旭
 * @email  
 * @create 2024/10/27 23:03
 */
@Validated
@RestController
@RequestMapping("/listTime")
public class ListTimeController {

    @Resource
    private IListTimeService iListTimeService;

    /**
     * 查询消息最后阅读时间列表
     */
    @SaCheckPermission("im:listTime:list")
    @GetMapping("/list")
    public Result list(ListTimeDto dto) {
        PageView<ListTimeVo> list = iListTimeService.queryPageList(dto);
        return Result.ok(list);
    }


    /**
     * 获取消息最后阅读时间详细信息
     *
     * @param id 主键
     */
    @SaCheckPermission("im:listTime:query")
    @GetMapping("/{id}")
    public Result getInfo(@NotNull(message = "主键不能为空") @PathVariable Long id) {
        return Result.ok(iListTimeService.queryById(id));
    }

    /**
     * 新增消息最后阅读时间
     *
     * @param dto 实体参数
     * @return 新增结果
     */
    @SaCheckPermission("im:listTime:add")
    @PostMapping()
    public Result add(@Validated(AddGroup.class) @RequestBody ListTimeDto dto) {
        return iListTimeService.insert(dto) ? Result.ok() :Result.error();
    }

    /**
     * 修改消息最后阅读时间
     *
     * @param dto 实体参数
     * @return 修改结果
     */
    @SaCheckPermission("im:listTime:edit")
    @PutMapping()
    public Result edit(@Validated(EditGroup.class) @RequestBody ListTimeDto dto) {
        return iListTimeService.update(dto) ? Result.ok() :Result.error();
    }

    /**
     * 删除消息最后阅读时间
     *
     * @param ids 主键串
     * @return 删除结果
     */
    @SaCheckPermission("im:listTime:remove")
    @DeleteMapping("/{ids}")
    public Result remove(@NotEmpty(message = "主键不能为空") @PathVariable Long[] ids) {
        return iListTimeService.deleteIds(Arrays.asList(ids)) ? Result.ok() :Result.error();
    }
}
