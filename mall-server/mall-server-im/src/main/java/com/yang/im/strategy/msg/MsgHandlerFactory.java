package com.yang.im.strategy.msg;

import cn.hutool.core.util.ObjectUtil;
import com.yang.common.core.utils.Result;
import com.yang.im.enums.ChatMessageTypeEnum;
import com.yang.im.vo.msg.ChatMessageVO;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;

import java.util.HashMap;
import java.util.Map;

/**
 * 消息工厂类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2024/3/14 16:23
 */
public class MsgHandlerFactory {
    private static final Map<String, AbstractMsgHandler<?>> STRATEGY_MAP = new HashMap<>();

    public static void register(String code, AbstractMsgHandler<?> strategy) {
        STRATEGY_MAP.put(code, strategy);
    }

    public static AbstractMsgHandler<?> getStrategyNoNull(String code, NioSocketChannel channel) {
        AbstractMsgHandler<?> strategy = STRATEGY_MAP.get(code);
        if (ObjectUtil.isEmpty(strategy)) {
            ChatMessageVO chatMessageVO = new ChatMessageVO();
            chatMessageVO.setAction(ChatMessageTypeEnum.ACTION_ERROR.getAction());
            channel.writeAndFlush(new TextWebSocketFrame(Result.okJson(chatMessageVO)));
            return null;
        }
        return strategy;
    }
}
