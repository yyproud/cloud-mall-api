package com.yang.im.strategy.msg;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.date.DateUtil;
import com.yang.common.core.utils.Result;
import com.yang.common.core.utils.StringUtils;
import com.yang.im.dto.msg.HeartbeatMsgDTO;
import com.yang.im.enums.ChatMessageTypeEnum;
import com.yang.im.service.SocketMessageService;
import com.yang.im.vo.ChatLinkInfoVo;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2024/3/14 15:42
 */
@Slf4j
@Component
public class HeartbeatHandler extends AbstractMsgHandler<HeartbeatMsgDTO>{

    @Override
    ChatMessageTypeEnum getMsgTypeEnum() {
        return ChatMessageTypeEnum.HEARTBEAT;
    }

    @Override
    protected void saveMsg(HeartbeatMsgDTO heartbeatMsgDTO, NioSocketChannel channel) {
        channel.writeAndFlush(new TextWebSocketFrame(Result.okJson(heartbeatMsgDTO)));
    }
}
