package com.yang.im.listener;

import com.yang.common.redis.utils.RedisUtils;
import com.yang.im.event.UserOnlineEvent;
import com.yang.im.service.IpService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 用户上线监听器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/29 17:12
 */
@Slf4j
@Component
public class UserOnlineListener {

    @Resource
    private IpService ipService;
    @Async
    @EventListener(classes = UserOnlineEvent.class)
    public void push(UserOnlineEvent onlineUser) {
        log.info("用户上线推送消息：{}", onlineUser.toString());
    }

    @Async
    @EventListener(classes = UserOnlineEvent.class)
    public void save(UserOnlineEvent onlineUser) {
        // 将连接信息存储到redis
        RedisUtils.setCacheMapValue(
                "chatLinkInfoList",
                onlineUser.getChatLinkInfoVo().getUserId(),
                onlineUser.getChatLinkInfoVo()
        );
        // 查询ip归属地
        ipService.updateIpHomeLocation(onlineUser.getChatLinkInfoVo().getUserId());
    }
}
