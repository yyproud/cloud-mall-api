package com.yang.im.service.impl;

import cn.hutool.core.lang.TypeReference;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.yang.common.redis.utils.RedisUtils;
import com.yang.im.dto.IpDetail;
import com.yang.im.dto.IpResult;
import com.yang.im.service.IpService;
import com.yang.im.vo.ChatLinkInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * IPService接口实现类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/30 13:43
 */
@Slf4j
@Service
public class IpServiceImpl implements IpService {

    @Async("ipServiceExecutor")
    @Override
    public void updateIpHomeLocation(String userId) {
        ChatLinkInfoVo linkInfo = RedisUtils.getCacheMapValue("chatLinkInfoList", userId);
        if (ObjectUtil.isNull(linkInfo)) {
            return;
        }
        if (ObjectUtil.isEmpty(linkInfo.getIp())) {
            return;
        }
        IpDetail ipDetailOrNull = getIpDetailOrNull(linkInfo.getIp());
        if (ObjectUtil.isNotNull(ipDetailOrNull)) {
            linkInfo.setIpHomeLocation(ipDetailOrNull.getDistrict());
            // 将归属地保存进缓存
            RedisUtils.setCacheMapValue("chatLinkInfoList", userId, linkInfo);
        }
    }

    /**
     * 根据IP获取归属地
     *
     * @param ip IP地址
     * @return 归属地
     */
    public static IpDetail getIpDetailOrNull(String ip) {
        // 最大重试次数
        int maxRetries = 3;
        // 初始重试次数
        int retryCount = 0;
        // 初始延迟为1秒
        int delay = 2000;
        while (retryCount < maxRetries) {
            try {
                // 尝试获取
                String body = HttpUtil.get("https://qifu-api.baidubce.com/ip/geo/v1/district?ip=" + ip);
                IpResult<IpDetail> result = JSONUtil.toBean(body, new TypeReference<IpResult<IpDetail>>() {
                }, false);
                if (result.isSuccess()) {
                    return result.getData();
                } else {
                    // 重试
                    retryCount++;
                    Thread.sleep(delay);
                    delay *= 2;
                }
            } catch (InterruptedException e) {
                e.printStackTrace(System.out);
            }
        }
        return null;
    }
}
