package com.yang.im.service.impl;

import cn.dev33.satoken.stp.StpUtil;
import com.alibaba.fastjson.JSONObject;
import com.yang.common.core.utils.Result;
import com.yang.common.core.utils.StringUtils;
import com.yang.im.event.UserOfflineEvent;
import com.yang.im.event.UserOnlineEvent;
import com.yang.im.service.SocketMessageService;
import com.yang.im.vo.ChatLinkInfoVo;
import com.yang.im.vo.msg.ChatMessageVO;
import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.AttributeKey;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/27 15:02
 */
@Slf4j
@Service
public class SocketMessageServiceImpl implements SocketMessageService {

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    /**
     * 绑定用户Id
     */
    private final AttributeKey<String> userKey = AttributeKey.valueOf("user");
    /**
     * 所有连接
     */
    private static final ChannelGroup CHANNEL_GROUP = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    @Override
    public boolean putConnection(ChatLinkInfoVo chatLinkInfoVo, NioSocketChannel channel) {
        String loginId = (String) StpUtil.getLoginIdByToken(chatLinkInfoVo.getUserId());
        if (StringUtils.isNotNull(loginId) && null == getChannel(loginId)) {
            log.info("loginIdByToken：{}", loginId);
            // 全局保存连接
            channel.attr(userKey).set(loginId);
            CHANNEL_GROUP.add(channel);
            // 上线事件
            chatLinkInfoVo.setUserId(loginId);
            applicationEventPublisher.publishEvent(new UserOnlineEvent(this, chatLinkInfoVo));
            log.info("用户上线：{}", loginId);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }

    @Override
    public void removeConnection(NioSocketChannel channel) {
        String userId = getBindUserId(channel);
        if (null != userId) {
            CHANNEL_GROUP.remove(channel);
            ChatLinkInfoVo chatLinkInfoVo = new ChatLinkInfoVo();
            chatLinkInfoVo.setUserId(userId);
            applicationEventPublisher.publishEvent(new UserOfflineEvent(this, chatLinkInfoVo));
        }
    }

    @Override
    public void sendMessage(String toUserId, ChatMessageVO chatMessageVO) {
        NioSocketChannel channel = getChannel(toUserId);
        if (channel != null) {
            channel.writeAndFlush(new TextWebSocketFrame(Result.okJson(chatMessageVO)));
        }
    }

    /**
     * 根据连接信息获取用户Id
     *
     * @param channel 连接信息
     * @return 用户Id
     */
    private String getBindUserId(Channel channel) {
        if (channel.hasAttr(userKey)) {
            return channel.attr(userKey).get();
        }
        return null;
    }

    /**
     * 根据UserId取出连接信息
     *
     * @param userId userId
     * @return 连接信息
     */
    @Override
    public NioSocketChannel getChannel(String userId) {
        for (Channel channel : CHANNEL_GROUP) {
            if (userId.equalsIgnoreCase(channel.attr(userKey).get())) {
                return (NioSocketChannel) channel;
            }
        }
        return null;
    }
}
