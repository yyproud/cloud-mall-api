package com.yang.im.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

/**
 * Netty启动类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/27 19:21
 */
@Data
@Component
@RefreshScope
@ConfigurationProperties("netty")
public class NettyWebSocketProperties {
    /**
     * Netty端口
     */
    private Integer nettyPort;
    /**
     * Netty应用名称
     */
    private String nettyName;
    /**
     * 临时队列长度
     */
    private Integer backlogValue;
    /**
     * 读空闲时间
     */
    private long readerIdleTime;
    /**
     * 写空闲时间
     */
    private long writerIdleTime;
    /**
     * 最大内容长度
     */
    private Integer maxContentLength;
}
