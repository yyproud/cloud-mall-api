package com.yang.im.listener;

import com.alibaba.fastjson2.JSON;
import com.yang.im.dto.msg.BroadcastingMessageDTO;
import com.yang.im.service.SocketMessageService;
import com.yang.im.vo.msg.ChatMessageVO;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 即时通讯消息监听器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2024/03/16 15:38
 */
@Component
@RocketMQMessageListener(topic = "mallPush", consumerGroup = "im", messageModel = MessageModel.BROADCASTING)
public class MsgListener implements RocketMQListener<MessageExt> {
    @Resource
    SocketMessageService socketMessageService;
    @Override
    public void onMessage(MessageExt messageExt) {
        BroadcastingMessageDTO broadcastingMessageDTO = JSON.parseObject(new String(messageExt.getBody()), BroadcastingMessageDTO.class);
        ChatMessageVO chatMessageVo = new ChatMessageVO();
        chatMessageVo.setData(broadcastingMessageDTO.getTextMsgDTO());
        socketMessageService.sendMessage(broadcastingMessageDTO.getReceivedFriendId().toString(),chatMessageVo);

        System.err.println(broadcastingMessageDTO);
    }
}
