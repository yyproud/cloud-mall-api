package com.yang.im.service;

import com.yang.im.vo.ChatLinkInfoVo;
import com.yang.im.vo.msg.ChatMessageVO;
import io.netty.channel.socket.nio.NioSocketChannel;

/**
 * 用户连接信息Service接口
 *
 * @author 杨旭
 * @email
 * @create 2023/11/19 19:07
 */
public interface SocketMessageService {

    /**
     * 保存在线信息
     *
     * @param chatLinkInfoVo 用户连接信息
     * @param channel        连接信息
     * @return 存储成功或失败
     */
    boolean putConnection(ChatLinkInfoVo chatLinkInfoVo, NioSocketChannel channel);

    /**
     * 移除连接信息
     *
     * @param channel 连接信息
     */
    void removeConnection(NioSocketChannel channel);

    /**
     * 发送消息
     *
     * @param toUserId      接收用户Id
     * @param chatMessageVo 消息内容
     */
    void sendMessage(String toUserId, ChatMessageVO chatMessageVo);

    /**
     * 获取连接信息
     *
     * @param userId 用户Id
     * @return 连接信息
     */
    NioSocketChannel getChannel(String userId);


}
