package com.yang.coss;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 云对象存储服务启动类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/11/16 11:26
 */
@EnableAsync
@EnableDubbo
@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
public class MallServerCossApplication {

    public static void main(String[] args) {
        SpringApplication.run(MallServerCossApplication.class, args);
    }
}
