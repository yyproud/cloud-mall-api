package com.yang.coss.service.impl;

import cn.hutool.core.util.StrUtil;
import com.yang.common.core.utils.DateUtils;
import com.yang.common.core.utils.Result;
import com.yang.coss.config.MinioConfiguration;
import com.yang.coss.service.MinIoService;
import io.minio.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.InputStream;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * MinioService实现类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/12/03 19:02
 */
@Slf4j
@Service
public class MinIoServiceImpl implements MinIoService {

    @Resource
    private MinioClient minioClient;

    @Resource
    private MinioConfiguration configuration;

    @Override
    @SneakyThrows
    public Result uploadFile(List<MultipartFile> file) {
        checkMakeBucket();
        List<Map<String, Object>> mapList = new ArrayList<>();
        for (MultipartFile multipartFile : file) {
            Map<String, Object> objectMap = new HashMap<>(16);

            InputStream is = multipartFile.getInputStream();
            // 获取文件名
            String originalFileName = multipartFile.getOriginalFilename();
            // 获取后缀（不带点）
            String extension = "";
            if (originalFileName != null && originalFileName.lastIndexOf(".") != -1) {
                extension = originalFileName.substring(originalFileName.lastIndexOf(".") + 1);
                extension = extension.toLowerCase();
            }
            // 文件路径规则 年+月+日+文件后缀+文件名
            String path = DateUtils.getDate(DateUtils.FILE_YYYY_MM_DD)
                    + StrUtil.SLASH
                    + extension
                    + StrUtil.SLASH
                    + originalFileName;
            PutObjectArgs args = PutObjectArgs.builder()
                    .bucket(configuration.getBucketName())
                    .object(path)
                    .stream(is, multipartFile.getSize(), -1)
                    .contentType(multipartFile.getContentType())
                    .build();
            minioClient.putObject(args);
            is.close();

            objectMap.put("size", multipartFile.getSize());
            objectMap.put("type", multipartFile.getContentType());
            objectMap.put("oldName", originalFileName);
            objectMap.put("url", configuration.getUrl()
                    + StrUtil.SLASH
                    + configuration.getBucketName()
                    + StrUtil.SLASH
                    + path
            );
            mapList.add(objectMap);
        }
        return Result.ok(mapList);
    }

    @Override
    @SneakyThrows
    public Result getPresignedPostFormData(String fileName) {
        checkMakeBucket();
        // 为存储桶创建一个上传策略，过期时间为7天
        PostPolicy policy = new PostPolicy(configuration.getBucketName(), ZonedDateTime.now().plusDays(7));
        // 设置一个参数key，值为上传对象的名称
        policy.addEqualsCondition("key", fileName);
        Map<String, String> map = minioClient.getPresignedPostFormData(policy);
        HashMap<String, String> hashMap = new HashMap<>(16);
        map.forEach((k, v) -> {
            hashMap.put(k.replaceAll(StrUtil.DASHED, ""), v);
        });
        hashMap.put("host", configuration.getUrl() + StrUtil.SLASH + configuration.getBucketName());
        hashMap.put("downloadUrl", configuration.getUrl() + StrUtil.SLASH + configuration.getBucketName() + StrUtil.SLASH + fileName);
        return Result.ok(hashMap);
    }

    /**
     * 检查桶是否存在
     */
    @SneakyThrows
    private void checkMakeBucket() {
        // 检查桶是否存在，不存在则创建
        boolean bucketExists = minioClient.bucketExists(
                BucketExistsArgs.builder()
                        .bucket(configuration.getBucketName())
                        .build()
        );
        if (!bucketExists) {
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(configuration.getBucketName()).build());
        }
    }
}
