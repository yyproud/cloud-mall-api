package com.yang.coss.controller;

import com.yang.common.core.utils.Result;
import com.yang.coss.service.MinIoService;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "MinIoController")
@RestController
@RequestMapping("/test")
public class MinIoController {
    @Resource
    MinIoService minIoService;

    /**
     * 前端直接上传文件
     *
     * @param files 文件集合
     * @return 上传结果
     */
    @PostMapping("/upload")
    public Result uploadToFile(@RequestParam(name = "files", required = false) List<MultipartFile> files) {
        return minIoService.uploadFile(files);
    }
    @GetMapping("/getPreSignedObjectUrl/{fileName}")
    public Result getPreSignedObjectUrl(@PathVariable String fileName) {
        return minIoService.getPresignedPostFormData(fileName);
    }

}
