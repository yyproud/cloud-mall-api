package com.yang.coss.service;

import com.yang.common.core.utils.Result;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * MinioService接口
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/12/03 19:02
 */
public interface MinIoService {

    /**
     * 上传文件集合
     *
     * @param files 文件集合
     * @return 上传结果
     */
    Result uploadFile(List<MultipartFile> files);

    /**
     * 获取临时上传地址
     * @param fileName 文件名称
     * @return 前端直接上传地址
     */
    Result getPresignedPostFormData(String fileName);
}
