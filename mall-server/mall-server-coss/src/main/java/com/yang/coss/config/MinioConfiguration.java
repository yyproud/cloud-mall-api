package com.yang.coss.config;

import io.minio.MinioClient;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Minio配置类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/12/03 18:43
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "minio")
public class MinioConfiguration {
    /**
     * 用户名
     */
    private String accessKey;
    /**
     * 密码
     */
    private String secretKey;
    /**
     * Minio 服务端 Api地址
     */
    private String url;
    /**
     * 存储桶名字
     */
    private String bucketName;

    /**
     * 构建 操作Minio的客户端
     *
     * @return Minio客户端
     */
    @Bean
    public MinioClient minioClient() {
        return MinioClient.builder()
                .endpoint(url)
                .credentials(accessKey, secretKey)
                .build();
    }
}
