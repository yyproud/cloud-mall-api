package com.yang.generator.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.lang.Dict;
import cn.hutool.core.util.ObjectUtil;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.yang.common.core.constant.GeneratorConstants;
import com.yang.common.core.exception.ServiceException;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.satoken.utils.LoginHelper;
import com.yang.common.core.utils.JsonUtils;
import com.yang.common.core.utils.StreamUtils;
import com.yang.common.core.utils.StringUtils;
import com.yang.common.core.utils.file.FileUtils;
import com.yang.generator.dto.GeneratorImportTableDto;
import com.yang.generator.dto.GeneratorTableDto;
import com.yang.generator.entity.GeneratorTableColumnEntity;
import com.yang.generator.entity.GeneratorTableEntity;
import com.yang.generator.mapper.GeneratorTableMapper;
import com.yang.generator.service.GeneratorTableColumnService;
import com.yang.generator.service.GeneratorTableService;
import com.yang.generator.utils.GeneratorUtils;
import com.yang.generator.utils.VelocityInitializer;
import com.yang.generator.utils.VelocityUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.data.keyvalue.core.IdentifierGenerator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static com.yang.generator.utils.GeneratorUtils.buildTableColumn;

/**
 * 代码生成
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 17:05
 */
@Service
public class GeneratorTableServiceImpl extends ServiceImpl<GeneratorTableMapper, GeneratorTableEntity> implements GeneratorTableService {

    @Resource
    GeneratorTableColumnService tableColumnService;
    @Resource
    IdentifierGenerator identifierGenerator;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public PageView<GeneratorTableEntity> getList(GeneratorTableDto generatorTableDto) {
        return null;
    }

    @Override
    public PageView<GeneratorTableEntity> getTableList(GeneratorTableDto generatorTableDto) {
        return null;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void importTables(GeneratorImportTableDto importTableDto) {
        // 将需要导出的表名解析
        String[] tableList = Convert.toStrArray(importTableDto.getTables());
        // 查询出详细信息
        List<GeneratorTableEntity> tableListByTableNames = mapper.getTableListByTableNames(tableList);
        // 取出当前登陆人
        Long userId = LoginHelper.getUserId();

        for (GeneratorTableEntity generatorTableEntity : tableListByTableNames) {
            String tableName = generatorTableEntity.getTableName();
            GeneratorUtils.buildTable(importTableDto, generatorTableEntity, userId);
            int insert = mapper.insert(generatorTableEntity);
            if (insert > 0) {
                // 保存列信息
                List<GeneratorTableColumnEntity> listByTableName = tableColumnService.getListByTableName(tableName);
                // 结构处理
                for (GeneratorTableColumnEntity generatorTableColumnEntity : listByTableName) {
                    buildTableColumn(generatorTableColumnEntity, generatorTableEntity.getTableId());
                }
                tableColumnService.saveBatch(listByTableName);
            }
        }
    }

    @Override
    public Map<String, String> previewCode(Long tableId) {
        Map<String, String> dataMap = new LinkedHashMap<>();
        GeneratorTableEntity generatorTable = mapper.getTableInfo(new HashMap<String, Object>(1) {{
            put("tableId", tableId);
        }});
        List<Long> menuIds = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            // TODO
            menuIds.add(1L);
        }
        generatorTable.setMenuIds(menuIds);
        // 设置主子表信息
        setSubTable(generatorTable);
        // 设置主键列信息
        setPkColumn(generatorTable);
        VelocityInitializer.initVelocity();

        VelocityContext context = VelocityUtils.prepareContext(generatorTable);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(generatorTable.getTplCategory());
        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);
            dataMap.put(template, sw.toString());
        }

        return dataMap;
    }

    @Override
    public GeneratorTableEntity selectGeneratorTableById(Long tableId) {
        GeneratorTableEntity generatorTable = this.baseMapper.getTableInfo(new HashMap<String, Object>(1) {{
            put("tableId", tableId);
        }});
        return setTableFromOptions(generatorTable);
    }

    @Override
    public List<GeneratorTableEntity> selectGeneratorTableAll() {
        return this.baseMapper.selectGeneratorTableAll();
    }

    @Override
    public void validateEdit(GeneratorTableEntity generatorTable) {
        if (GeneratorConstants.TPL_TREE.equals(generatorTable.getTplCategory())) {
            String options = JsonUtils.toJsonString(generatorTable.getParams());
            Dict paramsObj = JsonUtils.parseMap(options);
            if (paramsObj != null && StringUtils.isEmpty(paramsObj.getStr(GeneratorConstants.TREE_CODE))) {
                throw new ServiceException("树编码字段不能为空");
            } else if (paramsObj != null && StringUtils.isEmpty(paramsObj.getStr(GeneratorConstants.TREE_PARENT_CODE))) {
                throw new ServiceException("树父编码字段不能为空");
            } else if (paramsObj != null && StringUtils.isEmpty(paramsObj.getStr(GeneratorConstants.TREE_NAME))) {
                throw new ServiceException("树名称字段不能为空");
            } else if (GeneratorConstants.TPL_SUB.equals(generatorTable.getTplCategory())) {
                if (StringUtils.isEmpty(generatorTable.getSubTableName())) {
                    throw new ServiceException("关联子表的表名不能为空");
                } else if (StringUtils.isEmpty(generatorTable.getSubTableFkName())) {
                    throw new ServiceException("子表关联的外键名不能为空");
                }
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateGeneratorTable(GeneratorTableEntity generatorTable) {
        String options = JsonUtils.toJsonString(generatorTable.getParams());
        generatorTable.setOptions(options);
        int row = this.baseMapper.updateById(generatorTable);
        if (row > 0) {
            for (GeneratorTableColumnEntity cenTableColumn : generatorTable.getGeneratorTableColumns()) {
                this.tableColumnService.updateById(cenTableColumn);
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteGeneratorTableByIds(Long[] tableIds) {
        List<Long> ids = Arrays.asList(tableIds);
        this.baseMapper.deleteBatchIds(ids);
        tableColumnService.remove(new LambdaQueryWrapper<GeneratorTableColumnEntity>().in(GeneratorTableColumnEntity::getTableId, ids));
    }

    @Override
    public void synchroDb(String tableName) {
        GeneratorTableEntity generatorTable = this.baseMapper.getTableInfo(new HashMap<String, Object>(1) {{
            put("tableName", tableName);
        }});
        List<GeneratorTableColumnEntity> generatorTableColumnList = generatorTable.getGeneratorTableColumns();
        Map<String, GeneratorTableColumnEntity> tableColumnMap = StreamUtils.toIdentityMap(generatorTableColumnList, GeneratorTableColumnEntity::getColumnName);

        List<GeneratorTableColumnEntity> listByTableName = tableColumnService.getListByTableName(tableName);
        if (CollUtil.isEmpty(listByTableName)) {
            throw new ServiceException("同步数据失败，原表结构不存在");
        }
        List<String> dbTableColumnNames = StreamUtils.toList(listByTableName, GeneratorTableColumnEntity::getColumnName);

        List<GeneratorTableColumnEntity> saveColumns = new ArrayList<>();
        listByTableName.forEach(column -> {
            GeneratorUtils.buildTableColumn(column, generatorTable.getTableId());
            if (tableColumnMap.containsKey(column.getColumnName())) {
                GeneratorTableColumnEntity prevColumn = tableColumnMap.get(column.getColumnName());
                column.setColumnId(prevColumn.getColumnId());
                if (column.isList()) {
                    // 如果是列表，继续保留查询方式/字典类型选项
                    column.setDictType(prevColumn.getDictType());
                    column.setQueryType(prevColumn.getQueryType());
                }
                if (StringUtils.isNotEmpty(prevColumn.getIsRequired()) && !column.isPk()
                        && (column.isInsert() || column.isEdit())
                        && ((column.isUsableColumn()) || (column.isSuperColumn()))) {
                    // 如果是(新增/修改&非主键/非忽略及父属性)，继续保留必填/显示类型选项
                    column.setIsRequired(prevColumn.getIsRequired());
                    column.setHtmlType(prevColumn.getHtmlType());
                }
            }
            saveColumns.add(column);
        });
        if (CollUtil.isNotEmpty(saveColumns)) {

            tableColumnService.insertOrUpdateBatch(saveColumns);
        }

        List<GeneratorTableColumnEntity> delColumns = StreamUtils.filter(generatorTableColumnList, column -> !dbTableColumnNames.contains(column.getColumnName()));
        if (CollUtil.isNotEmpty(delColumns)) {
            List<Long> ids = StreamUtils.toList(delColumns, GeneratorTableColumnEntity::getColumnId);
            tableColumnService.removeBatchByIds(ids);
        }
    }

    @Override
    public byte[] downloadCode(String[] tableNames) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(outputStream);
        for (String tableName : tableNames) {
            generatorCode(tableName, zip);
        }
        IoUtil.close(zip);
        return outputStream.toByteArray();
    }

    @Override
    public void generatorCode(String tableName) {
        GeneratorTableEntity generatorTable = generatorCodeEntity(tableName);

        VelocityInitializer.initVelocity();

        VelocityContext context = VelocityUtils.prepareContext(generatorTable);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(generatorTable.getTplCategory());
        for (String template : templates) {

            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);
            try {
                String path = getGenPath(generatorTable, template);
                FileUtils.writeUtf8String(sw.toString(), path);
            } catch (Exception e) {
                throw new ServiceException("渲染模板失败，表名：" + generatorTable.getTableName());
            }
        }
    }

    /**
     * 获取代码生成地址
     *
     * @param table    业务表信息
     * @param template 模板文件路径
     * @return 生成地址
     */
    public static String getGenPath(GeneratorTableEntity table, String template) {
        String genPath = table.getGeneratorPath();
        if (StringUtils.equals(genPath, "/")) {
            return System.getProperty("user.dir") + File.separator + "src" + File.separator + VelocityUtils.getFileName(template, table);
        }
        return genPath + File.separator + VelocityUtils.getFileName(template, table);
    }

    /**
     * 查询表信息并生成代码
     */
    private void generatorCode(String tableName, ZipOutputStream zip) {

        GeneratorTableEntity generatorTable = generatorCodeEntity(tableName);

        VelocityInitializer.initVelocity();

        VelocityContext context = VelocityUtils.prepareContext(generatorTable);

        // 获取模板列表
        List<String> templates = VelocityUtils.getTemplateList(generatorTable.getTplCategory());
        for (String template : templates) {
            // 渲染模板
            StringWriter sw = new StringWriter();
            Template tpl = Velocity.getTemplate(template, "UTF-8");
            tpl.merge(context, sw);
            try {
                // 添加到zip
                zip.putNextEntry(new ZipEntry(VelocityUtils.getFileName(template, generatorTable)));
                IoUtil.write(zip, StandardCharsets.UTF_8, false, sw.toString());
                IoUtil.close(sw);
                zip.flush();
                zip.closeEntry();
            } catch (IOException e) {
                log.error("渲染模板失败，表名：" + generatorTable.getTableName(), e);
            }
        }
    }

    /**
     * 生成代码实体
     *
     * @param tableName 表名
     * @return 实体
     */
    public GeneratorTableEntity generatorCodeEntity(String tableName) {
        // 查询表信息
        GeneratorTableEntity generatorTable = this.baseMapper.getTableInfo(new HashMap<String, Object>(1) {{
            put("tableName", tableName);
        }});
        Long menuMaxId = this.baseMapper.getMenuMaxId();
        List<Long> menuIds = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            menuIds.add(menuMaxId++);
        }
        generatorTable.setMenuIds(menuIds);
        // 设置主子表信息
        setSubTable(generatorTable);
        // 设置主键列信息
        setPkColumn(generatorTable);

        return generatorTable;
    }

    /**
     * 设置代码生成其他选项值
     *
     * @param genTable 设置后的生成对象
     */
    public GeneratorTableEntity setTableFromOptions(GeneratorTableEntity genTable) {
        Dict paramsObj = JsonUtils.parseMap(genTable.getOptions());
        if (ObjectUtil.isNotNull(paramsObj)) {
            String treeCode = paramsObj.getStr(GeneratorConstants.TREE_CODE);
            String treeParentCode = paramsObj.getStr(GeneratorConstants.TREE_PARENT_CODE);
            String treeName = paramsObj.getStr(GeneratorConstants.TREE_NAME);
            String parentMenuId = paramsObj.getStr(GeneratorConstants.PARENT_MENU_ID);
            String parentMenuName = paramsObj.getStr(GeneratorConstants.PARENT_MENU_NAME);

            genTable.setTreeCode(treeCode);
            genTable.setTreeParentCode(treeParentCode);
            genTable.setTreeName(treeName);
            genTable.setParentMenuId(parentMenuId);
            genTable.setParentMenuName(parentMenuName);
        }
        return genTable;
    }

    /**
     * 设置主子表信息
     *
     * @param table 业务表信息
     */
    public void setSubTable(GeneratorTableEntity table) {
        String subTableName = table.getSubTableName();
        if (StringUtils.isNotEmpty(subTableName)) {
            GeneratorTableEntity generatorTableEntity = this.baseMapper.getTableInfo(new HashMap<String, Object>(1) {{
                put("tableName", subTableName);
            }});
            table.setSubTable(generatorTableEntity);
        }
    }

    /**
     * 设置主键列信息
     *
     * @param table 业务表信息
     */
    public void setPkColumn(GeneratorTableEntity table) {
        for (GeneratorTableColumnEntity column : table.getGeneratorTableColumns()) {
            if (column.isPk()) {
                table.setPkColumn(column);
                break;
            }
        }
        if (ObjectUtil.isNull(table.getPkColumn())) {
            table.setPkColumn(table.getGeneratorTableColumns().get(0));
        }
        if (GeneratorConstants.TPL_SUB.equals(table.getTplCategory())) {
            for (GeneratorTableColumnEntity column : table.getSubTable().getGeneratorTableColumns()) {
                if (column.isPk()) {
                    table.getSubTable().setPkColumn(column);
                    break;
                }
            }
            if (ObjectUtil.isNull(table.getSubTable().getPkColumn())) {
                table.getSubTable().setPkColumn(table.getSubTable().getGeneratorTableColumns().get(0));
            }
        }
    }
}
