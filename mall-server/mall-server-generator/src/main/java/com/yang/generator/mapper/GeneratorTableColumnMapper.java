package com.yang.generator.mapper;

import com.mybatisflex.core.BaseMapper;
import com.yang.generator.entity.GeneratorTableColumnEntity;

import java.util.List;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 17:04
 */
public interface GeneratorTableColumnMapper extends BaseMapper<GeneratorTableColumnEntity> {

    /**
     * 根据表名查询列信息
     * @param tableName 表名
     * @return 列集合
     */
    List<GeneratorTableColumnEntity> getListByTableName(String tableName);
}
