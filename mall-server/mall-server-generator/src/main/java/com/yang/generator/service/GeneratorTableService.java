package com.yang.generator.service;

import com.mybatisflex.core.service.IService;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.generator.dto.GeneratorImportTableDto;
import com.yang.generator.dto.GeneratorTableDto;
import com.yang.generator.entity.GeneratorTableEntity;

import java.util.List;
import java.util.Map;

/**
 * 代码生成
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 17:05
 */
public interface GeneratorTableService extends IService<GeneratorTableEntity> {

    /**
     * 查询列表
     *
     * @param generatorTableDto 查询条件
     * @return 分页数据
     */
    PageView<GeneratorTableEntity> getList(GeneratorTableDto generatorTableDto);

    /**
     * 查询数据库表集合
     *
     * @param generatorTableDto 查询条件
     * @return 分页数据
     */
    PageView<GeneratorTableEntity> getTableList(GeneratorTableDto generatorTableDto);

    /**
     * 导入表结构
     *
     * @param importTableDto 导入参数
     */
    void importTables(GeneratorImportTableDto importTableDto);

    /**
     * 预览数据列表
     *
     * @param tableId 生成表ID
     * @return 数据列表
     */
    Map<String, String> previewCode(Long tableId);

    /**
     * 查询业务信息
     *
     * @param tableId 表ID
     * @return 详情
     */
    GeneratorTableEntity selectGeneratorTableById(Long tableId);

    /**
     * 查询所有表信息
     *
     * @return 表信息集合
     */
    List<GeneratorTableEntity> selectGeneratorTableAll();

    /**
     * 修改保存参数校验
     *
     * @param generatorTable 参数
     */
    void validateEdit(GeneratorTableEntity generatorTable);

    /**
     * 修改业务
     *
     * @param generatorTable 参数
     */
    void updateGeneratorTable(GeneratorTableEntity generatorTable);

    /**
     * 删除业务
     *
     * @param tableIds 业务id集合
     */
    void deleteGeneratorTableByIds(Long[] tableIds);

    /**
     * 同步数据库
     *
     * @param tableName 表名称
     */
    void synchroDb(String tableName);

    /**
     * 批量生成代码（下载方式）
     *
     * @param tableNames 表数组
     * @return 数据
     */
    byte[] downloadCode(String[] tableNames);

    /**
     * 生成代码（自定义路径）
     *
     * @param tableName 表名称
     */
    void generatorCode(String tableName);
}
