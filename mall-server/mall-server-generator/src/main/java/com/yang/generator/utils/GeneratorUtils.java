package com.yang.generator.utils;

import com.yang.common.core.constant.GeneratorConstants;
import com.yang.common.core.utils.StringUtils;
import com.yang.generator.dto.GeneratorImportTableDto;
import com.yang.generator.entity.GeneratorTableColumnEntity;
import com.yang.generator.entity.GeneratorTableEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.RegExUtils;

import java.util.Arrays;
import java.util.Date;

/**
 * 代码生成器工具类
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/19 13:50
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class GeneratorUtils {

    public static final String LEFT_BRACKET = "(";
    public static final String RIGHT_BRACKET = ")";
    public static final String NAME = "name";
    public static final String STATUS = "status";
    public static final String SEX = "sex";
    public static final String TYPE = "type";
    public static final String IMAGE = "image";
    public static final String FILE = "file";
    public static final String CONTENT = "content";

    /**
     * 初始化表信息
     *
     * @param tableDto    导入参数
     * @param tableEntity 需要初始化的表信息
     * @param userId      当前登陆人ID
     */
    public static void buildTable(GeneratorImportTableDto tableDto, GeneratorTableEntity tableEntity, Long userId) {
        tableEntity.setClassName(convertClassName(tableDto, tableEntity.getTableName()));
        tableEntity.setPackageName(tableDto.getPackageName());
        tableEntity.setModuleName(getModuleName(tableDto.getPackageName()));
        tableEntity.setBusinessName(getBusinessName(tableEntity.getTableName()));
        tableEntity.setFunctionName(replaceText(tableEntity.getTableComment()));
        tableEntity.setFunctionAuthor(tableDto.getAuthor());
        tableEntity.setCreateId(userId);
        tableEntity.setCreateTime(new Date());
    }

    /**
     * 初始化表中的列
     *
     * @param tableColumnEntity 列
     * @param tableId           表ID
     */
    public static void buildTableColumn(GeneratorTableColumnEntity tableColumnEntity, Long tableId) {
        // 绑定表id
        tableColumnEntity.setTableId(tableId);
        // 列类型
        String columnType = getDbType(tableColumnEntity.getColumnType());
        // 列名称
        String columnName = tableColumnEntity.getColumnName();
        // 设置JAVA字段名
        tableColumnEntity.setJavaField(StringUtils.toCamelCase(columnName));
        // 预先设置默认类型
        tableColumnEntity.setJavaType(GeneratorConstants.TYPE_STRING);
        // 预先设置默认查询类型
        tableColumnEntity.setQueryType(GeneratorConstants.QUERY_EQ);

        if (arraysContains(GeneratorConstants.COLUMN_TYPE_STR, columnType) || arraysContains(GeneratorConstants.COLUMN_TYPE_TEXT, columnType)) {
            // 字符串长度超过500设置为文本域
            Integer columnLength = getColumnLength(tableColumnEntity.getColumnType());
            String htmlType = columnLength >= 500 || arraysContains(GeneratorConstants.COLUMN_TYPE_TEXT, columnType) ? GeneratorConstants.HTML_TEXTAREA : GeneratorConstants.HTML_INPUT;
            tableColumnEntity.setHtmlType(htmlType);
        } else if (arraysContains(GeneratorConstants.COLUMN_TYPE_TIME, columnType)) {
            tableColumnEntity.setJavaType(GeneratorConstants.TYPE_DATE);
            tableColumnEntity.setHtmlType(GeneratorConstants.HTML_DATETIME);
        } else if (arraysContains(GeneratorConstants.COLUMN_TYPE_NUMBER, columnType)) {
            tableColumnEntity.setHtmlType(GeneratorConstants.HTML_INPUT);
            // 如果是浮点型 统一用BigDecimal
            String[] str = StringUtils.split(StringUtils.substringBetween(tableColumnEntity.getColumnType(), "(", ")"), StringUtils.SEPARATOR);
            if (str != null && str.length == 2 && Integer.parseInt(str[1]) > 0) {
                tableColumnEntity.setJavaType(GeneratorConstants.TYPE_BIGDECIMAL);
            } else if (str != null && str.length == 1 && Integer.parseInt(str[0]) <= 10) {
                // 如果是整形
                tableColumnEntity.setJavaType(GeneratorConstants.TYPE_INTEGER);
            } else {
                // 长整形
                tableColumnEntity.setJavaType(GeneratorConstants.TYPE_LONG);
            }
        }

        // BO对象 默认插入勾选
        if (!arraysContains(GeneratorConstants.COLUMN_NAME_NOT_ADD, columnName) && !tableColumnEntity.isPk()) {
            tableColumnEntity.setIsInsert(GeneratorConstants.REQUIRE);
        }
        // BO对象 默认编辑勾选
        if (!arraysContains(GeneratorConstants.COLUMN_NAME_NOT_EDIT, columnName)) {
            tableColumnEntity.setIsEdit(GeneratorConstants.REQUIRE);
        }
        // BO对象 默认是否必填勾选
        if (!arraysContains(GeneratorConstants.COLUMN_NAME_NOT_EDIT, columnName)) {
            tableColumnEntity.setIsRequired(GeneratorConstants.REQUIRE);
        }
        // VO对象 默认返回勾选
        if (!arraysContains(GeneratorConstants.COLUMN_NAME_NOT_LIST, columnName)) {
            tableColumnEntity.setIsList(GeneratorConstants.REQUIRE);
        }
        // BO对象 默认查询勾选
        if (!arraysContains(GeneratorConstants.COLUMN_NAME_NOT_QUERY, columnName) && !tableColumnEntity.isPk()) {
            tableColumnEntity.setIsQuery(GeneratorConstants.REQUIRE);
        }

        // 查询字段类型
        if (StringUtils.endsWithIgnoreCase(columnName, NAME)) {
            tableColumnEntity.setQueryType(GeneratorConstants.QUERY_LIKE);
        }

        if (StringUtils.endsWithIgnoreCase(columnName, STATUS)) {
            // 状态字段设置单选框
            tableColumnEntity.setHtmlType(GeneratorConstants.HTML_RADIO);
        } else if (StringUtils.endsWithIgnoreCase(columnName, TYPE) || StringUtils.endsWithIgnoreCase(columnName, SEX)) {
            // 类型&性别字段设置下拉框
            tableColumnEntity.setHtmlType(GeneratorConstants.HTML_SELECT);
        } else if (StringUtils.endsWithIgnoreCase(columnName, IMAGE)) {
            // 图片字段设置图片上传控件
            tableColumnEntity.setHtmlType(GeneratorConstants.HTML_IMAGE_UPLOAD);
        } else if (StringUtils.endsWithIgnoreCase(columnName, FILE)) {
            // 文件字段设置文件上传控件
            tableColumnEntity.setHtmlType(GeneratorConstants.HTML_FILE_UPLOAD);
        } else if (StringUtils.endsWithIgnoreCase(columnName, CONTENT)) {
            // 内容字段设置富文本控件
            tableColumnEntity.setHtmlType(GeneratorConstants.HTML_EDITOR);
        }
    }

    /**
     * 校验数组是否包含指定值
     *
     * @param arr         数组
     * @param targetValue 值
     * @return 是否包含
     */
    public static boolean arraysContains(String[] arr, String targetValue) {
        return Arrays.asList(arr).contains(targetValue);
    }

    /**
     * 获取字段长度
     *
     * @param columnType 列类型
     * @return 截取后的列类型
     */
    public static Integer getColumnLength(String columnType) {
        if (StringUtils.indexOf(columnType, LEFT_BRACKET) > 0) {
            String length = StringUtils.substringBetween(columnType, LEFT_BRACKET, RIGHT_BRACKET);
            return Integer.valueOf(length);
        } else {
            return 0;
        }
    }

    /**
     * 获取数据库类型字段
     *
     * @param columnType 列类型
     * @return 截取后的列类型
     */
    public static String getDbType(String columnType) {
        if (StringUtils.indexOf(columnType, LEFT_BRACKET) > 0) {
            return StringUtils.substringBefore(columnType, LEFT_BRACKET);
        } else {
            return columnType;
        }
    }

    /**
     * 表名转换成Java类名
     *
     * @param tableName 表名称
     * @return 类名
     */
    public static String convertClassName(GeneratorImportTableDto importTableDto, String tableName) {
        boolean autoRemovePre = importTableDto.getAutoRemovePre();
        String tablePrefix = importTableDto.getTablePrefix();
        if (autoRemovePre && StringUtils.isNotEmpty(tablePrefix)) {
            String[] searchList = StringUtils.split(tablePrefix, StringUtils.SEPARATOR);
            tableName = replaceFirst(tableName, searchList);
        }
        return StringUtils.convertToCamelCase(tableName);
    }

    /**
     * 批量替换前缀
     *
     * @param replacement 替换值
     * @param searchList  替换列表
     * @return 替换结果
     */
    public static String replaceFirst(String replacement, String[] searchList) {
        String text = replacement;
        for (String searchString : searchList) {
            if (replacement.startsWith(searchString)) {
                text = replacement.replaceFirst(searchString, StringUtils.EMPTY);
                break;
            }
        }
        return text;
    }

    /**
     * 获取模块名
     *
     * @param packageName 包名
     * @return 模块名
     */
    public static String getModuleName(String packageName) {
        int lastIndex = packageName.lastIndexOf(".");
        int nameLength = packageName.length();
        return StringUtils.substring(packageName, lastIndex + 1, nameLength);
    }

    /**
     * 获取业务名
     *
     * @param tableName 表名
     * @return 业务名
     */
    public static String getBusinessName(String tableName) {
        int firstIndex = tableName.indexOf("_");
        int nameLength = tableName.length();
        String businessName = StringUtils.substring(tableName, firstIndex + 1, nameLength);
        businessName = StringUtils.toCamelCase(businessName);
        return businessName;
    }

    /**
     * 关键字替换
     *
     * @param text 需要被替换的名字
     * @return 替换后的名字
     */
    public static String replaceText(String text) {
        return RegExUtils.replaceAll(text, "(?:表)", "");
    }
}
