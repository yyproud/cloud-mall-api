package com.yang.generator.service;

import com.mybatisflex.core.service.IService;
import com.yang.generator.entity.GeneratorTableColumnEntity;

import java.util.List;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 17:05
 */
public interface GeneratorTableColumnService extends IService<GeneratorTableColumnEntity> {

    /**
     * 根据表名查询列信息
     * @param tableName 表名
     * @return 列集合
     */
    List<GeneratorTableColumnEntity> getListByTableName(String tableName);

    /**
     * 查询字段列表
     * @param tableId 表ID
     * @return 字段列表
     */
    List<GeneratorTableColumnEntity> getListByByTableId(Long tableId);

    /**
     * 批量插入或更新
     * @param generatorTableColumnList 数据集
     */
    void insertOrUpdateBatch(List<GeneratorTableColumnEntity> generatorTableColumnList);
}
