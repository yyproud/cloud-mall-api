package com.yang.generator;

import org.apache.dubbo.config.spring.context.annotation.EnableDubbo;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.context.config.annotation.RefreshScope;

/**
 * 代码生成器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/7/5 9:43
 */
@EnableDubbo
@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan("${mybatis-plus.mapperPackage}")
public class MallServerGeneratorApplication {
    public static void main(String[] args) {
        SpringApplication.run(MallServerGeneratorApplication.class, args);
    }
}
