package com.yang.generator.service.impl;

import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import com.yang.generator.entity.GeneratorTableColumnEntity;
import com.yang.generator.mapper.GeneratorTableColumnMapper;
import com.yang.generator.service.GeneratorTableColumnService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 17:06
 */
@Service
public class GeneratorTableColumnServiceImpl extends ServiceImpl<GeneratorTableColumnMapper, GeneratorTableColumnEntity> implements GeneratorTableColumnService {
    @Override
    public List<GeneratorTableColumnEntity> getListByTableName(String tableName) {
        return mapper.getListByTableName(tableName);
    }

    @Override
    public List<GeneratorTableColumnEntity> getListByByTableId(Long tableId) {
        QueryWrapper query = new QueryWrapper();

        query.where(GeneratorTableColumnEntity.)


        return mapper.selectListByQuery(new QueryWrapper<GeneratorTableColumnEntity>()
                .eq(GeneratorTableColumnEntity::getTableId, tableId)
                .orderByAsc(GeneratorTableColumnEntity::getSort));
    }

    @Override
    public void insertOrUpdateBatch(List<GeneratorTableColumnEntity> generatorTableColumnList) {
        this.baseMapper.insertOrUpdateBatch(generatorTableColumnList);
    }
}
