package com.yang.generator.mapper;

import com.mybatisflex.core.BaseMapper;
import com.mybatisflex.core.paginate.Page;
import com.yang.generator.dto.GeneratorTableDto;
import com.yang.generator.entity.GeneratorTableEntity;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * 代码生成
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 17:04
 */
public interface GeneratorTableMapper extends BaseMapper<GeneratorTableEntity> {

    /**
     * 查询列表
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<GeneratorTableEntity> getList(Page<GeneratorTableEntity> page, @Param("dto") GeneratorTableDto dto);

    /**
     * 查询数据库表集合
     *
     * @param page 分页插件
     * @param dto  查询条件
     * @return 分页数据
     */
    Page<GeneratorTableEntity> getTableList(Page<GeneratorTableEntity> page, @Param("dto") GeneratorTableDto dto);

    /**
     * 根据表名查询数据集合
     *
     * @param tableNames 表名
     * @return 数据集合
     */
    List<GeneratorTableEntity> getTableListByTableNames(String[] tableNames);

    /**
     * 根据参数查询详情
     *
     * @param map 查询条件
     * @return 详情
     */
    GeneratorTableEntity getTableInfo(@Param("map") Map<String, Object> map);

    /**
     * 查询所有表信息
     *
     * @return 表信息集合
     */
    List<GeneratorTableEntity> selectGeneratorTableAll();

    /**
     * 获取菜单表最大ID
     * @return 最大ID
     */
    Long getMenuMaxId();
}
