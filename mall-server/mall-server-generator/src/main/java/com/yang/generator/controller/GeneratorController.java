package com.yang.generator.controller;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.IoUtil;
import com.yang.common.mybatis.core.page.PageView;
import com.yang.common.core.utils.Result;
import com.yang.generator.dto.GeneratorImportTableDto;
import com.yang.generator.dto.GeneratorTableDto;
import com.yang.generator.entity.GeneratorTableColumnEntity;
import com.yang.generator.entity.GeneratorTableEntity;
import com.yang.generator.service.GeneratorTableColumnService;
import com.yang.generator.service.GeneratorTableService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/9/5 17:03
 */
@RestController
@RequestMapping("/generator")
public class GeneratorController {

    @Resource
    GeneratorTableService generatorTableService;
    @Resource
    GeneratorTableColumnService generatorTableColumnService;

    /**
     * 查询代码生成列表
     *
     * @param generatorTableDto 查询条件
     * @return 分页数据
     */
    @GetMapping("/list")
    public Result getList(GeneratorTableDto generatorTableDto) {
        PageView<GeneratorTableEntity> list = generatorTableService.getList(generatorTableDto);
        return Result.ok(list);
    }

    /**
     * 查询数据库表集合
     *
     * @param generatorTableDto 查询条件
     * @return 分页数据
     */
    @GetMapping("/getTableList")
    public Result getTableList(GeneratorTableDto generatorTableDto) {
        PageView<GeneratorTableEntity> list = generatorTableService.getTableList(generatorTableDto);
        return Result.ok(list);
    }

    /**
     * 保存生成的表结构
     *
     * @param importTableDto 要生成的表
     * @return 保存结果
     */
    @PostMapping("/importTable")
    public Result importTable(GeneratorImportTableDto importTableDto) {
        generatorTableService.importTables(importTableDto);
        return Result.ok();
    }

    /**
     * 预览
     *
     * @param tableId 表ID
     * @return 预览代码
     */
    @GetMapping("/previewCode/{tableId}")
    public Result previewCode(@PathVariable("tableId") Long tableId) {
        Map<String, String> dataMap = generatorTableService.previewCode(tableId);
        return Result.ok(dataMap);
    }

    /**
     * 编辑查看详情
     *
     * @param tableId 表ID
     * @return 详情
     */
    @GetMapping("/getInfo/{tableId}")
    public Result getInfo(@PathVariable("tableId") Long tableId) {
        GeneratorTableEntity generatorTable = generatorTableService.selectGeneratorTableById(tableId);
        List<GeneratorTableEntity> generatorTableEntities = generatorTableService.selectGeneratorTableAll();
        List<GeneratorTableColumnEntity> listByByTableId = generatorTableColumnService.getListByByTableId(tableId);

        Map<String, Object> map = new HashMap<>(8);
        map.put("info", generatorTable);
        map.put("rows", listByByTableId);
        map.put("tables", generatorTableEntities);
        return Result.ok(map);
    }

    @PutMapping("/editSave")
    public Result editSave(@RequestBody GeneratorTableEntity generatorTable) {
        generatorTableService.validateEdit(generatorTable);
        generatorTableService.updateGeneratorTable(generatorTable);
        return Result.ok();
    }

    /**
     * 根据id集合删除业务
     *
     * @param tableIds id集合
     * @return 删除结果
     */
    @DeleteMapping("/delete/{tableIds}")
    public Result delete(@PathVariable Long[] tableIds) {
        generatorTableService.deleteGeneratorTableByIds(tableIds);
        return Result.ok();
    }

    /**
     * 同步数据库
     *
     * @param tableName 表名
     * @return 结果
     */
    @GetMapping("/synchroDb/{tableName}")
    public Result synchroDb(@PathVariable("tableName") String tableName) {
        generatorTableService.synchroDb(tableName);
        return Result.ok();
    }

    @GetMapping("/batchGenerateCode")
    public void batchGenerateCode(HttpServletResponse response, String tables) throws IOException {
        String[] tableNames = Convert.toStrArray(tables);
        byte[] data = generatorTableService.downloadCode(tableNames);
        genCode(response, data);
    }
    @GetMapping("/generateCode/{tableName}")
    public Result generateCode(@PathVariable("tableName") String tableName) {
        generatorTableService.generatorCode(tableName);
        return Result.ok();
    }

    /**
     * 生成zip文件
     */
    private void genCode(HttpServletResponse response, byte[] data) throws IOException {
        response.reset();
        response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
        response.setHeader("Content-Disposition", "attachment; filename=\"ruoyi.zip\"");
        response.addHeader("Content-Length", String.valueOf(data.length));
        response.setContentType("application/octet-stream; charset=UTF-8");
        IoUtil.write(response.getOutputStream(), false, data);
    }
}
