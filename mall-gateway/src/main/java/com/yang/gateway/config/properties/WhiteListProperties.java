package com.yang.gateway.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * 白名单配置
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/6/28 11:16
 */
@Data
@Component
@RefreshScope
@ConfigurationProperties("security")
public class WhiteListProperties {
    /**
     * 白名单集合，网关不进行校验
     */
    private List<String> whiteList = new ArrayList<>();
}
