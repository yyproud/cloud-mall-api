package com.yang.gateway.filter;

import cn.dev33.satoken.reactor.filter.SaReactorFilter;
import cn.dev33.satoken.router.SaRouter;
import cn.dev33.satoken.stp.StpUtil;
import com.yang.gateway.config.properties.WhiteListProperties;
import com.yang.common.core.constant.HttpStatus;
import com.yang.common.core.utils.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 身份认证过滤器
 *
 * @author 杨旭
 * @email 18811132173@163.com
 * @create 2023/6/28 11:04
 */
@Slf4j
@Configuration
public class AuthFilter {
    /**
     * 注册 Sa-Token 全局过滤器
     */
    @Bean
    public SaReactorFilter getSaReactorFilter(WhiteListProperties whiteList) {
        return new SaReactorFilter()
                // 拦截地址
                .addInclude("/**")
                .addExclude("/favicon.ico", "/actuator/**")
                // 鉴权方法：每次访问进入
                .setAuth(obj -> {
                    // 登录校验 -- 拦截所有路由
                    SaRouter.match("/**")
                            .notMatch(whiteList.getWhiteList())
                            .check(r -> {
                                // 检查是否登录 是否有token
                                StpUtil.checkLogin();

                                // 有效率影响 用于临时测试
                                // log.info("剩余有效时间: {}", StpUtil.getTokenTimeout());
                                // log.info("临时有效时间: {}", StpUtil.getTokenActivityTimeout());
                            });
                }).setError(e -> new Result().stringJson(HttpStatus.UNAUTHORIZED, "认证失败，无法访问系统资源!"));
    }
}
